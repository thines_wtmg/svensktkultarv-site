﻿using System.Globalization;
using umbraco.cms.businesslogic.web;
using Umbraco.Core.Models;
using Umbraco.Core.Services;
using Umbraco.Web;

namespace Umbraco.Extensions.Utilities
{
    public static class LocalizationExtensions
    {
        public static ILanguage GetLanguageByContent(this ILocalizationService localizationService, IContent content)
        {
            if (content == null) return null;

            // There isn't a DomainService in Umbraco yet so we need to use the legacy Domain API
            var domain = Domain.GetDomain("*" + content.Id); // dummy domain name : example: *1234

            return (domain == null)
              ? localizationService.GetLanguageByContent(content.Parent()) // recursive
              : localizationService.GetLanguageByCultureCode(domain.Language.CultureAlias);
        }

        public static CultureInfo GetCultureInfo(this IPublishedContent content)
        {
            var services = UmbracoContext.Current.Application.Services;
            var lang = services.LocalizationService.GetLanguageByContent(services.ContentService.GetById(content.Id));
            return lang == null ? CultureInfo.CurrentCulture : lang.CultureInfo;
        }

    }
}