﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Umbraco.Extensions.Utilities
{
    public static class StringExtensions
    {
        public static bool IsNullOrEmpty(this string value)
        {
            return string.IsNullOrEmpty(value);
        }

        public static string TruncateAtWord(this string value, int length)
        {
            if (value == null || value.Length < length || value.IndexOf(" ", length, System.StringComparison.Ordinal) == -1)
                return value;

            return value.Substring(0, value.IndexOf(" ", length, System.StringComparison.Ordinal));
        }
       public static string GetStringByNumberOfChars(string text, int length)
            {
                if (!string.IsNullOrEmpty(text) && text.Length > length)
                {
                    return text.Substring(0, length);
                }

                return text;
            }
       public static string GetUri(this string s)
       {
           try
           {
               var uri = new UriBuilder(s).Uri;
               return uri.ToString();
           }
           catch (Exception)
           {
               return "s";
           }
       }
    }
}