﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using Examine;
using Examine.SearchCriteria;

namespace Umbraco.Extensions.Utilities.Services
{
    public class ExamineSearchService
    {
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public ISearchCriteria GetAllAttractionsSearchCriteria()
        {
            var criteria = ExamineManager.Instance.SearchProviderCollection["AttractionSearcher"].CreateSearchCriteria();
            var query = new StringBuilder();

            //Exclude 'onlyShowInApp' exhibitions.
            query.Append("(");
            query.Append(string.Format("+onlyShowInApp:{0} ", "0"));
            query.Append(")");

            //For HomePage
            query.Append(string.Format("AND searchPath:{0} ", "1285"));

            //String should build like this: "(+onlyShowInApp:0) AND (searchPath:1285) "
            return criteria.RawQuery(query.ToString());
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public ISearchCriteria GetAllExhibitionsSearchCriteria()
        {
            var criteria = ExamineManager.Instance.SearchProviderCollection["ExhibitionSearcher"].CreateSearchCriteria();
            var query = new StringBuilder();

            //Exclude 'onlyShowInApp' exhibitions.
            query.Append("(");
            query.Append(string.Format("+onlyShowInApp:{0} ", "0"));
            query.Append(")");

            //For HomePage
            query.Append(string.Format("AND searchPath:{0} ", "1285"));

            //String should build like this: "(+onlyShowInApp:0) AND (searchPath:1285) "
            return criteria.RawQuery(query.ToString());
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ancestorNodes"></param>
        /// <param name="attractionCategories"></param>
        /// <param name="serviceCategories"></param>
        /// <returns></returns>
        public ISearchCriteria GetAttractionSearchCriteria(string[] ancestorNodes, string[] attractionCategories, string[] serviceCategories)
        {
            var criteria = ExamineManager.Instance.SearchProviderCollection["AttractionSearcher"].CreateSearchCriteria();
            var query = new StringBuilder();
            const string operatorAndString = " AND ";

            //Exclude 'onlyShowInApp' exhibitions.
            query.Append("(");
            query.Append(string.Format("+onlyShowInApp:{0} ", "0"));
            query.Append(")");

            //Builds the query string for selected counties/geocontainers.
            if (ancestorNodes.Any())
            {
                query.Append(" AND (");
                foreach (string s in ancestorNodes)
                {
                    query.Append(string.Format("searchPath:{0} ", s));
                }
                query.Append(")");
            }

            //Builds the query string for selected attraction categories.
            if (attractionCategories.Any())
            {
                query.Append(string.Format("{0}(", operatorAndString));
                foreach (string s in attractionCategories)
                {
                    query.Append(string.Format("attractionCategories:{0} ", s));
                }
                query.Append(")");
            }

            //Builds the query string for selected service categories.
            if (serviceCategories.Any())
            {
                query.Append(string.Format("{0}(", operatorAndString));
                foreach (string s in serviceCategories)
                {
                    query.Append(string.Format("+serviceCategories:{0} ", s));
                }
                query.Append(")");
            }

            //String should build like this: "(+onlyShowInApp:0) AND (searchPath:1329 searchPath:1300 searchPath:1334 searchPath:1331 searchPath:1333 searchPath:1337) AND (attractionCategories:1305 attractionCategories:1358) AND (+serviceCategories:1361 +serviceCategories:1363 +serviceCategories:1307)"
            return criteria.RawQuery(query.ToString());
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ancestorNodes"></param>
        /// <param name="exhibitionCategories"></param>
        /// <param name="serviceCategories"></param>
        /// <param name="startDate"></param>
        /// <param name="stopDate"></param>
        /// <returns></returns>
        public ISearchCriteria GetExhibitionSearchCriteria(string[] ancestorNodes, string[] exhibitionCategories, string[] serviceCategories, string startDate = "", string stopDate = "")
        {
            const string unreachableFuture = "25000101";
            const string unreachablePast = "00010101";
            string now = DateTime.Now.Date.ToString("yyyyMMdd");
            string searchStart = string.IsNullOrWhiteSpace(startDate) ? now : startDate;
            string searchStop = string.IsNullOrWhiteSpace(stopDate) ? unreachableFuture : stopDate;
            var criteria = ExamineManager.Instance.SearchProviderCollection["ExhibitionSearcher"].CreateSearchCriteria();
            var query = new StringBuilder();
            const string operatorAndString = " AND ";

            //Exclude 'onlyShowInApp' exhibitions.
            query.Append("(");
            query.Append(string.Format("+onlyShowInApp:{0} ", "0"));
            query.Append(")");

            //Builds the query string for selected counties/geocontainers.
            if (ancestorNodes.Any())
            {
                query.Append(" AND (");
                foreach (string s in ancestorNodes)
                {
                    query.Append(string.Format("searchPath:{0} ", s));
                }
                query.Append(")");
            }

            //Builds the query string for selected main categories/attraction categories.
            if (exhibitionCategories.Any())
            {
                query.Append(string.Format("{0}(", operatorAndString));
                foreach (string s in exhibitionCategories)
                {
                    query.Append(string.Format("exhibitionCategories:{0} ", s));
                }
                query.Append(")");
            }

            //Builds the query string for selected service categories.
            if (serviceCategories.Any())
            {
                query.Append(string.Format("{0}(", operatorAndString));
                foreach (string s in serviceCategories)
                {
                    query.Append(string.Format("+serviceCategories:{0} ", s));
                }
                query.Append(")");
            }

            query.Append(string.Format("{0}", operatorAndString));

            query.Append(string.Format("(stopDate:[{0} TO {1}] ", searchStart, unreachableFuture));
            query.Append(")");

            query.Append(string.Format(" AND (startDate:[{0} TO {1}] ", unreachablePast, searchStop));
            query.Append(")");

            //String should build like this: "(+onlyShowInApp:0) AND (searchPath:1329 searchPath:1300 searchPath:1334 searchPath:1331 searchPath:1333 searchPath:1337) AND (exhibitionCategories:1305 exhibitionCategories:1358) AND (+serviceCategories:1361 +serviceCategories:1363 +serviceCategories:1307) AND (stopDate:[20150101 TO 25000101]) AND (startDate:[00010101 TO 20150101])"
            return criteria.RawQuery(query.ToString());
        }

    }
}