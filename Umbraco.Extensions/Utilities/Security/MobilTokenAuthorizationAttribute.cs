﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;
using Google.Apis.Auth;
using Umbraco.Core;
using Umbraco.Extensions.Controllers.WebAPI.v1;
using AuthorizeAttribute = System.Web.Mvc.AuthorizeAttribute;

namespace Umbraco.Extensions.Utilities.Security
{
    /// <summary>
    /// Cheep authority check for mobil api, checks request headers for authorizationToken (Stored in APP and in web.config) 
    /// </summary>
    public class MobilTokenAuthorizationAttribute : AuthorizationFilterAttribute 
    {
        public override void OnAuthorization(HttpActionContext actionContext)
        {
            if (!actionContext.Request.Headers.Contains("authority"))
            {
                var missingTokenResponse = new System.Net.Http.HttpResponseMessage(System.Net.HttpStatusCode.BadRequest);
                throw new HttpResponseException(missingTokenResponse);
            }
            else
            {
                var token = actionContext.Request.Headers.GetValues("authority").FirstOrDefault();
                if (token == Settings.MobilApiKeyStore.WebConfigMobilApiKey)
                    return;
                else
                {
                    var unAuthorizedResponse = new System.Net.Http.HttpResponseMessage(System.Net.HttpStatusCode.Forbidden);
                    throw new HttpResponseException(unAuthorizedResponse);
                }
            }
        }
        
    }
}