﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Caching;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;
using StackExchange.Profiling;

//Throttle function for web api calls se:http://stackoverflow.com/questions/20817300/how-to-throttle-requests-in-a-web-api and :http://www.codeproject.com/Articles/23306/ASP-NET-Performance-and-Scalability-Secrets Better would be -> https://github.com/stefanprodan/WebApiThrottle
namespace Umbraco.Extensions.Utilities.Security
{
    public class ThrottleAttribute : ActionFilterAttribute
    {
        /// <summary>
        /// A unique name for this Throttle.
        /// </summary>
        /// <remarks>
        /// We'll be inserting a Cache record based on this name and client IP, e.g. "Name-192.168.0.1"
        /// </remarks>
        public string Name { get; set; }

        /// <summary>
        /// The number of minutes clients must wait before executing this decorated route again.
        /// </summary>
        public int Duration { get; set; }

        /// <summary>
        /// The number of hits thats allowed per client under specified duration 
        /// </summary>
        public int Hits { get; set; }

        /// <summary>
        /// A text message that will be sent to the client upon throttling.  
        /// </summary>
        public string Message { get; set; }

        public override void OnActionExecuting(HttpActionContext actionContext)
        {
            var allowExecute = false;
            var key = string.Concat(Name, "-", GetClientIp(actionContext.Request));
            var hitsForUser = (HitInfo)(HttpRuntime.Cache[key] ?? new HitInfo());
            hitsForUser.Hits++;

            allowExecute = hitsForUser.Hits > (int) Hits ? false : true;

            if (hitsForUser.Hits == 1)
            {
                HttpRuntime.Cache.Add(key,
                    hitsForUser, // is this the smallest data we can have?
                    null, // no dependencies
                    DateTime.Now.AddMinutes(Duration), // absolute expiration
                    System.Web.Caching.Cache.NoSlidingExpiration,
                    CacheItemPriority.Low,
                    null); // no callback

                allowExecute = true;
            }

            if (!allowExecute)
            {
                if (string.IsNullOrEmpty(Message))
                {
                    Message = string.Format("You may only perform this action {0} times every {1} minutes", Hits.ToString(CultureInfo.InvariantCulture), Duration.ToString(CultureInfo.InvariantCulture));
                }

                actionContext.Response = actionContext.Request.CreateResponse((HttpStatusCode)429, Message);
            }
        }
        private string GetClientIp(HttpRequestMessage request)
        {
            // Web-hosting
            if (request.Properties.ContainsKey("HttpContext"))
            {
                var ctx = (HttpContextWrapper)request.Properties["HttpContext"];
                if (ctx != null)
                {
                    return ctx.Request.UserHostAddress;
                }
            }

            return null;
        }
    }

    public class HitInfo
    {
        public int Hits { get; set; }
    }
}
