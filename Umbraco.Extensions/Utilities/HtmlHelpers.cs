﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using Umbraco.Extensions.Models.Custom;
using Umbraco.Extensions.Models.Generated;

namespace Umbraco.Extensions.Utilities
{
    public static class HtmlHelpers
    {
        //TODO:Make Globalized support.
        //TODO:Refactore 
        public static IHtmlString RenderOrdinaryOpeningHours(this HtmlHelper helper, OpeningHours openingHours)
        {
            var sb = new StringBuilder();
            var tb = new TagBuilder("p");
            string closed = "stängt";

            //Måndag
            #region måndag

            string day = "Måndag";
            if ((openingHours.Monday.To - openingHours.Monday.From) > new TimeSpan(0, 0, 0))
            {
                tb.InnerHtml = string.Format("<span class=\"day\">{0}: </span><span class=\"time\">{1:hh\\:mm} - {2:hh\\:mm}</span>", day, openingHours.Monday.From, openingHours.Monday.To);
                sb.Append(tb.ToString() + Environment.NewLine);
            }

            #endregion

            //Tisdag
            #region Tisdag

            day = "Tisdag";
            if ((openingHours.Tuesday.To - openingHours.Tuesday.From) > new TimeSpan(0, 0, 0))
            {
                tb.InnerHtml = string.Format("<span class=\"day\">{0}: </span><span class=\"time\">{1:hh\\:mm} - {2:hh\\:mm}</span>", day, openingHours.Tuesday.From, openingHours.Tuesday.To);
                sb.Append(tb.ToString() + Environment.NewLine);
            }

            #endregion

            //Onsdag
            #region Onsdag

            day = "Onsdag";
            if ((openingHours.Wednesday.To - openingHours.Wednesday.From) > new TimeSpan(0, 0, 0))
            {
                tb.InnerHtml = string.Format("<span class=\"day\">{0}: </span><span class=\"time\">{1:hh\\:mm} - {2:hh\\:mm}</span>", day, openingHours.Wednesday.From, openingHours.Wednesday.To);
                sb.Append(tb.ToString() + Environment.NewLine);
            }

            #endregion

            //Torsdag
            #region Torsdag

            day = "Torsdag";
            if ((openingHours.Thursday.To - openingHours.Thursday.From) > new TimeSpan(0, 0, 0))
            {
                tb.InnerHtml = string.Format("<span class=\"day\">{0}: </span><span class=\"time\">{1:hh\\:mm} - {2:hh\\:mm}</span>", day, openingHours.Thursday.From, openingHours.Thursday.To);
                sb.Append(tb + Environment.NewLine);
            }

            #endregion

            //Fredag
            #region Fredag

            day = "Fredag";
            if ((openingHours.Friday.To - openingHours.Friday.From) > new TimeSpan(0, 0, 0))
            {
                tb.InnerHtml = string.Format("<span class=\"day\">{0}: </span><span class=\"time\">{1:hh\\:mm} - {2:hh\\:mm}</span>", day, openingHours.Friday.From, openingHours.Friday.To);
                sb.Append(tb + Environment.NewLine);
            }

            #endregion

            //Lördag
            #region Lördag

            day = "Lördag";
            if ((openingHours.Saturday.To - openingHours.Saturday.From) > new TimeSpan(0, 0, 0))
            {
                tb.InnerHtml = string.Format("<span class=\"day\">{0}: </span><span class=\"time\">{1:hh\\:mm} - {2:hh\\:mm}</span>", day, openingHours.Saturday.From, openingHours.Saturday.To);
                sb.Append(tb + Environment.NewLine);
            }

            #endregion

            //Söndag
            #region Söndag

            day = "Söndag";
            if ((openingHours.Sunday.To - openingHours.Sunday.From) > new TimeSpan(0, 0, 0))
            {
                tb.InnerHtml = string.Format("<span class=\"day\">{0}: </span><span class=\"time\">{1:hh\\:mm} - {2:hh\\:mm}</span>", day, openingHours.Sunday.From, openingHours.Sunday.To);
                sb.Append(tb + Environment.NewLine);
            }

            #endregion

            return new HtmlString(sb.ToString());
        }


        public static IHtmlString RenderOrdinaryOpeningHours(this HtmlHelper helper, Attraction attraction, CultureInfo culture)
        {
            var sb = new StringBuilder();
            var tb = new TagBuilder("p");

            tb.InnerHtml = culture.DateTimeFormat.GetDayName(0);
            if ((attraction.OrdinaryOpeningHours.Monday.From != attraction.OrdinaryOpeningHours.Tuesday.From) || attraction.OrdinaryOpeningHours.Monday.To != attraction.OrdinaryOpeningHours.Tuesday.To)
            {

            }

            return new HtmlString("");
        }
    }
}