﻿using System;
using System.IO;
using System.Reflection;
using System.Runtime.Caching;
using System.Web.Hosting;
using log4net;
using Newtonsoft.Json;

namespace Umbraco.Extensions.Utilities.Cache
{
    public class JsonDiskCache
    {
        private static readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        public bool Add<T>(string Key, T Object)
        {
            if (string.IsNullOrEmpty(Key) || Object == null)
            {
                return false;
            }

            if (HostingEnvironment.ApplicationPhysicalPath != null)
            {
                string jsonPath = Path.Combine(HostingEnvironment.ApplicationPhysicalPath, "App_Data", "JsonCache",
                    Key + ".json");

                string serializedJson = JsonConvert.SerializeObject(Object);

                lock (Key)
                {
                    try
                    {
                        System.IO.File.WriteAllText(jsonPath, serializedJson);
                        MemoryCache.Default[Key] = Object;

                        return true;
                    }
                    catch (Exception ex)
                    {
                        Log.ErrorFormat("Could not write JsonDiskCache:{0} ", ex.Message);
                    }
                }
            }

            return false;
        }

        public T Get<T>(string Key)
        {
            if (string.IsNullOrEmpty(Key))
            {
                return default(T);
            }

            T cachedObject = (T) MemoryCache.Default[Key];

            if (cachedObject == null)
            {
                if (HostingEnvironment.ApplicationPhysicalPath != null)
                {
                    string jsonPath = Path.Combine(HostingEnvironment.ApplicationPhysicalPath, "App_Data", "JsonCache",
                        Key + ".json");

                    if (System.IO.File.Exists(jsonPath))
                    {
                        lock (Key)
                        {
                            try
                            {
                                string json = System.IO.File.ReadAllText(jsonPath);
                                T deserializedObject = JsonConvert.DeserializeObject<T>(json);
                                MemoryCache.Default[Key] = deserializedObject;
                                return deserializedObject;
                            }
                            catch (Exception ex)
                            {
                                Log.ErrorFormat("Could not read JsonDiskCache:{0} ", ex.Message);
                            }
                        }
                    }
                }
            }
            else
            {
                return cachedObject;
            }

            return default(T);
        }
    }
}