﻿using System;
using System.Runtime.Caching;

namespace Umbraco.Extensions.Utilities.Cache
{
    public class MemoryCacher
    {
        public object GetValue(string key)
        {
            MemoryCache memoryCache = MemoryCache.Default;
            return memoryCache.Get(key);
        }

        public bool Add(string key, object value, DateTimeOffset absExpiration)
        {
            MemoryCache memoryCache = MemoryCache.Default;
            return memoryCache.Add(key, value, absExpiration);
        }

        public void Delete(string key)
        {
            MemoryCache memoryCache = MemoryCache.Default;
            if (memoryCache.Contains(key))
            {
                memoryCache.Remove(key);
            }
        }

        public void RemoveAll()
        {
            this.Delete(Controllers.WebAPI.v1.Settings.CacheKeys.AllAttractions);
            this.Delete(Controllers.WebAPI.v1.Settings.CacheKeys.AllExhibitions);
            //this.Delete(Controllers.WebAPI.v1.Settings.CacheKeys.AllAttractionsForAutoComplete);
            //this.Delete(Controllers.WebAPI.v1.Settings.CacheKeys.AllExhibitionsForAutoComplete);
            //this.Delete(Controllers.WebAPI.v1.Settings.CacheKeys.AllAttractionsExhibitionsForAutoComplete);

            //Removing Cache for initial requests ie. ("/api/v1/attractions/getallforhome" with pagesizes)
            this.Delete(GetSimpleCacheKeyFromPageSize(Controllers.WebAPI.v1.Settings.CacheKeys.AllAttractions, Controllers.WebAPI.v1.Settings.MaxPageSize));
            this.Delete(GetSimpleCacheKeyFromPageSize(Controllers.WebAPI.v1.Settings.CacheKeys.AllAttractions, Controllers.WebAPI.v1.Settings.InitialPageSizeForHomePage));
            this.Delete(GetSimpleCacheKeyFromPageSize(Controllers.WebAPI.v1.Settings.CacheKeys.AllAttractions, Controllers.WebAPI.v1.Settings.InitialPageSizeForAttractionSearch));
            this.Delete(GetSimpleCacheKeyFromPageSize(Controllers.WebAPI.v1.Settings.CacheKeys.AllExhibitions, Controllers.WebAPI.v1.Settings.MaxPageSize));
            this.Delete(GetSimpleCacheKeyFromPageSize(Controllers.WebAPI.v1.Settings.CacheKeys.AllExhibitions, Controllers.WebAPI.v1.Settings.InitialPageSizeForHomePage));
            this.Delete(GetSimpleCacheKeyFromPageSize(Controllers.WebAPI.v1.Settings.CacheKeys.AllExhibitions, Controllers.WebAPI.v1.Settings.InitialPageSizeForExhibitionSearch));
            
        }

        public string GetSimpleCacheKeyFromPageSize(string baseKey, int pageSize)
        {
            return baseKey + pageSize;
        }
    }
}