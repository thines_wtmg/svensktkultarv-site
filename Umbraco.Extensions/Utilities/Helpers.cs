﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Mvc;
using log4net;
using Umbraco.Extensions.Models.Custom;
using Umbraco.Extensions.Models.Generated;

namespace Umbraco.Extensions.Utilities
{
    public class Helpers
    {
        private static readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        #region DateTime helpers

        /// <summary>
        /// 
        /// </summary>
        /// <param name="start"></param>
        /// <param name="stop"></param>
        /// <returns></returns>
        public static string GetFormatedStartStopDates(DateTime start, DateTime stop)
        {
            string startString = start.ToString(start.Year == stop.Year ? "dd MMMM" : "dd MMMM yyyy");

            return string.Format("{0} - {1}", startString, stop.ToString("dd MMMM yyyy"));
        }

        public static string GetFormatedStartStopDates(DateTime start, DateTime stop, String langCulture)
        {
            CultureInfo culture = new CultureInfo(langCulture);
            DateTime dt = DateTime.MinValue;

            string startString = start.ToString(start.Year == stop.Year ? "dd MMMM" : "dd MMMM yyyy", culture);

            return string.Format("{0} - {1}", startString, stop.ToString("dd MMMM yyyy", culture));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        public static TimeSpan GetTimeSpan(string s)
        {
            TimeSpan outValue;
            bool success = TimeSpan.TryParse(s, out outValue);
            return success ? outValue : TimeSpan.Zero;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="openingHours"></param>
        /// <returns></returns>
        public static bool IsOpen(OpeningHours openingHours)
        {
            var nowDate = DateTime.Now;
            TimeSpan now = nowDate.TimeOfDay;

            ////Check if we have any exception to ordinary opening hours.
            //if (openingHours != null && openingHours.OpeningHoursExceptions != null)
            //{
            //    var exceptionDate = openingHours.OpeningHoursExceptions.FirstOrDefault(d => d.Date.Date == nowDate.Date);
            //    if (exceptionDate != null)
            //    {
            //        return IsOpened(now, exceptionDate.From, exceptionDate.To);
            //    }
            //}

            if (openingHours != null)
            {
                //Check against ordinary opening hours
                switch (nowDate.DayOfWeek)
                {
                    case DayOfWeek.Monday:
                        return IsOpened(now, openingHours.Monday.From, openingHours.Monday.To);
                    case DayOfWeek.Tuesday:
                        return IsOpened(now, openingHours.Tuesday.From, openingHours.Tuesday.To);
                    case DayOfWeek.Wednesday:
                        return IsOpened(now, openingHours.Wednesday.From, openingHours.Wednesday.To);
                    case DayOfWeek.Thursday:
                        return IsOpened(now, openingHours.Thursday.From, openingHours.Thursday.To);
                    case DayOfWeek.Friday:
                        return IsOpened(now, openingHours.Friday.From, openingHours.Friday.To);
                    case DayOfWeek.Saturday:
                        return IsOpened(now, openingHours.Saturday.From, openingHours.Saturday.To);
                    case DayOfWeek.Sunday:
                        return IsOpened(now, openingHours.Sunday.From, openingHours.Sunday.To);
                    default:
                        return true;
                }
            }
            return false;

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="from"></param>
        /// <param name="to"></param>
        /// <returns></returns>
        private static bool IsClosed(TimeSpan from, TimeSpan to)
        {
            if (from == TimeSpan.Zero || to == TimeSpan.Zero)
            {
                return true;
            }
            if ((to - from) <= new TimeSpan(0, 0, 0))
                return true;
            return false;
        }

        private static bool IsOpened(TimeSpan now, TimeSpan from, TimeSpan to)
        {
            //Check if we have time span zero or the difference between from and to is 0 or negative
            if (IsClosed(from, to))
                return false;
            return true;

            //We do not check precis, only if its open / day
            //if ((now > from) && (now < to))
            //{
            //    return true; //It's Open
            //}
            //else
            //{
            //    return false; //It's Closed
            //}
        }

        public static string GetTodaysOpeningHours(OpeningHours openingHours)
        {
            var nowDate = DateTime.Now;

            try
            {
                switch (nowDate.DayOfWeek)
                {
                    case DayOfWeek.Monday:
                        return openingHours.Monday.From.ToString("hh\\:mm") + " - " +
                               openingHours.Monday.To.ToString("hh\\:mm");
                    case DayOfWeek.Tuesday:
                        return openingHours.Tuesday.From.ToString("hh\\:mm") + " - " +
                               openingHours.Tuesday.To.ToString("hh\\:mm");
                    case DayOfWeek.Wednesday:
                        return openingHours.Wednesday.From.ToString("hh\\:mm") + " - " +
                               openingHours.Wednesday.To.ToString("hh\\:mm");
                    case DayOfWeek.Thursday:
                        return openingHours.Thursday.From.ToString("hh\\:mm") + " - " +
                               openingHours.Thursday.To.ToString("hh\\:mm");
                    case DayOfWeek.Friday:
                        return openingHours.Friday.From.ToString("hh\\:mm") + " - " +
                               openingHours.Friday.To.ToString("hh\\:mm");
                    case DayOfWeek.Saturday:
                        return openingHours.Saturday.From.ToString("hh\\:mm") + " - " +
                               openingHours.Saturday.To.ToString("hh\\:mm");
                    case DayOfWeek.Sunday:
                        return openingHours.Sunday.From.ToString("hh\\:mm") + " - " +
                               openingHours.Sunday.To.ToString("hh\\:mm");
                    default:
                        return "";
                }

            }
            catch (Exception ex)
            {
                Log.ErrorFormat("Time format error:{0} ", ex.Message);
                return "Nan - Nan";
            }
            
        }

        public static bool OrdinaryOpeningHoursSet(OpeningHours openingHours)
        {
            var nowDate = DateTime.Now;
            TimeSpan now = nowDate.TimeOfDay;

            if (IsOpened(now, openingHours.Monday.From, openingHours.Monday.To))
                return true;
            if (IsOpened(now, openingHours.Tuesday.From, openingHours.Tuesday.To))
                return true;
            if (IsOpened(now, openingHours.Wednesday.From, openingHours.Wednesday.To))
                return true;
            if (IsOpened(now, openingHours.Thursday.From, openingHours.Thursday.To))
                return true;
            if (IsOpened(now, openingHours.Friday.From, openingHours.Friday.To))
                return true;
            if (IsOpened(now, openingHours.Saturday.From, openingHours.Saturday.To))
                return true;
            if (IsOpened(now, openingHours.Sunday.From, openingHours.Sunday.To))
                return true;
            return false;
        }


        #endregion

        /// <summary>
        /// 
        /// </summary>
        /// <param name="commaSeparatedIntegers"></param>
        /// <returns></returns>
        public static int[] ConvertCommaSeparatedIntStringToIntArray(string commaSeparatedIntegers)
        {
            if (!string.IsNullOrEmpty(commaSeparatedIntegers))
            {
                var ints = commaSeparatedIntegers.Split(',').Select(str =>
                {
                    int value;
                    bool success = int.TryParse(str, out value);
                    return new { value, success };
                }).Where(pair => pair.success).Select(pair => pair.value);

                return ints.ToArray();
            }
            var empty = new List<int>();
            return empty.ToArray();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="commaSeparatedIntegers"></param>
        /// <returns></returns>
        public static string[] ConvertCommaSeparatedIntStringToStringArray(string commaSeparatedIntegers)
        {
            if (!string.IsNullOrEmpty(commaSeparatedIntegers))
            {
                var ints = commaSeparatedIntegers.Split(',');
                return ints.ToArray();
            }
            var empty = new List<string>();
            return empty.ToArray();
        }

    }
}