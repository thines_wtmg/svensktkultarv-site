﻿using System;
using System.Collections.Generic;
using System.Linq;
using Umbraco.Extensions.Models.Rdbms.POCOS;
using Umbraco.Extensions.Utilities;

namespace Umbraco.Extensions.BLL
{
    public class CultureCardLogic : BaseClass
    {
        private static bool CultureCardExists(string number)
        {
            var cultureCard = Database.Query<CultureCard>("SELECT * FROM CultureCard WHERE PhoneNumber=@0", number).FirstOrDefault();
            if (cultureCard != null)
            {
                return true;
            }
            return false;
        }

        private static bool CultureCardExistsWithZipCode(string number, string zip)
        {
            var cultureCard = Database.Query<CultureCard>("SELECT * FROM CultureCard WHERE PhoneNumber=@0", number).FirstOrDefault();
            if (cultureCard != null)
            {
                if (cultureCard.ZipCode == zip)
                    return true;

                return false;
            }
            return false;
        }

        public static bool CultureCardHasZip(CultureCard cultureCard, string zip)
        {
            if (cultureCard.ZipCode.Trim() == zip.Trim())
            {
                return true;
            }
            return false;
        }

        public static IEnumerable<CultureCard> GetAll()
        {
            var cultureCard = Database.Query<CultureCard>("SELECT * FROM CultureCard");

            return cultureCard;
        }

        public static CultureCard GetById(int id)
        {
            var cultureCard = Database.Query<CultureCard>("SELECT * FROM CultureCard WHERE id=@0",id).FirstOrDefault();

            return cultureCard;
        }

        public static CultureCard GetByNumberAndZip(string number, string zip)
        {
            if (CultureCardExistsWithZipCode(number, zip))
            {
                var cultureCard = Database.Query<CultureCard>("SELECT * FROM CultureCard WHERE PhoneNumber=@0", number).FirstOrDefault();
                return cultureCard;
            }
            return null;
        }

        public static CultureCard GetByNumber(string number)
        {
            var cultureCard = Database.Query<CultureCard>("SELECT * FROM CultureCard WHERE PhoneNumber=@0", number).FirstOrDefault();
            return cultureCard;
        }

        public static IEnumerable<CultureCard> GetAllExpired()
        {
            var cultureCard = Database.Query<CultureCard>("SELECT * FROM CultureCard Where ExpireDate < @0",DateTime.Now);

            return cultureCard;
        }
    }

    public static class CultureCardExtensions
    {
        public static string LogFormatedString(this CultureCard card)
        {
            return string.Format("Id:{0} Firstname:{1} Lastname:{2} Phone:{3} Zip:{4} Device:{5} Expire:{6}", card.Id, card.FirstName, card.LastName, card.PhoneNumber, card.ZipCode, card.DeviceId, card.ExpireDate);
        }
    }
}