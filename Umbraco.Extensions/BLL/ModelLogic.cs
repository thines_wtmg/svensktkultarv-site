﻿using System;
using System.Collections.Generic;
using System.Linq;
using umbraco;
using Umbraco.Core.Models;
using Umbraco.Extensions.Models;
using Umbraco.Extensions.Models.Custom;
using Umbraco.Extensions.Models.Generated;
using Umbraco.Extensions.Utilities;
using Umbraco.Web;
using Archetype.Models;

namespace Umbraco.Extensions.BLL
{
    public class ModelLogic : BaseClass
    {
        #region MasterModel

        public static IMasterModel CreateMasterModel()
        {
            var content = UmbracoContext.Current.PublishedContentRequest.PublishedContent;
            return CreateMasterModel(content);
        }

        public static IMasterModel CreateMasterModel(IPublishedContent content)
        {
            var baseContent = content as Base;
            var model = CreateMagicModel(typeof(MasterModel<>), content) as IMasterModel;
            model.MenuItems = GetMenuItems();
            model.SeoTitle = GetMetaTitle(content);
            model.SeoDescription = GetMetaDescription(content);
            model.Twitter = content.StartPage().Twitter;
            model.Facebook = content.StartPage().Facebook;
            model.FooterAdress = GetFooterAdress(content);
            model.AboutUsText = content.StartPage().AboutUsText as string;
            model.AboutUsLink = content.StartPage().AboutUsLink;
            model.MobilAppInfoPage = content.StartPage().MobilAppInfoPage;
            model.SwedishHeritageCardInfoPage = content.StartPage().SwedishHeritageCardInfoPage;
            model.SwedishHeritageCardImage = content.StartPage().SwedishHeritageCardImage;
            model.Ads = AdLogic.GetAds();
            model.ShowSideBarMenu = baseContent.ShowSideBarMenu;
            model.SideBarMenuStart = baseContent.SideBarMenuStart;
            model.CurrentPage = content;
            return model;
        }

        private static object CreateMagicModel(Type genericType, IPublishedContent content)
        {
            var contentType = content.GetType();
            var modelType = genericType.MakeGenericType(contentType);
            var model = Activator.CreateInstance(modelType, content);
            return model;
        }

        private static IEnumerable<MenuItem> GetMenuItems()
        {
            var menuItems = (
                from n in CurrentPage.StartPage().Children<Base>()
                where !n.HideInMenu
                select new MenuItem()
                {
                    Id = n.Id,
                    Title = n.MenuTitle,
                    Url = n.Url,
                    ActiveClass = CurrentPage.Path.Contains(n.Id.ToString()) ? "active" : null,
                    ChildPages = new List<MenuItem>()
                }).ToList();

            AddLevelTwoMenuItems(menuItems);

            //Adding the startpage
            menuItems.Insert(0, new MenuItem()
            {
                Id = CurrentPage.StartPage().Id,
                Title = CurrentPage.StartPage().MenuTitle,
                Url = CurrentPage.StartPage().Url,
                ActiveClass = CurrentPage == CurrentPage.StartPage() ? "active" : null,
                ChildPages = new List<MenuItem>()
            });

            return menuItems;
        }

        private static void AddLevelTwoMenuItems(IEnumerable<MenuItem> topMenuItems)
        {
            var menuItems = topMenuItems as MenuItem[] ?? topMenuItems.ToArray();
            foreach (var menuItem in menuItems)
            {
                menuItem.ChildPages = Umbraco.TypedContent(menuItem.Id).Children<Base>().Where(p => p.HideInMenu != true && !IsNewsItem(p)).Select(m => new MenuItem
                {
                    Id = m.Id,
                    Title = m.MenuTitle,
                    Url = m.Url,
                    ActiveClass = CurrentPage.Id == m.Id ? "active" : null
                });
            }

            foreach (var menuItem in menuItems)
            {
                if (menuItem.ChildPages.Any())
                {
                    string appendClass = string.IsNullOrEmpty(menuItem.ActiveClass) ? "has-children" : " has-children";
                    menuItem.ActiveClass += appendClass;
                }
            }
        }

        private static bool IsNewsItem(Base content)
        {
            var newsItem = content as Newsitem;
            return newsItem != null;
        }

        private static Adress GetFooterAdress(IPublishedContent content)
        {
            var archetypeModel = content.StartPage().GetPropertyValue<ArchetypeModel>("adress");
            return archetypeModel.Select(x => new Adress()
            {
                Adress1 = x.GetValue<string>("adressLine1"),
                Adress2 = x.GetValue<string>("adressLine2"),
                Adress3 = x.GetValue<string>("adressLine3"),
                Email = x.GetValue<string>("email"),
                Phone = x.GetValue<string>("phone")
            }).FirstOrDefault();
        }

        public static string GetMetaDescription(IPublishedContent content)
        {
            var baseContent = content as Base;
            if (baseContent != null)
            {
                var preamble = baseContent.Preamble != null ? baseContent.Preamble as string : string.Empty;
                return string.IsNullOrEmpty(baseContent.Seo.Description)
                    ? preamble.TruncateAtWord(175)
                    : baseContent.Seo.Description.Trim();
            }
            else
            {
                return "";
            }
        }

        public static string GetMetaTitle(IPublishedContent content)
        {
            var baseContent = content as Base;
            if (baseContent != null)
                return string.IsNullOrEmpty(baseContent.Seo.Title) ? baseContent.Name : baseContent.Seo.Title.Trim();
            return "";
        }

        #endregion
    }
}