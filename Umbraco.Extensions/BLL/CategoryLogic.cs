﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Umbraco.Extensions.Models.Generated;
using Umbraco.Extensions.Utilities;
using Umbraco.Web;

namespace Umbraco.Extensions.BLL
{
    public class CategoryLogic : BaseClass
    {
        public static IEnumerable<Category> GetAttractionCategories()
        {
            var settingsPage = CurrentPage.StartPage().SettingsPage as Settings;

            if (settingsPage != null)
            {
                if (settingsPage.AttractionCategoryStart != null)
                    return Umbraco.TypedContent(settingsPage.AttractionCategoryStart.Id).Children<Category>();
            }
            return new List<Category>();
        }

        public static IEnumerable<Category> GetAttractionCategoriesForWebApi()
        {
            var settingsPage = DomainLogic.GetSettingsPage(Umbraco);

            if (settingsPage != null)
            {
                if (settingsPage.AttractionCategoryStart != null)
                    return Umbraco.TypedContent(settingsPage.AttractionCategoryStart.Id).Children<Category>();
            }
            return new List<Category>();
        }

        public static IEnumerable<Category> GetServiceCategories()
        {
            var settingsPage = CurrentPage.StartPage().SettingsPage as Settings;

            if (settingsPage != null)
            {
                if (settingsPage.ServiceCategoryStart != null)
                    return Umbraco.TypedContent(settingsPage.ServiceCategoryStart.Id).Children<Category>();
            }
            return null;
        }

        public static IEnumerable<Category> GetServiceCategoriesForWebApi()
        {
            var settingsPage = DomainLogic.GetSettingsPage(Umbraco);

            if (settingsPage != null)
            {
                if (settingsPage.ServiceCategoryStart != null)
                    return Umbraco.TypedContent(settingsPage.ServiceCategoryStart.Id).Children<Category>();
            }
            return null;
        }

        public static IEnumerable<Category> GetExhibitionCategories()
        {
            var settingsPage = CurrentPage.StartPage().SettingsPage as Settings;

            if (settingsPage != null)
            {
                return Umbraco.TypedContent(settingsPage.ExhibitionCategoryStart.Id).Children<Category>();
            }
            return null;
        }

        public static IEnumerable<Category> GetExhibitionCategoriesForWebApi()
        {
            var settingsPage = DomainLogic.GetSettingsPage(Umbraco);

            if (settingsPage != null)
            {
                return Umbraco.TypedContent(settingsPage.ExhibitionCategoryStart.Id).Children<Category>();
            }
            return null;
        }

    }
}
