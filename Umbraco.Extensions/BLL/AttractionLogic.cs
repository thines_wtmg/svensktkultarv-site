﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Umbraco.Extensions.Models.Custom;
using Umbraco.Extensions.Models.Generated;
using Umbraco.Extensions.Utilities;

namespace Umbraco.Extensions.BLL
{
    public class AttractionLogic : BaseClass
    {
        public static OpeningHours AttractionOpeningHours()
        {
            var openingHours = new OpeningHours();
            var page = CurrentPage as Attraction;
            if (page != null)
            {
                openingHours.Monday.From = Helpers.GetTimeSpan(page.OpeningHoursMondayFrom);
                openingHours.Monday.To = Helpers.GetTimeSpan(page.OpeningHoursMondayTo);

                openingHours.Tuesday.From = Helpers.GetTimeSpan(page.OpeningHoursTuesdayFrom);
                openingHours.Tuesday.To = Helpers.GetTimeSpan(page.OpeningHoursTuesdayTo);

                openingHours.Wednesday.From = Helpers.GetTimeSpan(page.OpeningHoursWednesdayFrom);
                openingHours.Wednesday.To = Helpers.GetTimeSpan(page.OpeningHoursWednesdayTo);

                openingHours.Thursday.From = Helpers.GetTimeSpan(page.OpeningHoursThursdayFrom);
                openingHours.Thursday.To = Helpers.GetTimeSpan(page.OpeningHoursThursdayTo);

                openingHours.Friday.From = Helpers.GetTimeSpan(page.OpeningHoursFridayFrom);
                openingHours.Friday.To = Helpers.GetTimeSpan(page.OpeningHoursFridayTo);

                openingHours.Saturday.From = Helpers.GetTimeSpan(page.OpeningHoursSaturdayFrom);
                openingHours.Saturday.To = Helpers.GetTimeSpan(page.OpeningHoursSaturdayTo);

                openingHours.Sunday.From = Helpers.GetTimeSpan(page.OpeningHoursSundayFrom);
                openingHours.Sunday.To = Helpers.GetTimeSpan(page.OpeningHoursSundayTo);

                openingHours.OpeningHoursExceptionsHeading = page.HeadingOpeningHoursExceptions;

                openingHours.OpeningHoursGenericExceptions = page.OpeningHours != null ? page.OpeningHours.ToHtmlString() : string.Empty;

                return openingHours;
            }
            return null;
        }

    }
}