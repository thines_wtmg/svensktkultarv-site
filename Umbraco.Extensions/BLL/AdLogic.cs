﻿using System.Collections.Generic;
using Umbraco.Extensions.Models.Generated;
using Umbraco.Extensions.Utilities;
using Umbraco.Web;


namespace Umbraco.Extensions.BLL
{
    public class AdLogic: BaseClass
    {
        public static IEnumerable<Annons> GetAds()
        {
            var startpageAd = CurrentPage.StartPage().AnnonsContainer;

            if (startpageAd != null)
            {
                return Umbraco.TypedContent(startpageAd.Id).Children<Annons>();
            }
            return null;
        }
    }
}