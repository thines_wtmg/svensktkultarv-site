﻿using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web.WebPages;
using AttributeRouting;
using AttributeRouting.Web.Http;
using Umbraco.Extensions.Controllers.WebAPI.v1.Dto;
using Umbraco.Extensions.Models.Generated;
using Umbraco.Extensions.Utilities;
using umbraco.NodeFactory;
using Umbraco.Web;
using Umbraco.Web.Routing;
using Umbraco.Web.WebApi;
using Umbraco.Extensions.Models.Generated;

namespace Umbraco.Extensions
{
    public static class DomainLogic
    {
        //TODO:Fix this to support multilingual site, it just returns first startpage.
        public static Home GetHomePage(UmbracoHelper helper)
        {
            var startPage = helper.TypedContentAtRoot().DescendantsOrSelf<Home>().FirstOrDefault();
            return startPage;
        }

        //TODO:Fix this to support multilingual site.
        public static Settings GetSettingsPage(UmbracoHelper helper)
        {
            var settingsPage = helper.TypedContentAtRoot().DescendantsOrSelf<Home>().FirstOrDefault().SettingsPage as Settings;
            return settingsPage;
        }
    }
}
