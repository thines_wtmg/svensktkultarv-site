﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Umbraco.Core.Models;
using Umbraco.Extensions.Models.Custom;
using Umbraco.Extensions.Models.Generated;
using Umbraco.Extensions.Utilities;
using umbraco.NodeFactory;
using Umbraco.Web;

namespace Umbraco.Extensions.BLL
{
    public class ExhibitionLogic : BaseClass
    {

        public static IEnumerable<Exhibition> GetRelevantExhibitionsForAttraction()
        {
            return Umbraco.TypedContent(CurrentPage.Id).Children<Exhibition>().Where(x => x.StopDate >= DateTime.Now); ;
        }

        public static IEnumerable<Exhibition> GetRelevantExhibitions()
        {
            return Umbraco.TypedContent(CurrentPage.Id).Siblings<Exhibition>().Where(x => x.Id != CurrentPage.Id).Where(x=> x.StopDate >= DateTime.Now);
        }
        public static Attraction GetRelatedAttraction()
        {
            var parent = CurrentPage.Parent as Attraction;

            return parent;
        }

        //TODO:Null check or make this more robust.
        public static Attraction GetRelatedAttractionForWebApi(IPublishedContent node)
        {
            var parent = node.Parent as Attraction;

            return parent;
        }

        //TODO:Refactor
        public static OpeningHours AttractionOpeningHours()
        {
            var openingHours = new OpeningHours();
            var page = CurrentPage.Parent as Attraction;
            if (page != null)
            {
                openingHours.Monday.From = Helpers.GetTimeSpan(page.OpeningHoursMondayFrom);
                openingHours.Monday.To = Helpers.GetTimeSpan(page.OpeningHoursMondayTo);

                openingHours.Tuesday.From = Helpers.GetTimeSpan(page.OpeningHoursTuesdayFrom);
                openingHours.Tuesday.To = Helpers.GetTimeSpan(page.OpeningHoursTuesdayTo);

                openingHours.Wednesday.From = Helpers.GetTimeSpan(page.OpeningHoursWednesdayFrom);
                openingHours.Wednesday.To = Helpers.GetTimeSpan(page.OpeningHoursWednesdayTo);

                openingHours.Thursday.From = Helpers.GetTimeSpan(page.OpeningHoursThursdayFrom);
                openingHours.Thursday.To = Helpers.GetTimeSpan(page.OpeningHoursThursdayTo);

                openingHours.Friday.From = Helpers.GetTimeSpan(page.OpeningHoursFridayFrom);
                openingHours.Friday.To = Helpers.GetTimeSpan(page.OpeningHoursFridayTo);

                openingHours.Saturday.From = Helpers.GetTimeSpan(page.OpeningHoursSaturdayFrom);
                openingHours.Saturday.To = Helpers.GetTimeSpan(page.OpeningHoursSaturdayTo);

                openingHours.Sunday.From = Helpers.GetTimeSpan(page.OpeningHoursSundayFrom);
                openingHours.Sunday.To = Helpers.GetTimeSpan(page.OpeningHoursSundayTo);

                openingHours.OpeningHoursExceptionsHeading = page.HeadingOpeningHoursExceptions;

                openingHours.OpeningHoursGenericExceptions = page.OpeningHours != null ? page.OpeningHours.ToHtmlString() : string.Empty;

                return openingHours;
            }
            return null;
        }

    }
}
