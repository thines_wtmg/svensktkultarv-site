﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Umbraco.Extensions.Models.Generated;
using Umbraco.Extensions.Utilities;
using Umbraco.Web;

namespace Umbraco.Extensions.BLL
{
    public class GeoContainerLogic : BaseClass
    {
        public static IEnumerable<GeoContainer> GetGeoContainers()
        {
            return Umbraco.TypedContent(CurrentPage.StartPage().Id).Children<GeoContainer>();
        }

        public static IEnumerable<GeoContainer> GetGeoContainersForWebApi()
        {
            return Umbraco.TypedContentAtRoot().DescendantsOrSelf<GeoContainer>();
        }
    }
}