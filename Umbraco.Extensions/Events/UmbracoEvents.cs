﻿using System.Collections.Generic;
using System.Configuration;
using System.Drawing;
using DevTrends.MvcDonutCaching;
using Examine;
using ImageProcessor;
using ImageProcessor.Imaging;
using Newtonsoft.Json;
using Our.Umbraco.PropertyConverters;
using System;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Http;
using System.Web.Routing;
using Umbraco.Core;
using Umbraco.Core.Configuration;
using Umbraco.Core.Configuration.UmbracoSettings;
using Umbraco.Core.Events;
using Umbraco.Core.IO;
using Umbraco.Core.Logging;
using Umbraco.Core.Models;
using Umbraco.Core.Persistence;
using Umbraco.Core.PropertyEditors;
using Umbraco.Core.Publishing;
using Umbraco.Core.Services;
using Umbraco.Extensions.BLL;
using Umbraco.Extensions.Controllers;
using Umbraco.Extensions.Utilities;
using Umbraco.Extensions.Utilities.Cache;
using Umbraco.Web;
using Umbraco.Web.Models;
using Umbraco.Web.Mvc;
using UmbracoExamine;

namespace Umbraco.Extensions.Events
{
    public class UmbracoEvents : ApplicationEventHandler
    {
        private static ServiceContext Services
        {
            get { return UmbracoContext.Current.Application.Services; }
        }

        protected static UmbracoHelper Umbraco
        {
            get { return new UmbracoHelper(UmbracoContext.Current); }
        }

        protected override void ApplicationStarting(UmbracoApplicationBase umbracoApplication, ApplicationContext applicationContext)
        {
            //Events
            ContentService.Created += Content_New;
            ContentService.Saving += ContentService_Saving;
            ContentService.Saved += ContentService_Saved;
            ContentService.Published += Content_Published;
            ContentService.UnPublished += Content_Unpublished;
            ContentService.Moved += Content_Moved;
            ContentService.Trashed += Content_Trashed;
            ContentService.Deleted += Content_Deleted;
            MediaService.Saved += Media_Saved;
            MediaService.Saving += Media_Saving;

            //By registering this here we can make sure that if route hijacking doesn't find a controller it will use this controller.
            //That way each page will always be routed through one of our controllers.
            DefaultRenderMvcControllerResolver.Current.SetDefaultControllerType(typeof(DefaultController));

            //Remove the media picker property converters from the Umbraco Core Property Value Converters package.
            //These will be replaced by custom converters.
            PropertyValueConvertersResolver.Current.RemoveType<MediaPickerPropertyConverter>();
            PropertyValueConvertersResolver.Current.RemoveType<MultipleMediaPickerPropertyConverter>();

            //Add a web api handler. Here we can change the values from each web api call.
            GlobalConfiguration.Configuration.MessageHandlers.Add(new WebApiHandler());

            //With the url providers we can change node urls.
            //UrlProviderResolver.Current.InsertTypeBefore<DefaultUrlProvider, HomeUrlProvider>();
            //UrlProviderResolver.Current.InsertTypeBefore<DefaultUrlProvider, NewsUrlProvider>();

            //With the url segment provider we can change a segment of the url. No content finder is required for this because the node still matches the tree structure.
            //UrlSegmentProviderResolver.Current.InsertTypeBefore<DefaultUrlSegmentProvider, VacancyItemUrlSegmentProvider>();

            //With the content finder we can match nodes to urls.
            //ContentFinderResolver.Current.InsertTypeBefore<ContentFinderByNotFoundHandlers, NewsContentFinder>();

            RouteTable.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/v1/{controller}/{action}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );

            RouteTable.Routes.MapHttpRoute(
                name: "SvkDefaultScheduler",
                routeTemplate: "svk/{controller}/{action}/{token}",
                defaults: new { token = RouteParameter.Optional }
            );
        }

        protected override void ApplicationStarted(UmbracoApplicationBase umbracoApplication, ApplicationContext applicationContext)
        {
            //Get the Umbraco Database context
            var db = applicationContext.DatabaseContext.Database;

            //Check if the DB table does NOT exist
            if (!db.TableExist("CultureCard"))
            {
                //Create DB table - and set overwrite to false
                db.CreateTable<Extensions.Models.Rdbms.POCOS.CultureCard>(false);
            }

            //ExamineManager.Instance.IndexProviderCollection["ExternalIndexer"].GatheringNodeData += OnGatheringNodeData;
            ExamineManager.Instance.IndexProviderCollection["AttractionIndexer"].GatheringNodeData += AttracionIndexer_GatheringNodeData;
            ExamineManager.Instance.IndexProviderCollection["ExhibitionIndexer"].GatheringNodeData += ExhibitionIndexer_GatheringNodeData;
            PreRenderViewActionFilterAttribute.ActionExecuted += PreRenderViewActionFilterAttribute_ActionExecuted;
        }

        #region Events

        private void Content_New(IContentService sender, NewEventArgs<IContent> e)
        {
            //Set current date.
            if (e.Entity.HasProperty("currentDate"))
            {
                e.Entity.SetValue("currentDate", DateTime.Today);
            }
        }

        protected void ContentService_Saving(IContentService sender, SaveEventArgs<IContent> e)
        {
            foreach (var entity in e.SavedEntities)
            {
                //Set node name if the properties are empty. We can't do this in the Content_New event because we don't have the node name yet.
                if (entity.HasProperty("menuTitle") && string.IsNullOrWhiteSpace(entity.GetValue<string>("menuTitle")))
                {
                    entity.SetValue("menuTitle", entity.Name);
                }
                if (entity.HasProperty("title") && string.IsNullOrWhiteSpace(entity.GetValue<string>("title")))
                {
                    entity.SetValue("title", entity.Name);
                }
                if (entity.HasProperty("name") && string.IsNullOrWhiteSpace(entity.GetValue<string>("name")))
                {
                    entity.SetValue("name", entity.Name);
                }
            }
        }

        private void ContentService_Saved(IContentService sender, SaveEventArgs<IContent> e)
        {
            foreach (var entity in e.SavedEntities)
            {
                try
                {
                    if (entity.ContentType.Alias == "Gallery")
                    {
                        //Check if a media folder is already linked to the gallery.
                        bool hasContentOverride = ContentOverrideLogic.HasContentOverride(
                            "Gallery",
                            "images",
                            nodeId: entity.Id,
                            configAlias: "startNodeId"
                        );

                        if (!hasContentOverride)
                        {
                            //Create a media folder for the gallery.
                            var mediaFolder = Services.MediaService.CreateMedia(entity.Name, -1, "Folder");
                            Services.MediaService.Save(mediaFolder);

                            //Set the media folder as the start node for the Gallery images media picker.
                            ContentOverrideLogic.CreateContentOverride(
                                mediaFolder.Id.ToString(),
                                "Gallery",
                                "images",
                                nodeId: entity.Id,
                                configAlias: "startNodeId"
                            );
                        }
                    }
                }
                catch (Exception ex)
                {
                    Umbraco.LogException<UmbracoEvents>(ex);
                }
            }

            ClearCache();
        }

        private void Content_Published(IPublishingStrategy sender, PublishEventArgs<IContent> e)
        {
            ClearCache();
        }

        private void Content_Deleted(IContentService sender, DeleteEventArgs<IContent> e)
        {
            ClearCache();
        }

        private void Content_Trashed(IContentService sender, MoveEventArgs<IContent> e)
        {
            ClearCache();
        }

        private void Content_Moved(IContentService sender, MoveEventArgs<IContent> e)
        {
            ClearCache();
        }

        private void Content_Unpublished(IPublishingStrategy sender, PublishEventArgs<IContent> e)
        {
            ClearCache();
        }

        private void Media_Saved(IMediaService sender, SaveEventArgs<IMedia> e)
        {
            ClearCache();
        }

        private void Media_Saving(IMediaService sender, SaveEventArgs<IMedia> e)
        {
            var sizeMax = ConfigurationManager.AppSettings["MaxUploadWidth"];
            int outMax = 0;
            outMax = Int32.TryParse(sizeMax, out outMax) ? outMax : 1080;
            var mediaFileSystem = FileSystemProviderManager.Current.GetFileSystemProvider<MediaFileSystem>();
            IContentSection contentSection = UmbracoConfig.For.UmbracoSettings().Content;
            IEnumerable<string> supportedTypes = contentSection.ImageFileTypes.ToList();

            foreach (IMedia media in e.SavedEntities)
            {
                if (media.HasProperty("umbracoFile"))
                {
                    // Make sure it's an image.
                    string path = GetImagePath(media);
                    string extension = Path.GetExtension(path).Substring(1);
                    if (supportedTypes.InvariantContains(extension))
                    {
                        // Resize the image to 1920px wide, height is driven by the
                        // aspect ratio of the image.
                        string fullPath = mediaFileSystem.GetFullPath(path);
                        using (ImageFactory imageFactory = new ImageFactory(true))
                        {
                            ResizeLayer layer = new ResizeLayer(new Size(outMax, 0), null, ResizeMode.Max)
                            {
                                Upscale = false
                            };

                            imageFactory.Load(fullPath)
                                .Resize(layer)
                                .Save(fullPath);
                        }
                    }
                }

            }
        }
        private string GetImagePath(IMedia media)
        {
            var imagePath = media.GetValue<string>("umbracoFile");
            ImageCropDataSet cropDataSet = null;
            try
            {
                cropDataSet = JsonConvert.DeserializeObject<ImageCropDataSet>(imagePath);
            }
            catch (Exception ex)
            {
                return imagePath;
            }

            return cropDataSet.Src;
        }


        /// <summary>
        /// Handles the GatheringNodeData event of the InternalExamineEvents control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="IndexingNodeDataEventArgs"/> instance containing the event data.</param>
        private void AttracionIndexer_GatheringNodeData(object sender, IndexingNodeDataEventArgs e)
        {
            if (e.IndexType != IndexTypes.Content) return;

            // Create searchable path
            if (e.Fields.ContainsKey("path"))
            {
                e.Fields["searchPath"] = e.Fields["path"].Replace(',', ' ');
            }

            // Create searchable attractionCategories
            if (e.Fields.ContainsKey("attractionCategories"))
            {
                e.Fields["attractionCategories"] = e.Fields["attractionCategories"].Replace(',', ' ');
            }

            // Create searchable serviceCategories
            if (e.Fields.ContainsKey("serviceCategories"))
            {
                e.Fields["serviceCategories"] = e.Fields["serviceCategories"].Replace(',', ' ');
            }

            // Lowercase all the fields for case insensitive searching
            var keys = e.Fields.Keys.ToList();
            foreach (var key in keys)
            {
                e.Fields[key] = HttpUtility.HtmlDecode(e.Fields[key].ToLower(CultureInfo.InvariantCulture));
            }
        }

        /// <summary>
        /// Handles the GatheringNodeData event of the InternalExamineEvents control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="IndexingNodeDataEventArgs"/> instance containing the event data.</param>
        private void ExhibitionIndexer_GatheringNodeData(object sender, IndexingNodeDataEventArgs e)
        {
            if (e.IndexType != IndexTypes.Content) return;

            var contentService = new ContentService();

            // Create searchable path
            if (e.Fields.ContainsKey("path"))
            {
                e.Fields["searchPath"] = e.Fields["path"].Replace(',', ' ');
            }

            // Create searchable exhibitionCategories
            if (e.Fields.ContainsKey("exhibitionCategories"))
            {
                e.Fields["exhibitionCategories"] = e.Fields["exhibitionCategories"].Replace(',', ' ');
            }

            // Create searchable startDates
            if (e.Fields.ContainsKey("startDate"))
            {
                var date = DateTime.Parse(e.Fields["startDate"]);

                e.Fields["startDate"] = date.ToString("yyyyMMdd");
            }

            // Create searchable stopDates
            if (e.Fields.ContainsKey("stopDate"))
            {
                var date = DateTime.Parse(e.Fields["stopDate"]);

                e.Fields["stopDate"] = date.ToString("yyyyMMdd");
            }

            IContent node = contentService.GetById(e.NodeId);
            IContent parentNode = contentService.GetById(node.ParentId);

            //Gathering properties from parent, ie. the exhibitions related attraction.
            if (parentNode != null)
            {
                if (parentNode.ContentType.Alias == "Attraction")
                {
                    if (parentNode.HasProperty("city"))
                    {
                        var city = parentNode.GetValue("city");
                        e.Fields["city"] = city.ToString();
                    }

                    if (parentNode.HasProperty("onlyShowInApp"))
                    {
                        var onlyApp = parentNode.GetValue("onlyShowInApp");
                        e.Fields["onlyShowInApp"] = onlyApp.ToString();
                    }

                    if (parentNode.HasProperty("serviceCategories"))
                    {
                        var serviceCategories = parentNode.GetValue("serviceCategories");
                        e.Fields["serviceCategories"] = serviceCategories.ToString().Replace(',', ' ');
                    }
                }
            }

            // Lowercase all the fields for case insensitive searching
            var keys = e.Fields.Keys.ToList();
            foreach (var key in keys)
            {
                e.Fields[key] = HttpUtility.HtmlDecode(e.Fields[key].ToLower(CultureInfo.InvariantCulture));
            }
        }

        private void OnGatheringNodeData(object sender, IndexingNodeDataEventArgs e)
        {
            // Create searchable path
            if (e.Fields.ContainsKey("path"))
            {
                e.Fields["searchPath"] = e.Fields["path"].Replace(',', ' ');
            }

            // Lowercase all the fields for case insensitive searching
            var keys = e.Fields.Keys.ToList();
            foreach (var key in keys)
            {
                e.Fields[key] = HttpUtility.HtmlDecode(e.Fields[key].ToLower(CultureInfo.InvariantCulture));
            }

            // Extract the filename from media items
            if (e.Fields.ContainsKey("umbracoFile"))
            {
                e.Fields["umbracoFileName"] = Path.GetFileName(e.Fields["umbracoFile"]);
            }

            // Stuff all the fields into a single field for easier searching
            var combinedFields = new StringBuilder();
            foreach (var keyValuePair in e.Fields)
            {
                combinedFields.AppendLine(keyValuePair.Value);
            }
            e.Fields.Add("contents", combinedFields.ToString());
        }

        protected void PreRenderViewActionFilterAttribute_ActionExecuted(object sender, ActionExecutedEventArgs e)
        {
            //In this event it's possible to modify the model that will go the view. 
            //If we use a package that has it's own route hijacking (for example Articulate) we can still give it our own master model here.
        }

        #endregion

        #region Other

        private void ClearCache()
        {
            try
            {
                //Clear all output cache so everything is refreshed.
                var cacheManager = new OutputCacheManager();
                cacheManager.RemoveItems();

                //Clear memCache
                var memCacheManager = new MemoryCacher();
                memCacheManager.RemoveAll();

                //Clear the content finder cache.
                HttpContext.Current.Cache.Remove("CachedNewsNodes");
            }
            catch (Exception ex)
            {
                LogHelper.Error<UmbracoEvents>(string.Format("Exception: {0} - StackTrace: {1}", ex.Message, ex.StackTrace), ex);
            }
        }

        #endregion
    }
}