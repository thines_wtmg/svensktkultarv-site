﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DevTrends.MvcDonutCaching;
using Umbraco.Extensions.BLL;
using Umbraco.Extensions.Controllers.Base;
using Umbraco.Extensions.Models;
using Umbraco.Extensions.Models.Generated;
using Umbraco.Extensions.Models.Custom;

namespace Umbraco.Extensions.Controllers
{
    public class ExhibitionSearchController : SurfaceRenderMvcController
    {
        [DonutOutputCache(CacheProfile = "OneDay")]
        public ActionResult ExhibitionSearch()
        {
            var model = ModelLogic.CreateMasterModel() as MasterModel<ExhibitionSearch>;

            model.Content.Categories = new CategoriesViewModel
            {
                AttractionCategories = CategoryLogic.GetAttractionCategories(),
                ExhibitionCategories = CategoryLogic.GetExhibitionCategories(),
                ServiceCategories = CategoryLogic.GetServiceCategories()
            };
            model.Content.GeoContainers = GeoContainerLogic.GetGeoContainers();

            return CurrentTemplate(model);
        }
    }
}