﻿using DevTrends.MvcDonutCaching;
using System.Web.Mvc;
using Umbraco.Extensions.BLL;
using Umbraco.Extensions.Controllers.Base;
using Umbraco.Extensions.Models;
using Umbraco.Extensions.Models.Custom;
using Umbraco.Extensions.Models.Generated;

namespace Umbraco.Extensions.Controllers
{
    public class HomeController : SurfaceRenderMvcController
    {
        [DonutOutputCache(CacheProfile = "OneDay")]
        public ActionResult Home()
        {
            var model = ModelLogic.CreateMasterModel() as MasterModel<Home>;

            model.Content.Categories = new CategoriesViewModel
            {
                AttractionCategories = CategoryLogic.GetAttractionCategories(),
                ExhibitionCategories = CategoryLogic.GetExhibitionCategories(),
                ServiceCategories = CategoryLogic.GetServiceCategories()
            };
            model.Content.GeoContainers = GeoContainerLogic.GetGeoContainers();

            return CurrentTemplate(model);
        }
    }
}