﻿using System.Runtime.Remoting.Messaging;
using DevTrends.MvcDonutCaching;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Umbraco.Core;
using Umbraco.Extensions.BLL;
using Umbraco.Extensions.Controllers.Base;
using Umbraco.Extensions.Models;
using Umbraco.Extensions.Models.Generated;
using Umbraco.Extensions.Utilities;
using Umbraco.Web;

namespace Umbraco.Extensions.Controllers
{
    public class AttractionController : SurfaceRenderMvcController
    {
        [DonutOutputCache(CacheProfile = "OneDay")]
        public ActionResult Attraction()
        {
            var model = ModelLogic.CreateMasterModel() as MasterModel<Attraction>;
            model.Content.RelevantExhibitions = ExhibitionLogic.GetRelevantExhibitionsForAttraction();
            model.Content.OrdinaryOpeningHours = AttractionLogic.AttractionOpeningHours();

            return CurrentTemplate(model);
        }

        [ChildActionOnly]
        public ActionResult IsOpen(int? id)
        {
            if (id != null)
            {
                var openingHours = AttractionLogic.AttractionOpeningHours();
                return Content(Helpers.IsOpen(openingHours) == true ? "<span class=\" \">Öppet idag " + Helpers.GetTodaysOpeningHours(openingHours) + "</span>" : "");
            }
            return Content("null");
        }
        

    }
}
