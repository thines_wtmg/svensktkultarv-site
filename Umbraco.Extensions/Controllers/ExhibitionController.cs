﻿using DevTrends.MvcDonutCaching;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Umbraco.Core;
using Umbraco.Extensions.Controllers.Base;
using Umbraco.Extensions.BLL;
using Umbraco.Extensions.Models;
using Umbraco.Extensions.Models.Generated;
using Umbraco.Extensions.Utilities;
using Umbraco.Web;

namespace Umbraco.Extensions.Controllers
{
    public class ExhibitionController : SurfaceRenderMvcController
    {
        [DonutOutputCache(CacheProfile = "OneDay")]
        public ActionResult Exhibition()
        {
            var model = ModelLogic.CreateMasterModel() as MasterModel<Exhibition>;
            model.Content.RelatedAttraction = ExhibitionLogic.GetRelatedAttraction();
            model.Content.RelevantExhibitions = ExhibitionLogic.GetRelevantExhibitions();

            model.Content.OrdinaryOpeningHours = ExhibitionLogic.AttractionOpeningHours();

            return CurrentTemplate(model);
        }

        [DonutOutputCache(CacheProfile = "OneDay")]
        [ChildActionOnly]
        public ActionResult IsOpen(int? id)
        {
            if (id != null)
            {
                var openingHours = ExhibitionLogic.AttractionOpeningHours();
                return
                    Content(Helpers.IsOpen(openingHours) == true ? "<span class=\" \">Öppet idag " + Helpers.GetTodaysOpeningHours(openingHours) + "</span>" : "");
            }
            return Content("null");
        }
    }
}
