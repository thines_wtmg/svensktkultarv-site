﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Reflection;
using System.Runtime.Caching;
using System.Web.Http;
using AttributeRouting;
using log4net;
using Umbraco.Extensions.BLL;
using Umbraco.Web.WebApi;

namespace Umbraco.Extensions.Controllers.WebAPI
{
    [RoutePrefix("svk")]
    public class ScheduleController : UmbracoApiController
    {
        private static readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        private const string Prefix = "999";

        // GET svk/<controller>
        [HttpGet]
        public int PrefixExpiredCulturCards(string token)
        {
            if (string.Equals("u2nozwr1bgvkb2jgb3jbcmnoaxzpbmdfehbpcmvkq3vsdhvyzunhcmrzmtiziq", token, StringComparison.InvariantCultureIgnoreCase))
            {
                var expiredCards = CultureCardLogic.GetAllExpired().ToList();
                Log.InfoFormat("Archiving {0} expired culture cards", expiredCards.Count());

                //Get the Umbraco db
                var db = ApplicationContext.DatabaseContext.Database;

                foreach (var card in expiredCards)
                {
                    var unPrefixed = card.PhoneNumber;
                    var prefixedNumber = Prefix + card.PhoneNumber;

                    try
                    {
                        card.PhoneNumber = prefixedNumber;
                        card.ExpireDate = new DateTime(DateTime.Now.Year + 20, DateTime.Now.Month, DateTime.Now.Day, 0, 0, 0, DateTimeKind.Utc);
                        
                        //Add the object to the DB
                        db.Update(card);
                        Log.InfoFormat("Card number prefixed, phonenumber {0} is now prefixed like:{1}", unPrefixed, card.PhoneNumber);
                    }
                    catch (Exception ex)
                    {
                        Log.ErrorFormat("Could not update expired culture card with prefix, phonenumber:{0}, error:{1} ", card.PhoneNumber, ex.Message);
                    }

                }

                return expiredCards.Count;
            }

            return -1;
        }
    }
}