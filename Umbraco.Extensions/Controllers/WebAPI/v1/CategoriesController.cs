﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AttributeRouting;
using Umbraco.Core.Services;
using Umbraco.Extensions.Controllers.WebAPI.v1.Dto;
using Umbraco.Extensions.Controllers.WebAPI.v1.Mobil.Dto;
using Umbraco.Extensions.Models.Generated;
using Umbraco.Extensions.Utilities.Cache;
using Umbraco.Web;
using Umbraco.Web.Mvc;
using Umbraco.Web.WebApi;

namespace Umbraco.Extensions.Controllers.WebAPI.V1
{
    [RoutePrefix("v1")]
    public class CategoriesController : UmbracoApiController
    {
        /// <summary>
        /// Get all categories
        /// </summary>
        /// <returns></returns>
        [CacheControl(MaxAge = 100)]
        public IEnumerable<CategoryDto> GetAll()
        {
            var category = Umbraco.TypedContentAtRoot().DescendantsOrSelf<Category>().Select(c => new CategoryDto()
            {
                ID = c.Id,
                Name = c.Name
            });

            return category;
        }
    }
}