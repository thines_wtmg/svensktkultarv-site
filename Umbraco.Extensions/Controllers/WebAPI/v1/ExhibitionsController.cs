﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Headers;
using System.Web.Routing;
using AttributeRouting;
using Umbraco.Extensions.Controllers.WebAPI.v1.Dto;
using Umbraco.Extensions.Models.Generated;
using Umbraco.Extensions.Utilities;
using Umbraco.Extensions.Utilities.Cache;
using Umbraco.Extensions.Utilities.Services;
using Umbraco.Web;
using Umbraco.Web.Mvc;
using Umbraco.Web.WebApi;
using Umbraco.Extensions.BLL;
using System.Globalization;
using System.Net.Http;
using System.Net;
using System.Text;
using Examine;
using Examine.SearchCriteria;

namespace Umbraco.Extensions.Controllers.WebAPI.V1
{
    [RoutePrefix("v1")]
    public class ExhibitionsController : UmbracoApiController
    {
        private readonly MemoryCacher _memCacheManager;
        private readonly ExamineSearchService _examineSearchService;
        private readonly CultureInfo _svCulture = new CultureInfo("sv-SE");

        public ExhibitionsController() : this(new MemoryCacher(), new ExamineSearchService())
        {

        }

        private ExhibitionsController(MemoryCacher memCacheManager, ExamineSearchService examineSearchService)
        {

            _memCacheManager = memCacheManager;
            _examineSearchService = examineSearchService;
        }
        /// <summary>
        /// Get exhibition by it's ID
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public HttpResponseMessage GetById(int id)
        {
            var exhibition = Umbraco.TypedContent(id) as Exhibition;
            if (exhibition != null)
            {
                var exhibitionDto = new ExhibitionForHomeDto
                {
                    Id = exhibition.Id,
                    Name = exhibition.Name,
                    FK_RelatedAttraction = ExhibitionLogic.GetRelatedAttractionForWebApi(exhibition).Id,
                    RelatedAttraction = ExhibitionLogic.GetRelatedAttractionForWebApi(exhibition).Name,
                    Link = exhibition.Url,
                    City = ExhibitionLogic.GetRelatedAttractionForWebApi(exhibition).City ?? "",
                    Lat = ExhibitionLogic.GetRelatedAttractionForWebApi(exhibition).Location != null ? ExhibitionLogic.GetRelatedAttractionForWebApi(exhibition).Location.Lat : -1,
                    Lng = ExhibitionLogic.GetRelatedAttractionForWebApi(exhibition).Location != null ? ExhibitionLogic.GetRelatedAttractionForWebApi(exhibition).Location.Lng : -1,
                    MainImage = exhibition.MainImage != null ? exhibition.MainImage.Cropper.GetCropUrl(270, 240, null, 75) : string.Empty,
                    Date = Helpers.GetFormatedStartStopDates(exhibition.StartDate, exhibition.StopDate, "sv-SE")
                };
                HttpResponseMessage r = new HttpResponseMessage();
                
                return Request.CreateResponse(HttpStatusCode.OK, exhibitionDto);
            }
            return Request.CreateErrorResponse(HttpStatusCode.NotFound, string.Format("Could not find attraction with id:{0}", id));
        }

        /// <summary>
        /// Get exhibitions by parent attraction. -> All childs thats of exhibition type. using this for instance when ajax loading all exhibions when clicking on google maps marker.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [CacheControl(MaxAge = 100)]
        public IEnumerable<ExhibitionForHomeDto> GetByAttraction(int id)
        {
            var parentAttraction = Umbraco.TypedContent(id) as Attraction;
            if (parentAttraction != null)
            {
                var exhibitions = parentAttraction.Children<Exhibition>().Select(e => new ExhibitionForHomeDto
                {
                    Id = e.Id,
                    Name = e.Name,
                    RelatedAttraction = parentAttraction.Name,
                    Link = e.Url,
                    City = parentAttraction.City ?? "",
                    Lat = parentAttraction.Location != null ? parentAttraction.Location.Lat : -1,
                    Lng = parentAttraction.Location != null ? parentAttraction.Location.Lng : -1,
                    Date = Helpers.GetFormatedStartStopDates(e.StartDate, e.StopDate, "sv-SE")
                });

                return exhibitions;
            }
            return new List<ExhibitionForHomeDto>();
        }


        /// <summary>
        /// Get all published attractions by Geo Container for Home page.
        /// </summary>
        /// <param name="initialPageSize"></param>
        /// <param name="pageSize"></param>
        /// <param name="page"></param>
        /// <returns></returns>
        [CacheControl(MaxAge = 100)]
        public ExhibitionsForHomeResult GetAllForHome(int initialPageSize = 7, int pageSize = 8, int page = 0)
        {
            var result = _memCacheManager.GetValue(_memCacheManager.GetSimpleCacheKeyFromPageSize(v1.Settings.CacheKeys.AllExhibitions, initialPageSize));

            //Check if cached and Never cache paged results
            if (result == null || page != 0)
            {
                //Not from cache.
                var allExhibitionsHomeResult = GetAllExhibitionsHomeResult(Umbraco, initialPageSize, pageSize, page);

                //Add to cache. but never cache paged results, only page 0 but with different pagesizes
                if (allExhibitionsHomeResult != null && page == 0)
                {
                    _memCacheManager.Add(_memCacheManager.GetSimpleCacheKeyFromPageSize(v1.Settings.CacheKeys.AllExhibitions, initialPageSize), allExhibitionsHomeResult, DateTimeOffset.UtcNow.AddHours(v1.Settings.CacheTime.TwelveHours));
                }

                return allExhibitionsHomeResult;
            }
            else
            {
                //Cached response
                return (ExhibitionsForHomeResult)result;
            }

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="nodeIds"></param>
        /// <param name="exhibitionCategories"></param>
        /// <param name="serviceCategories"></param>
        /// <param name="startDate"></param>
        /// <param name="stopDate"></param>
        /// <param name="pageSize"></param>
        /// <param name="page"></param>
        /// <returns></returns>
        [CacheControl(MaxAge = 100)]
        public ExhibitionsForHomeResult GetFiltered(string nodeIds, string exhibitionCategories, string serviceCategories, string startDate, string stopDate, int initialPageSize = 7, int pageSize = 8, int page = 0)
        {
            var parentIds = Helpers.ConvertCommaSeparatedIntStringToStringArray(nodeIds);
            var exhibitionCats = Helpers.ConvertCommaSeparatedIntStringToStringArray(exhibitionCategories);
            var serviceCats = Helpers.ConvertCommaSeparatedIntStringToStringArray(serviceCategories);

            var exhibitions = Umbraco.TypedSearch(_examineSearchService.GetExhibitionSearchCriteria(parentIds, exhibitionCats, serviceCats, startDate, stopDate), ExamineManager.Instance.SearchProviderCollection["ExhibitionSearcher"]).Cast<Exhibition>().Where(e => e.StopDate >= DateTime.Now).OrderBy(e => e.Name, StringComparer.Create(_svCulture, false)).Select(e => new ExhibitionForHomeDto()
            {
                Id = e.Id,
                FK_RelatedAttraction = ExhibitionLogic.GetRelatedAttractionForWebApi(e).Id,
                Name = e.Name,
                RelatedAttraction = ExhibitionLogic.GetRelatedAttractionForWebApi(e).Name,
                Link = e.Url,
                City = ExhibitionLogic.GetRelatedAttractionForWebApi(e).City ?? "",
                Lat = ExhibitionLogic.GetRelatedAttractionForWebApi(e).Location != null ? ExhibitionLogic.GetRelatedAttractionForWebApi(e).Location.Lat : -1,
                Lng = ExhibitionLogic.GetRelatedAttractionForWebApi(e).Location != null ? ExhibitionLogic.GetRelatedAttractionForWebApi(e).Location.Lng : -1,
                MainImage = e.MainImage != null ? e.MainImage.Cropper.GetCropUrl(270, 240, null, 75) : string.Empty,
                Date = Helpers.GetFormatedStartStopDates(e.StartDate, e.StopDate, "sv-SE")
            });

            //Avoiding multiple enumeration when counting total number found and number returned
            var exhibitionsForHomeDtos = exhibitions as IList<ExhibitionForHomeDto> ?? exhibitions.ToList();
            var totalNumberOfExhibitions = exhibitionsForHomeDtos.Count();
            var slicedExhibitions = exhibitionsForHomeDtos.Skip(page * initialPageSize).Take(initialPageSize);
            var singlePageOfExhibitions = slicedExhibitions as IList<ExhibitionForHomeDto> ?? slicedExhibitions.ToList();

            return (new ExhibitionsForHomeResult
            {
                Total = totalNumberOfExhibitions,
                Count = singlePageOfExhibitions.Count(),
                CurrentPage = page,
                Exhibitions = singlePageOfExhibitions,
                PageSize = initialPageSize
            });
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="helper"></param>
        /// <param name="page"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        private ExhibitionsForHomeResult GetAllExhibitionsHomeResult(UmbracoHelper helper, int initialPageSize = 7, int pageSize = 8, int page = 0)
        {
            var exhibitions = Umbraco.TypedSearch(_examineSearchService.GetAllExhibitionsSearchCriteria(), ExamineManager.Instance.SearchProviderCollection["ExhibitionSearcher"]).Cast<Exhibition>().Where(e => e.StopDate >= DateTime.Now).OrderBy(e => e.Name, StringComparer.Create(_svCulture, false)).Select(e => new ExhibitionForHomeDto()
            {
                Id = e.Id,
                FK_RelatedAttraction = ExhibitionLogic.GetRelatedAttractionForWebApi(e).Id,
                Name = e.Name,
                RelatedAttraction = ExhibitionLogic.GetRelatedAttractionForWebApi(e).Name,
                Link = e.Url,
                City = ExhibitionLogic.GetRelatedAttractionForWebApi(e).City ?? "",
                Lat = ExhibitionLogic.GetRelatedAttractionForWebApi(e).Location != null ? ExhibitionLogic.GetRelatedAttractionForWebApi(e).Location.Lat : -1,
                Lng = ExhibitionLogic.GetRelatedAttractionForWebApi(e).Location != null ? ExhibitionLogic.GetRelatedAttractionForWebApi(e).Location.Lng : -1,
                MainImage = e.MainImage != null ? e.MainImage.Cropper.GetCropUrl(270, 240, null, 75) : string.Empty,
                Date = Helpers.GetFormatedStartStopDates(e.StartDate, e.StopDate, "sv-SE")

            });

            //Avoiding multiple enumeration when counting total number found and number returned
            var exhibitionsForHomeDtos = exhibitions as IList<ExhibitionForHomeDto> ?? exhibitions.ToList();
            var totalNumberOfExhibitions = exhibitionsForHomeDtos.Count();
            var slicedExhibitions = exhibitionsForHomeDtos.Skip(page * initialPageSize).Take(initialPageSize);
            var singlePageOfExhibitions = slicedExhibitions as IList<ExhibitionForHomeDto> ?? slicedExhibitions.ToList();

            return (new ExhibitionsForHomeResult
            {
                Total = totalNumberOfExhibitions,
                Count = singlePageOfExhibitions.Count(),
                CurrentPage = page,
                Exhibitions = singlePageOfExhibitions,
                PageSize = initialPageSize
            });
        }
    }
}