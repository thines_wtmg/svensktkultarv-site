﻿namespace Umbraco.Extensions.Controllers.WebAPI.v1.Dto
{
    public class SearchDto
    {
        public string Id { get; set; }
        public string label { get; set; }
        public string city { get; set; }
        public string link { get; set; }
        public string value { get; set; }
        public string type { get; set; }
    }
}