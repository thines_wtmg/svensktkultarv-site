﻿namespace Umbraco.Extensions.Controllers.WebAPI.v1.Dto
{
    public class BaseResult 
    {
        public int Total { get; set; }
        public int Count { get; set; }
        public int PageSize { get; set; }
        public int CurrentPage { get; set; }
    }
}