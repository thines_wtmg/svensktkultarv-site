﻿using System.Collections.Generic;

namespace Umbraco.Extensions.Controllers.WebAPI.v1.Dto
{
    public class AttractionsResult : BaseResult
    {
        public IEnumerable<AttractionDto> Attractions { get; set; }
    }
}