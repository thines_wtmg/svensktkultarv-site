﻿namespace Umbraco.Extensions.Controllers.WebAPI.v1.Dto
{
    public class AttractionForHomeDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string City { get; set; }
        public string Link { get; set; }
        public double Lat { get; set; }
        public double Lng { get; set; }
        public string MainImage { get; set; }
    }
}