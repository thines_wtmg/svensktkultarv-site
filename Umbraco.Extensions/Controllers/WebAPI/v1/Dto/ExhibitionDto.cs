﻿using System;

namespace Umbraco.Extensions.Controllers.WebAPI.v1.Dto
{
    public class ExhibitionDto 
    {
        public int ID { get; set; }
        public int FK_RelatedAttraction { get; set; }
        public string Name { get; set; }
        public string Preamble { get; set; }
        public string MainBody { get; set; }
        public string MainImage { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime StopDate { get; set; }
    }
}