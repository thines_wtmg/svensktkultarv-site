﻿namespace Umbraco.Extensions.Controllers.WebAPI.v1.Dto
{
    public class CategoryMobilDto 
    {
        public int ID { get; set; } 
        public string Name { get; set; }
    }
}