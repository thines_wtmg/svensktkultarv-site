﻿using System.Collections.Generic;

namespace Umbraco.Extensions.Controllers.WebAPI.v1.Dto
{
    public class AttractionsForHomeResult : BaseResult
    {
        public IEnumerable<AttractionForHomeDto> Attractions { get; set; }
    }
}