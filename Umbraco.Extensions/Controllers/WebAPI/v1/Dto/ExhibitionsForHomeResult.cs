﻿using System.Collections.Generic;

namespace Umbraco.Extensions.Controllers.WebAPI.v1.Dto
{
    public class ExhibitionsForHomeResult : BaseResult
    {
        public IEnumerable<ExhibitionForHomeDto> Exhibitions { get; set; }
    }
}