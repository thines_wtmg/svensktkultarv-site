﻿using System;
using System.Collections.Generic;

namespace Umbraco.Extensions.Controllers.WebAPI.v1.Mobil.Dto
{
    public class ExhibitionMobilDto 
    {
        public int ID { get; set; }
        public int FK_RelatedAttraction { get; set; }
        public string Name { get; set; }
        public string Link { get; set; }
        public string Preamble { get; set; }
        public string MainBody { get; set; }
        public MainImageInfo MainImage { get; set; }
        public IEnumerable<int> Categories { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime StopDate { get; set; }
    }
}