﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Umbraco.Extensions.Controllers.WebAPI.v1.Mobil.Dto
{
    public class BaseMobilDto
    {
        public DateTime Generated { get; set; }
    }
}