﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Umbraco.Extensions.Controllers.WebAPI.v1.Mobil.Dto
{
    public class SyncRequest
    {
        public DateTime SyncDate { get; set; }
        public IEnumerable<int> Attractions { get; set; }
        public IEnumerable<int> Exhibitions { get; set; }
    }
}