﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Umbraco.Extensions.Controllers.WebAPI.v1.Mobil.Dto
{
    public class CultureCardDto
    {
        public int Id { get; set; }
        public string PhoneNumber { get; set; }
        public string DeviceId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string ZipCode { get; set; }
        public string Expires { get; set; }
        
    }
}