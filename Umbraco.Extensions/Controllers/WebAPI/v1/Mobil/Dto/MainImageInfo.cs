﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Umbraco.Extensions.Controllers.WebAPI.v1.Mobil.Dto
{
    public class MainImageInfo
    {
        public string Url { get; set; }
        public string Width { get; set; }
        public string Height { get; set; }
    }
}