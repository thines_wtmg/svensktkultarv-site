﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Umbraco.Extensions.Controllers.WebAPI.v1.Dto;

namespace Umbraco.Extensions.Controllers.WebAPI.v1.Mobil.Dto
{
    public class SyncResponse
    {
        public List<AttractionMobilDto> UpdatedAttractions { get; set; }
        public List<AttractionMobilDto> NewAttractions { get; set; }
        public List<int> DeletedAttractions { get; set; }
        public List<ExhibitionMobilDto> UpdatedExhibitions { get; set; }
        public List<ExhibitionMobilDto> NewExhibitions { get; set; }
        public List<int> DeletedExhibitions { get; set; }
        public List<CategoryMobilDto> ServiceCategories { get; set; }
        public List<CategoryMobilDto> AttractionCategories { get; set; }
        public List<CategoryMobilDto> ExhibitionCategories { get; set; }
        public List<CountyMobilDto> Counties { get; set; }
    }
}