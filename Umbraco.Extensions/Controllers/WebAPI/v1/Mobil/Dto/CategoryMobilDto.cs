﻿namespace Umbraco.Extensions.Controllers.WebAPI.v1.Mobil.Dto
{
    public class CategoryDto 
    {
        public int ID { get; set; } 
        public string Name { get; set; }
    }
}