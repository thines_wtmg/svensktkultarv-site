﻿namespace Umbraco.Extensions.Controllers.WebAPI.v1.Mobil.Dto
{
    public class CountyMobilDto
    {
        public int ID { get; set; }
        public string Name { get; set; }
    }
}