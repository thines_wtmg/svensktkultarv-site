﻿using System.Collections.Generic;
using Umbraco.Extensions.Models.Custom;

namespace Umbraco.Extensions.Controllers.WebAPI.v1.Mobil.Dto
{
    public class AttractionMobilDto 
    {
        public int ID { get; set; }
        public int CountyId { get; set; }
        public string Link { get; set; }
        public string Name { get; set; }
        public string Preamble { get; set; }
        public string MainBody { get; set; }
        public double Lat { get; set; }
        public double Lng { get; set; }
        public ContactInformation Contact { get; set; }
        public OpeningHours OpeningHours { get; set; }
        public string Price { get; set; }
        public string CardDiscount { get; set; }
        public IEnumerable<int> Categories { get; set; }
        public bool Sponsored { get; set; }
        public IEnumerable<int> ServiceCategories { get; set; } 
        public MainImageInfo MainImage { get; set; }
    }
}