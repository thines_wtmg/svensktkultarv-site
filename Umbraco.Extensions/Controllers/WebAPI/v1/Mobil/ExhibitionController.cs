﻿using System;
using System.Collections.Generic;
using System.Linq;
using AttributeRouting;
using AttributeRouting.Web.Http;
using Umbraco.Extensions.Controllers.WebAPI.v1.Mobil.Dto;
using Umbraco.Extensions.Models.Generated;
using Umbraco.Extensions.Utilities;
using Umbraco.Extensions.Utilities.Cache;
using Umbraco.Extensions.Utilities.Security;
using Umbraco.Web;
using Umbraco.Web.WebApi;

namespace Umbraco.Extensions.Controllers.WebAPI.v1.Mobil
{
    [RoutePrefix("api")]
    [ReturnsJson]
    [MobilTokenAuthorization]
    public class ExhibitionController : UmbracoApiController
    {

        private readonly MemoryCacher _memCacheManager;
        private readonly JsonDiskCache _jsonDiskCache;

        public ExhibitionController() : this(new MemoryCacher(), new JsonDiskCache())
        {

        }

        private ExhibitionController(MemoryCacher memCacheManager, JsonDiskCache jsonDiskCache)
        {

            _memCacheManager = memCacheManager;
            _jsonDiskCache = jsonDiskCache;
        }

        /// <summary>
        /// Get all published exhibitions
        /// </summary>
        /// <returns></returns>
        [GET("v1/mobil/exhibitions/")]
        public List<ExhibitionMobilDto> GetAll()
        {
            var exhibitionJsonCacheKey = v1.Settings.CacheKeys.AllMobilExhibitions;

            var cachedJson = _jsonDiskCache.Get<List<ExhibitionMobilDto>>(exhibitionJsonCacheKey);

            if (cachedJson != null)
            {
                return cachedJson;
            }

            var exhibitions =
                Umbraco.TypedContentAtRoot().DescendantsOrSelf<Exhibition>().Where(e => e.StopDate >= DateTime.Now).Select(x => new ExhibitionMobilDto()
                {
                    ID = x.Id,
                    FK_RelatedAttraction = x.Parent.Id,
                    Name = x.Name,
                    Link = x.Url,
                    Preamble = x.Preamble as string,
                    MainBody = x.MainBody.ToHtmlString(),
                    MainImage = new MainImageInfo
                    {
                        Url = x.MainImage != null ? x.MainImage.Cropper.GetCropUrl(640, 480, null, 75) : string.Empty,
                        Width = x.MainImage != null ? x.MainImage.Width.ToString() : "-1",
                        Height = x.MainImage != null ? x.MainImage.Height.ToString() : "-1"
                    },
                    StartDate = x.StartDate,
                    StopDate = x.StopDate,
                    Categories = x.ExhibitionCategories.Select(c => c.Id)

                }).ToList();

            _jsonDiskCache.Add(exhibitionJsonCacheKey, exhibitions);

            return exhibitions;

        }

        /// <summary>
        /// Get all published exhibitions
        /// </summary>
        /// <returns></returns>
        [GET("v1/mobil/exhibition/{id}")]
        public ExhibitionMobilDto GetById(int id)
        {
            var exhibitionPage = Umbraco.TypedContent(id) as Exhibition;
            if (exhibitionPage != null)
            {
                var exhibition = new ExhibitionMobilDto()
                {
                    ID = exhibitionPage.Id,
                    FK_RelatedAttraction = exhibitionPage.Parent.Id,
                    Name = exhibitionPage.Name,
                    Link = exhibitionPage.Url,
                    Preamble = exhibitionPage.Preamble as string,
                    MainBody = exhibitionPage.MainBody.ToHtmlString(),
                    MainImage = new MainImageInfo
                    {
                        Url = exhibitionPage.MainImage != null ? exhibitionPage.MainImage.Cropper.GetCropUrl(640, 480, null, 75) : string.Empty,
                        Width = exhibitionPage.MainImage != null ? exhibitionPage.MainImage.Width.ToString() : "-1",
                        Height = exhibitionPage.MainImage != null ? exhibitionPage.MainImage.Height.ToString() : "-1"
                    },
                    StartDate = exhibitionPage.StartDate,
                    StopDate = exhibitionPage.StopDate,
                    Categories = exhibitionPage.ExhibitionCategories.Select(c => c.Id)
                };

                return exhibition;
            }
            return null;
        }
    }
}