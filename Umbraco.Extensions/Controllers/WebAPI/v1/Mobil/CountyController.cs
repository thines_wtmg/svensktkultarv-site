﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using AttributeRouting;
using AttributeRouting.Web.Http;
using AutoMapper.Internal;
using Examine;
using Examine.SearchCriteria;
using Umbraco.Extensions.BLL;
using Umbraco.Extensions.Controllers.WebAPI.v1.Dto;
using Umbraco.Extensions.Controllers.WebAPI.v1.Mobil.Dto;
using Umbraco.Extensions.Models.Custom;
using Umbraco.Extensions.Models.Generated;
using Umbraco.Extensions.Utilities;
using Umbraco.Extensions.Utilities.Security;
using Umbraco.Web;
using Umbraco.Web.WebApi;

namespace Umbraco.Extensions.Controllers.WebAPI.v1.Mobil
{
    [RoutePrefix("api")]
    [ReturnsJson]
    [MobilTokenAuthorization]
    public class CountyController : UmbracoApiController
    {

        /// <summary>
        /// Get all GeoContainers ie, counties
        /// </summary>
        /// <returns></returns>
        [GET("v1/mobil/counties/")]
        public List<CountyMobilDto> GetAll()
        {
            var counties = Umbraco.TypedContentAtRoot().DescendantsOrSelf<GeoContainer>().Select(a => new CountyMobilDto
            {
                ID = a.Id,
                Name = a.Name
            }).ToList();

            return counties;
        }
    }
}