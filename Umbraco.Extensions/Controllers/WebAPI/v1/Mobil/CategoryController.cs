﻿using System.Collections.Generic;
using System.Linq;
using System.Web.WebPages;
using AttributeRouting;
using AttributeRouting.Web.Http;
using Umbraco.Extensions.BLL;
using Umbraco.Extensions.Controllers.WebAPI.v1.Dto;
using Umbraco.Extensions.Models.Generated;
using Umbraco.Extensions.Utilities.Security;
using umbraco.NodeFactory;
using Umbraco.Web;
using Umbraco.Web.Routing;
using Umbraco.Web.WebApi;

namespace Umbraco.Extensions.Controllers.WebAPI.v1.Mobil
{
    [RoutePrefix("api")]
    [ReturnsJson]
    [MobilTokenAuthorization]
    public class CategoryController : UmbracoApiController
    {
        /// <summary>
        /// Get all categories
        /// </summary>
        /// <returns></returns>
        [GET("v1/mobil/categories/")]
        public List<CategoryMobilDto> GetAll()
        {
            var categories = Umbraco.TypedContentAtRoot().DescendantsOrSelf<Category>().Select(c => new CategoryMobilDto()
            {
                ID = c.Id,
                Name = c.Name
            }).ToList();

            return categories;
        }

        /// <summary>
        /// Get all service categories
        /// </summary>
        /// <returns></returns>
        [GET("v1/mobil/servicecategories/")]
        public List<CategoryMobilDto> GetAllServiceCategories()
        {
            var categories = CategoryLogic.GetServiceCategoriesForWebApi().Select(c => new CategoryMobilDto()
            {
                ID = c.Id,
                Name = c.Name
            }).ToList();

            return categories;
        }

        /// <summary>
        /// Get all attraction categories
        /// </summary>
        /// <returns></returns>
        [GET("v1/mobil/attractioncategories/")]
        public List<CategoryMobilDto> GetAllAttractionCategories()
        {
            var categories = CategoryLogic.GetAttractionCategoriesForWebApi().Select(c => new CategoryMobilDto()
            {
                ID = c.Id,
                Name = c.Name
            }).ToList();

            return categories;
        }

        /// <summary>
        /// Get all exhibition categories
        /// </summary>
        /// <returns></returns>
        [GET("v1/mobil/exhibitioncategories/")]
        public List<CategoryMobilDto> GetAllExhibitionCategories()
        {
            var categories = CategoryLogic.GetExhibitionCategoriesForWebApi().Select(c => new CategoryMobilDto()
            {
                ID = c.Id,
                Name = c.Name
            }).ToList();

            return categories;
        }
    }
}