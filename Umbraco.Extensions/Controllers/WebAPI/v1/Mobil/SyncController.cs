﻿using System;
using System.Collections.Generic;
using System.Web.Http;
using AttributeRouting;
using AttributeRouting.Web.Http;
using Umbraco.Extensions.Controllers.WebAPI.v1.Dto;
using Umbraco.Extensions.Controllers.WebAPI.v1.Mobil.Dto;
using Umbraco.Web.WebApi;

namespace Umbraco.Extensions.Controllers.WebAPI.v1.Mobil
{
    /// <summary>
    /// Test Controller, not used in production.
    /// </summary>
    [RoutePrefix("api")]
    [ReturnsJson]
    public class SyncController : UmbracoApiController
    {
        [POST("v1/mobil/sync/")]
        [HttpPost]
        public SyncResponse GetSyncResponse(SyncRequest syncRequest)
        {
            return new SyncResponse()
            {
                UpdatedAttractions = new List<AttractionMobilDto>(),
                NewAttractions = new List<AttractionMobilDto>(),
                DeletedAttractions = new List<int>(),
                UpdatedExhibitions = new List<ExhibitionMobilDto>(),
                NewExhibitions = new List<ExhibitionMobilDto>(),
                DeletedExhibitions = new List<int>(),
                AttractionCategories = new List<CategoryMobilDto>(),
                ExhibitionCategories = new List<CategoryMobilDto>(),
                ServiceCategories = new List<CategoryMobilDto>(),
                Counties = new List<CountyMobilDto>()
            };
        }
    }
}