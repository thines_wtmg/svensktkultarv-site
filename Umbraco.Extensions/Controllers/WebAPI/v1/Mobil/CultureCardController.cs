﻿using System;
using System.Net;
using System.Net.Http;
using System.Reflection;
using System.Web.Http;
using AttributeRouting;
using AttributeRouting.Web.Http;
using Google;
using log4net;
using umbraco.BusinessLogic;
using Umbraco.Extensions.BLL;
using Umbraco.Extensions.Controllers.WebAPI.v1.Mobil.Dto;
using Umbraco.Extensions.Models.Rdbms.POCOS;
using Umbraco.Extensions.Utilities.Security;
using Umbraco.Web.WebApi;

namespace Umbraco.Extensions.Controllers.WebAPI.v1.Mobil
{
    [RoutePrefix("api")]
    [ReturnsJson]
    [MobilTokenAuthorization]
    public class CultureCardController : UmbracoApiController
    {
        private static readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="number"></param>
        /// <param name="zip"></param>
        /// <returns></returns>
        [GET("v1/mobil/culturecard/{number}/{zip}")]
        public HttpResponseMessage GetByNumberAndZip(string number, string zip)
        {
            CultureCard cultureCard = null;
            Log.InfoFormat("Initiating fetch of culture card number:{0}, zip:{1} ", number, zip);

            try
            {
                cultureCard = CultureCardLogic.GetByNumber(number);
            }
            catch (Exception ex)
            {
                Log.ErrorFormat("Could not retriev culture card error:{0} ", ex.Message);
                throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.InternalServerError, "Retrieval failed"));
            }

            if (cultureCard != null)
            {
                if (CultureCardLogic.CultureCardHasZip(cultureCard, zip))
                {
                    Log.InfoFormat("Successfully returning culture card with phonenumber:{0}, HttpStatus:{1} ", cultureCard.PhoneNumber, "200");
                    return Request.CreateResponse(HttpStatusCode.OK, cultureCard);
                }

                Log.InfoFormat("Could retriev culture card with correct zip. Phonenumber:{0}, zip:{1}, old zip:{2}, HttpStatus:{3}", cultureCard.PhoneNumber, zip, cultureCard.ZipCode, "400");
                throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.BadRequest, "UnAuthorized"));

            }

            Log.InfoFormat("Could retriev culture card by phonenumber:{0}, zip:{1}, HttpStatus:{2}", number, zip, "400");
            throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.BadRequest, "Notfound"));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="card"></param>
        /// <returns></returns>
        [POST("v1/mobil/culturecard/add/")]
        public HttpResponseMessage AddCultureCard(CultureCardDto card)
        {
            DateTime parsedDate;
            var cultureCard = new CultureCard();
            CultureCard existingCard = null;

            cultureCard.PhoneNumber = card.PhoneNumber;
            cultureCard.DeviceId = card.DeviceId;
            cultureCard.FirstName = card.FirstName;
            cultureCard.LastName = card.LastName;
            cultureCard.ZipCode = card.ZipCode;

            try
            {
                existingCard = CultureCardLogic.GetByNumber(card.PhoneNumber);
            }
            catch (Exception ex)
            {
                Log.ErrorFormat("Error reading CultureCard DB, error:{0} ", ex.Message);
            }

            if (existingCard != null)
            {
                Log.ErrorFormat("Could not insert new culture card, card exists, deviceId: {0}, phonenumber: {1}, zip {2}, ", existingCard.DeviceId, existingCard.PhoneNumber, existingCard.ZipCode);
                throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.Conflict, "PhoneNumber exists"));
            }

            if (DateTime.TryParse(card.Expires, out parsedDate))
            {
                cultureCard.ExpireDate = parsedDate;
            }
            else
            {
                Log.ErrorFormat("Could not insert new culture card, wrong Date format on expire date, expire: {0}, phonenumber: {1}", card.Expires, card.PhoneNumber);
                throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.BadRequest, "Wrong Date format"));
            }

            try
            {
                //Get the Umbraco db
                var db = ApplicationContext.DatabaseContext.Database;

                //Add the object to the DB
                db.Insert(cultureCard);
            }
            catch (Exception ex)
            {
                Log.ErrorFormat("Could not insert new culture card, error:{0} ", ex.Message);
                throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.InternalServerError, "CRUDE operation failed"));
            }

            Log.InfoFormat("Culture card with phonenumber:{0}, Successfully inserted HttpStatus:{1}", cultureCard.PhoneNumber, "201");
            return Request.CreateResponse(HttpStatusCode.Created, cultureCard);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="card"></param>
        /// <returns></returns>
        [POST("v1/mobil/culturecard/update/")]
        public HttpResponseMessage UpdateCultureCard(CultureCardDto card)
        {
            var cultureCard = CultureCardLogic.GetByNumber(card.PhoneNumber);

            if (cultureCard != null)
            {
                Log.InfoFormat("Initiating Update of culture card, new deviceId:{0}, new phonenumber:{1} new zip:{2}, new expire date:{3}", card.DeviceId, card.PhoneNumber, card.ZipCode, card.Expires);

                if (CultureCardLogic.CultureCardHasZip(cultureCard, card.ZipCode))
                {
                    DateTime parsedDate;

                    if (!DateTime.TryParse(card.Expires, out parsedDate))
                    {
                        Log.ErrorFormat("Could not update culture card, wrong Date format on expire date, expire: {0}, phonenumber: {1}", card.Expires, card.PhoneNumber);
                        throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.BadRequest, "Wrong Date format"));
                    }

                    //Update the expire date
                    cultureCard.ExpireDate = parsedDate;

                    //Update deviceId if it is not null or empty.
                    cultureCard.DeviceId = card.DeviceId;

                    try
                    {
                        //Get the Umbraco db
                        var db = ApplicationContext.DatabaseContext.Database;

                        //Add the object to the DB
                        db.Update(cultureCard);
                    }
                    catch (Exception ex)
                    {
                        Log.ErrorFormat("Could not update existing culture card:{0}, error:{1} ", card.PhoneNumber, ex.Message);
                        throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.InternalServerError, "CRUDE operation failed"));
                    }

                    Log.InfoFormat("Culture card with phonenumber:{0}, Successfully updated HttpStatus:{1}", cultureCard.PhoneNumber, "200");
                    return Request.CreateResponse(HttpStatusCode.OK, cultureCard);
                }

                Log.ErrorFormat("Could not update existing culture card with phonenumber:{0}, the zip codes does not match. Old zip:{1}, new zip:{2}, HttpResponse:{3}", cultureCard.PhoneNumber, cultureCard.ZipCode, card.ZipCode, "400");
                throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.BadRequest, "UnAuthorized"));
            }

            Log.ErrorFormat("Could not update culture card with phonenumber:{0}, it was not found, HttpResponse:{1}", card.PhoneNumber, "400");
            throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.BadRequest, "Notfound"));
        }
    }
}
