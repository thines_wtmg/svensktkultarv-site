﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using AttributeRouting;
using Umbraco.Extensions.Controllers.WebAPI.v1.Dto;
using Umbraco.Extensions.Models.Generated;
using Umbraco.Extensions.Utilities;
using Umbraco.Extensions.Utilities.Cache;
using Umbraco.Extensions.Utilities.Services;
using Umbraco.Web;
using Umbraco.Web.WebApi;
using Examine;

namespace Umbraco.Extensions.Controllers.WebAPI.V1
{
    [RoutePrefix("v1")]
    public class AttractionsController : UmbracoApiController
    {
        private readonly MemoryCacher _memCacheManager;
        private readonly ExamineSearchService _examineSearchService;
        private readonly CultureInfo _svCulture = new CultureInfo("sv-SE");

        public AttractionsController() : this(new MemoryCacher(), new ExamineSearchService())
        {

        }

        private AttractionsController(MemoryCacher memCacheManager, ExamineSearchService examineSearchService)
        {

            _memCacheManager = memCacheManager;
            _examineSearchService = examineSearchService;
        }

        #region For home page

        /// <summary>
        /// Get all published attractions by Geo Container for Home page.
        /// </summary>
        /// <param name="pageSize"></param>
        /// <param name="page"></param>
        /// <param name="initialPageSize"></param>
        /// <returns></returns>
        [CacheControl(MaxAge = 100)]
        public AttractionsForHomeResult GetAllForHome(int initialPageSize = 7, int pageSize = 8, int page = 0)
        {
            var result = _memCacheManager.GetValue(_memCacheManager.GetSimpleCacheKeyFromPageSize(v1.Settings.CacheKeys.AllAttractions, initialPageSize));

            //Check if cached and Never cache paged results
            if (result == null || page != 0)
            {
                //Not from cache.
                var allAttractionsHomeResult = GetAllAttractionsHomeResult(Umbraco, initialPageSize, pageSize, page);

                //Add to cache. but never cache paged results, only page 0 but with different pagesizes
                if (allAttractionsHomeResult != null && page == 0)
                {
                    _memCacheManager.Add(_memCacheManager.GetSimpleCacheKeyFromPageSize(v1.Settings.CacheKeys.AllAttractions, initialPageSize), allAttractionsHomeResult, DateTimeOffset.UtcNow.AddHours(v1.Settings.CacheTime.TwelveHours));
                }

                return allAttractionsHomeResult;
            }
            else
            {
                //Cached response
                return (AttractionsForHomeResult)result;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="nodeIds"></param>
        /// <param name="attractionCategories"></param>
        /// <param name="serviceCategories"></param>
        /// <param name="initialPageSize"></param>
        /// <param name="pageSize"></param>
        /// <param name="page"></param>
        /// <returns></returns>
        [CacheControl(MaxAge = 100)]
        public AttractionsForHomeResult GetFiltered(string nodeIds, string attractionCategories, string serviceCategories, int initialPageSize = 7, int pageSize = 8, int page = 0)
        {
            var parentIds = Helpers.ConvertCommaSeparatedIntStringToStringArray(nodeIds);
            var attractionCats = Helpers.ConvertCommaSeparatedIntStringToStringArray(attractionCategories);
            var serviceCats = Helpers.ConvertCommaSeparatedIntStringToStringArray(serviceCategories);

            var attractions = Umbraco.TypedSearch(_examineSearchService.GetAttractionSearchCriteria(parentIds, attractionCats, serviceCats), ExamineManager.Instance.SearchProviderCollection["AttractionSearcher"]).Cast<Attraction>().OrderBy(a => a.Name, StringComparer.Create(_svCulture, false)).Select(a => new AttractionForHomeDto
            {
                Id = a.Id,
                Name = a.Name,
                City = a.City,
                Link = a.Url,
                Lat = a.Location != null ? a.Location.Lat : -1,
                Lng = a.Location != null ? a.Location.Lng : -1,
                MainImage = a.MainImage != null ? a.MainImage.Cropper.GetCropUrl(270, 240, null, 75) : string.Empty
            });

            //Avoiding multiple enumeration when counting total number found and number returned, (necessary?)
            var attractionForHomeDtos = attractions as IList<AttractionForHomeDto> ?? attractions.ToList();
            var totalNumberOfAttractions = attractionForHomeDtos.Count();
            var slicedAttractions = attractionForHomeDtos.Skip(page * initialPageSize).Take(initialPageSize);
            var singlePageOfAttractions = slicedAttractions as IList<AttractionForHomeDto> ?? slicedAttractions.ToList();

            return (new AttractionsForHomeResult
            {
                Total = totalNumberOfAttractions,
                Count = singlePageOfAttractions.Count(),
                CurrentPage = page,
                Attractions = singlePageOfAttractions,
                PageSize = initialPageSize
            });
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="helper"></param>
        /// <param name="page"></param>
        /// <param name="initialPageSize"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        private AttractionsForHomeResult GetAllAttractionsHomeResult(UmbracoHelper helper, int initialPageSize = 7, int pageSize = 8, int page = 0)
        {
            var attractions = Umbraco.TypedSearch(_examineSearchService.GetAllAttractionsSearchCriteria(), ExamineManager.Instance.SearchProviderCollection["AttractionSearcher"]).Cast<Attraction>().OrderBy(a => a.Name, StringComparer.Create(_svCulture, false)).Select(a => new AttractionForHomeDto
            {
                Id = a.Id,
                Name = a.Name,
                City = a.City,
                Link = a.Url,
                Lat = a.Location != null ? a.Location.Lat : -1,
                Lng = a.Location != null ? a.Location.Lng : -1,
                MainImage = a.MainImage != null ? a.MainImage.Cropper.GetCropUrl(270, 240, null, 75) : string.Empty
            });

            //Avoiding multiple enumeration when counting total number found and number returned
            var attractionForHomeDtos = attractions as IList<AttractionForHomeDto> ?? attractions.ToList();
            var totalNumberOfAttractions = attractionForHomeDtos.Count();
            var slicedAttractions = attractionForHomeDtos.Skip(page * initialPageSize).Take(initialPageSize);
            var singlePageOfAttractions = slicedAttractions as IList<AttractionForHomeDto> ?? slicedAttractions.ToList();

            return (new AttractionsForHomeResult
            {
                Total = totalNumberOfAttractions,
                Count = singlePageOfAttractions.Count(),
                CurrentPage = page,
                Attractions = singlePageOfAttractions,
                PageSize = initialPageSize
            });
        }

        #endregion


        #region General Api

        #endregion
    }

}