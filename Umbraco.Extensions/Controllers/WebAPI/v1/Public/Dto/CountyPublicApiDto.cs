﻿namespace Umbraco.Extensions.Controllers.WebAPI.v1.Public.Dto
{
    public class CountyPublicApiDto
    {
        public int ID { get; set; }
        public string Name { get; set; }
    }
}