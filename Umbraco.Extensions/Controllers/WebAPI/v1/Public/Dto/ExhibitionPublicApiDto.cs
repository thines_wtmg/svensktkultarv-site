﻿using System;
using System.Collections.Generic;
using Umbraco.Extensions.Controllers.WebAPI.v1.Mobil.Dto;

namespace Umbraco.Extensions.Controllers.WebAPI.v1.Public.Dto
{
    public class ExhibitionPublicApiDto 
    {
        public int ID { get; set; }
        public int FkRelatedAttraction { get; set; }
        public string Name { get; set; }
        public string Link { get; set; }
        public string Preamble { get; set; }
        public string MainBody { get; set; }
        public String MainImage { get; set; }
        public IEnumerable<int> Categories { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime StopDate { get; set; }
    }
}