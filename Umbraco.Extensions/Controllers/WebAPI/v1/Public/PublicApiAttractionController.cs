﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using AttributeRouting;
using AttributeRouting.Web.Http;
using Umbraco.Extensions.Controllers.WebAPI.v1.Public.Dto;
using Umbraco.Extensions.Models.Custom;
using Umbraco.Extensions.Models.Generated;
using Umbraco.Extensions.Utilities;
using Umbraco.Extensions.Utilities.Cache;
using Umbraco.Extensions.Utilities.Security;
using Umbraco.Web;
using Umbraco.Web.WebApi;

namespace Umbraco.Extensions.Controllers.WebAPI.v1.Public
{
    [RoutePrefix("api")]
    [ReturnsJson]
    public class PublicApiAttractionController : UmbracoApiController
    {
        private readonly MemoryCacher _memCacheManager;
        private readonly JsonDiskCache _jsonDiskCache;

        public PublicApiAttractionController(): this(new JsonDiskCache() ,new MemoryCacher())
        {
                
        }
        private PublicApiAttractionController(JsonDiskCache jsonDiskCache, MemoryCacher memCacheManager)
        {
            _jsonDiskCache = jsonDiskCache;
            _memCacheManager = memCacheManager;
        }

        /// <summary>
        /// Get all published attractions
        /// </summary>
        /// <returns></returns>
        [GET("v1/public/attractions/")]
        [Throttle(Name = "AttractionsThrottle", Duration = 60, Hits = 2)]
        public List<AttractionPublicApiDto> GetAll(string culture = "sv")
        {
            var attractionJsonCacheKey = v1.Settings.CacheKeys.AllPublicApiAttractions;

            var cachedJson = _jsonDiskCache.Get<List<AttractionPublicApiDto>>(attractionJsonCacheKey);

            if (cachedJson != null)
            {
                return cachedJson;
            } 

            //Not from cache.
            var attractions = Umbraco.TypedContentAtRoot().DescendantsOrSelf<Attraction>().Where(a=> !a.OnlyShowInApp).Select(a => new AttractionPublicApiDto
            {
                ID = a.Id,
                CountyId = a.GetCountyId(),
                Name = a.Name,
                Link = a.Url,
                Preamble = a.Preamble != null ? a.Preamble as string : null,
                MainBody = a.MainBody != null ? a.MainBody.ToHtmlString() : string.Empty,
                MainImage = a.MainImage != null ? a.MainImage.Cropper.GetCropUrl(640, 480, null, 75) : string.Empty,
                Lat = a.Location != null ? a.Location.Lat : -1,
                Lng = a.Location != null ? a.Location.Lng : -1,
                Contact = new ContactInformation
                {
                    Address = a.AdressLine1,
                    ZipCode = a.AdressLine2,
                    City = a.City,
                    County = a.GetCountyName(),
                    Phone = a.Phone,
                    Email = a.Email,
                    Www = a.Www
                },
                OpeningHours = new OpeningHours
                {
                    Monday = new DayOpeningHours { From = Helpers.GetTimeSpan(a.OpeningHoursMondayFrom), To = Helpers.GetTimeSpan(a.OpeningHoursMondayTo) },
                    Tuesday = new DayOpeningHours { From = Helpers.GetTimeSpan(a.OpeningHoursTuesdayFrom), To = Helpers.GetTimeSpan(a.OpeningHoursTuesdayTo) },
                    Wednesday = new DayOpeningHours { From = Helpers.GetTimeSpan(a.OpeningHoursWednesdayFrom), To = Helpers.GetTimeSpan(a.OpeningHoursWednesdayTo) },
                    Thursday = new DayOpeningHours { From = Helpers.GetTimeSpan(a.OpeningHoursThursdayFrom), To = Helpers.GetTimeSpan(a.OpeningHoursThursdayTo) },
                    Friday = new DayOpeningHours { From = Helpers.GetTimeSpan(a.OpeningHoursFridayFrom), To = Helpers.GetTimeSpan(a.OpeningHoursFridayTo) },
                    Saturday = new DayOpeningHours { From = Helpers.GetTimeSpan(a.OpeningHoursSaturdayFrom), To = Helpers.GetTimeSpan(a.OpeningHoursSaturdayTo) },
                    Sunday = new DayOpeningHours { From = Helpers.GetTimeSpan(a.OpeningHoursSundayFrom), To = Helpers.GetTimeSpan(a.OpeningHoursSundayTo) },
                    OpeningHoursExceptionsHeading = a.HeadingOpeningHoursExceptions,
                    OpeningHoursGenericExceptions = a.OpeningHours != null ? a.OpeningHours.ToHtmlString() : string.Empty,
                },
                Price = a.Price.ToHtmlString(),
                CardDiscount = a.CardDiscount != null ? a.CardDiscount.ToHtmlString() : string.Empty,
                Categories = a.AttractionCategories.Select(c => c.Id),
                ServiceCategories = a.ServiceCategories.Select(c => c.Id)
            }).ToList();

            _jsonDiskCache.Add(attractionJsonCacheKey, attractions);

            return attractions;
        }

        /// <summary>
        /// Get attraction by it's ID
        /// </summary>
        /// <param name="id"></param>
        /// <param name="culture"></param>
        /// <returns></returns>
        [GET("v1/public/attraction/{id}")]
        [Throttle(Name = "AttractionByIdThrottle", Duration = 10, Hits = 5)]
        public HttpResponseMessage GetById(int id, string culture = "sv")
        {
            var attraction = Umbraco.TypedContent(id) as Attraction;
            if (attraction != null && !attraction.OnlyShowInApp)
            {
                var attractionPublicApiDto = new AttractionPublicApiDto
                {
                    ID = attraction.Id,
                    CountyId = attraction.GetCountyId(),
                    Name = attraction.Name,
                    Link = attraction.Url,
                    Preamble = attraction.Preamble as string,
                    MainBody = attraction.MainBody != null ? attraction.MainBody.ToHtmlString() : string.Empty,
                    MainImage = attraction.MainImage != null ? attraction.MainImage.Cropper.GetCropUrl(640, 480, null, 75) : string.Empty,
                    Lat = attraction.Location != null ? attraction.Location.Lat : -1,
                    Lng = attraction.Location != null ? attraction.Location.Lng : -1,
                    Contact = new ContactInformation
                    {
                        Address = attraction.AdressLine1,
                        ZipCode = attraction.AdressLine2,
                        City = attraction.City,
                        County = attraction.GetCountyName(),
                        Phone = attraction.Phone,
                        Email = attraction.Email,
                        Www = attraction.Www
                    },
                    OpeningHours = new OpeningHours
                    {
                        Monday = new DayOpeningHours { From = Helpers.GetTimeSpan(attraction.OpeningHoursMondayFrom), To = Helpers.GetTimeSpan(attraction.OpeningHoursMondayTo) },
                        Tuesday = new DayOpeningHours { From = Helpers.GetTimeSpan(attraction.OpeningHoursTuesdayFrom), To = Helpers.GetTimeSpan(attraction.OpeningHoursTuesdayTo) },
                        Wednesday = new DayOpeningHours { From = Helpers.GetTimeSpan(attraction.OpeningHoursWednesdayFrom), To = Helpers.GetTimeSpan(attraction.OpeningHoursWednesdayTo) },
                        Thursday = new DayOpeningHours { From = Helpers.GetTimeSpan(attraction.OpeningHoursThursdayFrom), To = Helpers.GetTimeSpan(attraction.OpeningHoursThursdayTo) },
                        Friday = new DayOpeningHours { From = Helpers.GetTimeSpan(attraction.OpeningHoursFridayFrom), To = Helpers.GetTimeSpan(attraction.OpeningHoursFridayTo) },
                        Saturday = new DayOpeningHours { From = Helpers.GetTimeSpan(attraction.OpeningHoursSaturdayFrom), To = Helpers.GetTimeSpan(attraction.OpeningHoursSaturdayTo) },
                        Sunday = new DayOpeningHours { From = Helpers.GetTimeSpan(attraction.OpeningHoursSundayFrom), To = Helpers.GetTimeSpan(attraction.OpeningHoursSundayTo) },
                        OpeningHoursExceptionsHeading = attraction.HeadingOpeningHoursExceptions,
                        OpeningHoursGenericExceptions = attraction.OpeningHours != null ? attraction.OpeningHours.ToHtmlString() : string.Empty,
                    },
                    Price = attraction.Price.ToHtmlString(),
                    CardDiscount = attraction.CardDiscount.ToHtmlString(),
                    Categories = attraction.AttractionCategories.Select(c => c.Id),
                    ServiceCategories = attraction.ServiceCategories.Select(c => c.Id)
                };

                return Request.CreateResponse(HttpStatusCode.OK, attractionPublicApiDto);
            }
            return Request.CreateErrorResponse(HttpStatusCode.NotFound, string.Format("Could not find attraction with id:{0}", id));
        }
    }
}