﻿using System;
using System.Collections.Generic;
using System.Linq;
using AttributeRouting;
using AttributeRouting.Web.Http;
using Umbraco.Extensions.Controllers.WebAPI.v1.Mobil.Dto;
using Umbraco.Extensions.Controllers.WebAPI.v1.Public.Dto;
using Umbraco.Extensions.Models.Generated;
using Umbraco.Extensions.Utilities.Cache;
using Umbraco.Extensions.Utilities.Security;
using Umbraco.Web;
using Umbraco.Web.WebApi;

namespace Umbraco.Extensions.Controllers.WebAPI.v1.Public
{
    [RoutePrefix("api")]
    [ReturnsJson]
    public class PublicApiCountyController : UmbracoApiController
    {
        private readonly MemoryCacher _memCacheManager;

        public PublicApiCountyController()
            : this(new MemoryCacher())
        {

        }

        private PublicApiCountyController(MemoryCacher memoryCacher)
        {
            _memCacheManager = memoryCacher;
        }

        /// <summary>
        /// Get all GeoContainers ie, counties
        /// </summary>
        /// <returns></returns>
        [GET("v1/public/counties/")]
        [Throttle(Name = "CountiesThrottle", Duration = 10, Hits = 5)]
        public List<CountyPublicApiDto> GetAll(string culture = "sv")
        {
            var result = _memCacheManager.GetValue(v1.Settings.CacheKeys.Counties);

            if (result != null)
                return (List<CountyPublicApiDto>)result;

            var counties = Umbraco.TypedContentAtRoot().DescendantsOrSelf<GeoContainer>().Select(a => new CountyPublicApiDto
                {
                    ID = a.Id,
                    Name = a.Name
                }).ToList();

            //Add to cache.
            if (counties != null)
            {
                _memCacheManager.Add(v1.Settings.CacheKeys.Counties, counties, DateTimeOffset.UtcNow.AddHours(v1.Settings.CacheTime.TwelveHours));
            }

            return counties;
        }
    }
}