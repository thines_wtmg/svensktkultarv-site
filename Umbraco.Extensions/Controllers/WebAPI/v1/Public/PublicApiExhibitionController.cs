﻿using System;
using System.Collections.Generic;
using System.Linq;
using AttributeRouting;
using AttributeRouting.Web.Http;
using Umbraco.Extensions.Controllers.WebAPI.v1.Mobil.Dto;
using Umbraco.Extensions.Controllers.WebAPI.v1.Public.Dto;
using Umbraco.Extensions.Models.Generated;
using Umbraco.Extensions.Utilities;
using Umbraco.Extensions.Utilities.Cache;
using Umbraco.Extensions.Utilities.Security;
using Umbraco.Web;
using Umbraco.Web.WebApi;

namespace Umbraco.Extensions.Controllers.WebAPI.v1.Public
{
    [RoutePrefix("api")]
    [ReturnsJson]
    public class PublicApiExhibitionController : UmbracoApiController
    {

        private readonly MemoryCacher _memCacheManager;
        private readonly JsonDiskCache _jsonDiskCache;

        public PublicApiExhibitionController() : this(new MemoryCacher(), new JsonDiskCache())
        {

        }

        private PublicApiExhibitionController(MemoryCacher memCacheManager, JsonDiskCache jsonDiskCache)
        {

            _memCacheManager = memCacheManager;
            _jsonDiskCache = jsonDiskCache;
        }

        /// <summary>
        /// Get all published exhibitions
        /// </summary>
        /// <returns></returns>
        [GET("v1/public/exhibitions/")]
        [Throttle(Name = "ExhibitionsThrottle", Duration = 60, Hits = 2)]
        public List<ExhibitionPublicApiDto> GetAll(string culture = "sv")
        {
            var exhibitionJsonCacheKey = v1.Settings.CacheKeys.AllPublicApiExhibitions;

            var cachedJson = _jsonDiskCache.Get<List<ExhibitionPublicApiDto>>(exhibitionJsonCacheKey);

            if (cachedJson != null)
            {
                return cachedJson;
            }

            var exhibitions =
                Umbraco.TypedContentAtRoot().DescendantsOrSelf<Exhibition>().Where(e => e.StopDate >= DateTime.Now).Select(x => new ExhibitionPublicApiDto()
                {
                    ID = x.Id,
                    FkRelatedAttraction = x.Parent.Id,
                    Name = x.Name,
                    Link = x.Url,
                    Preamble = x.Preamble as string,
                    MainBody = x.MainBody.ToHtmlString(),
                    MainImage = x.MainImage != null ? x.MainImage.Cropper.GetCropUrl(640, 480, null, 75) : string.Empty,
                    StartDate = x.StartDate,
                    StopDate = x.StopDate,
                    Categories = x.ExhibitionCategories.Select(c => c.Id)

                }).ToList();

            _jsonDiskCache.Add(exhibitionJsonCacheKey, exhibitions);

            return exhibitions;

        }

        /// <summary>
        /// Get exhhibition by id.
        /// </summary>
        /// <returns></returns>
        [GET("v1/public/exhibition/{id}")]
        [Throttle(Name = "ExhibitionByIdThrottle", Duration = 10, Hits = 5)]
        public ExhibitionPublicApiDto GetById(int id, string culture = "sv")
        {
            var exhibitionPage = Umbraco.TypedContent(id) as Exhibition;
            if (exhibitionPage != null)
            {
                var exhibition = new ExhibitionPublicApiDto()
                {
                    ID = exhibitionPage.Id,
                    FkRelatedAttraction = exhibitionPage.Parent.Id,
                    Name = exhibitionPage.Name,
                    Link = exhibitionPage.Url,
                    Preamble = exhibitionPage.Preamble as string,
                    MainBody = exhibitionPage.MainBody.ToHtmlString(),
                    MainImage = exhibitionPage.MainImage != null ? exhibitionPage.MainImage.Cropper.GetCropUrl(640, 480, null, 75) : string.Empty,
                    StartDate = exhibitionPage.StartDate,
                    StopDate = exhibitionPage.StopDate,
                    Categories = exhibitionPage.ExhibitionCategories.Select(c => c.Id)
                };

                return exhibition;
            }
            return null;
        }

        /// <summary>
        /// Get all exhibitions for a specific attraction 
        /// </summary>
        /// <param name="id"></param>
        /// <param name="culture"></param>
        /// <returns></returns>
        [GET("v1/public/attraction/exhibitions/{id}")]
        [Throttle(Name = "ExhibitionsForAttractionThrottle", Duration = 10, Hits = 5)]
        public IEnumerable<ExhibitionPublicApiDto> GetByAttraction(int id, string culture = "sv")
        {
            var parentAttraction = Umbraco.TypedContent(id) as Attraction;
            if (parentAttraction != null)
            {
                var exhibitions = parentAttraction.Children<Exhibition>().Select(e => new ExhibitionPublicApiDto
                {
                    ID = e.Id,
                    FkRelatedAttraction = e.Parent.Id,
                    Name = e.Name,
                    Link = e.Url,
                    Preamble = e.Preamble as string,
                    MainBody = e.MainBody.ToHtmlString(),
                    MainImage = e.MainImage != null ? e.MainImage.Cropper.GetCropUrl(640, 480, null, 75) : string.Empty,
                    StartDate = e.StartDate,
                    StopDate = e.StopDate,
                    Categories = e.ExhibitionCategories.Select(c => c.Id)
                });

                return exhibitions;
            }
            return new List<ExhibitionPublicApiDto>();
        }
    }
}