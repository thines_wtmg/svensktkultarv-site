﻿using System.Collections.Generic;
using System.Linq;
using AttributeRouting;
using AttributeRouting.Web.Http;
using Umbraco.Extensions.BLL;
using Umbraco.Extensions.Controllers.WebAPI.v1.Public.Dto;
using Umbraco.Extensions.Utilities.Security;
using Umbraco.Web.WebApi;

namespace Umbraco.Extensions.Controllers.WebAPI.v1.Public
{
    [RoutePrefix("api")]
    [ReturnsJson]
    public class PublicApiCategoryController : UmbracoApiController
    {
        /// <summary>
        /// Get all service categories
        /// </summary>
        /// <returns></returns>
        [GET("v1/public/servicecategories/")]
        [Throttle(Name = "ServiceCategoriesThrottle", Duration = 10, Hits = 5)]
        public List<CategoryPublicApiDto> GetAllServiceCategories(string culture = "sv")
        {
            var categories = CategoryLogic.GetServiceCategoriesForWebApi().Select(c => new CategoryPublicApiDto()
            {
                ID = c.Id,
                Name = c.Name
            }).ToList();

            return categories;
        }

        /// <summary>
        /// Get all attraction categories
        /// </summary>
        /// <returns></returns>
        [GET("v1/public/attractioncategories/")]
        [Throttle(Name = "AttractionCategoriesThrottle", Duration = 10, Hits = 5)]
        public List<CategoryPublicApiDto> GetAllAttractionCategories(string culture = "sv")
        {
            var categories = CategoryLogic.GetAttractionCategoriesForWebApi().Select(c => new CategoryPublicApiDto()
            {
                ID = c.Id,
                Name = c.Name
            }).ToList();

            return categories;
        }

        /// <summary>
        /// Get all exhibition categories
        /// </summary>
        /// <returns></returns>
        [GET("v1/public/exhibitioncategories/")]
        [Throttle(Name = "ExhibitionCategoriesThrottle", Duration = 10, Hits = 5)]
        public List<CategoryPublicApiDto> GetAllExhibitionCategories(string culture = "sv")
        {
            var categories = CategoryLogic.GetExhibitionCategoriesForWebApi().Select(c => new CategoryPublicApiDto()
            {
                ID = c.Id,
                Name = c.Name
            }).ToList();

            return categories;
        }
    }
}