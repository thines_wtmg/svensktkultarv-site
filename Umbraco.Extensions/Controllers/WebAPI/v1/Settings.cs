﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace Umbraco.Extensions.Controllers.WebAPI.v1
{
    /// <summary>
    /// Used for Global settings for the WebAPi.
    /// </summary>
    public class Settings
    {
        public static int MaxPageSize { get { return 10000; } }
        public static int InitialPageSizeForHomePage { get { return 7; } }
        public static int InitialPageSizeForAttractionSearch { get { return 15; } }
        public static int InitialPageSizeForExhibitionSearch { get { return 15; } }

        public static class MobilApiKeyStore 
        {
            public static string WebConfigMobilApiKey { get { return ConfigurationManager.AppSettings["MobilApiSecretKey"]; } }
        }
        public static class CultureCardAdminUsers
        {
            public static string AllowedUsers { get { return ConfigurationManager.AppSettings["AllowedToListCultureCards"]; } }
        }

        public static class CacheKeys
        {
            public static string SingleAttraction { get { return "SingleAttraction"; } }
            public static string SingleExhibition { get { return "SingleExhibition"; } }
            public static string AllAttractions { get { return "AllAttractions"; } }
            public static string AllExhibitions { get { return "AllExhibitions"; } }
            public static string Counties { get { return "AllExhibitions"; } }
            public static string AllAttractionsExhibitionsForAutoComplete { get { return "AllAttractionsExhibitionsForAutoComplete"; } }
            public static string AllAttractionsForAutoComplete { get { return "AllAttractionsForAutoComplete"; } }
            public static string AllExhibitionsForAutoComplete { get { return "AllExhibitionsForAutoComplete"; } }
            public static string AllMobilAttractions { get { return "attractions" + DateTime.Now.ToString("yyyyMMdd"); } }
            public static string AllMobilExhibitions { get { return "exhibitions" + DateTime.Now.ToString("yyyyMMdd"); } }
            public static string AllPublicApiAttractions { get { return "public-api-attractions" + DateTime.Now.ToString("yyyyMMdd"); } }
            public static string AllPublicApiExhibitions { get { return "public-api-exhibitions" + DateTime.Now.ToString("yyyyMMdd"); } }
        }

        public static class CacheTime
        {
            public static int TenMinutes { get { return 10; } }
            public static int OneHour { get { return 1; } }
            public static int TwelveHours { get { return 12; } }
            public static int AllAttractionsDuration { get { return 12; } }
            public static int AllExhibitionsDuration { get { return 12; } }
            public static int AllMobilAttractionsDuration { get { return 12; } }
            public static int AllMobilExhibitionsDuration { get { return 12; } }
        }
    }
}