﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using AttributeRouting;
using Umbraco.Extensions.Controllers.WebAPI.v1;
using Umbraco.Extensions.Controllers.WebAPI.v1.Dto;
using Umbraco.Extensions.Models.Generated;
using Umbraco.Extensions.Utilities;
using Umbraco.Extensions.Utilities.Cache;
using Umbraco.Extensions.Utilities.Services;
using Umbraco.Web;
using Umbraco.Web.WebApi;

namespace Umbraco.Extensions.Controllers.WebAPI.V1
{
    [RoutePrefix("v1")]
    [ReturnsJson]
    public class SearchController : UmbracoApiController
    {
        private readonly MemoryCacher _memCacheManager;
        private readonly ExamineSearchService _examineSearchService;
        private readonly JsonDiskCache _jsonDiskCache;
        private readonly CultureInfo _svCulture = new CultureInfo("sv-SE");

        public SearchController() : this(new MemoryCacher(), new ExamineSearchService(), new JsonDiskCache())
        {

        }

        private SearchController(MemoryCacher memCacheManager, ExamineSearchService examineSearchService, JsonDiskCache jsonDiskCache)
        {
            _memCacheManager = memCacheManager;
            _examineSearchService = examineSearchService;
            _jsonDiskCache = jsonDiskCache;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [CacheControl(MaxAge = 100)]
        public List<SearchDto> GetAll()
        {
            var result = _memCacheManager.GetValue(v1.Settings.CacheKeys.AllAttractionsExhibitionsForAutoComplete);

            if (result == null)
            {
                //Not from cache
                var attractions = Umbraco.TypedContentAtRoot()
                    .DescendantsOrSelf<Attraction>()
                    .Where(a => !a.OnlyShowInApp)
                    .Select(s => new SearchDto
                        {
                            Id = s.Id.ToString(CultureInfo.InvariantCulture),
                            label = string.Format("{0} {1} {2}", s.Name, s.Parent.Name, s.City),
                            city = s.City,
                            value = string.Format("{0}", s.Name),
                            link = s.Url,
                            type = "attraction"
                        });

                var exhibitions = Umbraco.TypedContentAtRoot()
                    .DescendantsOrSelf<Exhibition>().Where(e => e.StopDate >= DateTime.Now)
                    .Select(s => new SearchDto
                        {
                            Id = s.Id.ToString(CultureInfo.InvariantCulture),
                            label = string.Format("{0} {1} {2}", s.Name, s.GetGeoContainerName(), s.GetCity()),
                            city = s.GetCity(),
                            value = string.Format("{0}", s.Name),
                            link = s.Url,
                            type = "exhibition"
                        });

                var combinedSearch = attractions.Concat(exhibitions).OrderBy(s => s.value, StringComparer.Create(_svCulture, false)).ToList();

                //Add to cache.
                if (combinedSearch != null)
                {
                    _memCacheManager.Add(v1.Settings.CacheKeys.AllAttractionsExhibitionsForAutoComplete, combinedSearch, DateTimeOffset.UtcNow.AddHours(v1.Settings.CacheTime.TwelveHours));
                }

                return combinedSearch;
            }
            else
            {
                return (List<SearchDto>)result;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [CacheControl(MaxAge = 100)]
        public List<SearchDto> GetAllAttractions()
        {
            var result = _memCacheManager.GetValue(v1.Settings.CacheKeys.AllAttractionsForAutoComplete);

            if (result == null)
            {
                //Not from cache
                var attractions =
                    Umbraco.TypedContentAtRoot()
                        .DescendantsOrSelf<Attraction>()
                        .Where(a => !a.OnlyShowInApp)
                        .Select(s => new SearchDto
                        {
                            Id = s.Id.ToString(CultureInfo.InvariantCulture),
                            label = string.Format("{0} {1} {2}", s.Name, s.Parent.Name, s.City),
                            city = s.City,
                            value = string.Format("{0}", s.Name),
                            link = s.Url
                        }).OrderBy(s => s.value, StringComparer.Create(_svCulture, false)).ToList();
                
                //Add to cache.
                if (attractions != null)
                {
                    _memCacheManager.Add(v1.Settings.CacheKeys.AllAttractionsForAutoComplete, attractions, DateTimeOffset.UtcNow.AddHours(v1.Settings.CacheTime.TwelveHours));
                }

                return attractions;
            }
            else
            {
                return (List<SearchDto>)result;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [CacheControl(MaxAge = 100)]
        public List<SearchDto> GetAllExhibitions()
        {
            var result = _memCacheManager.GetValue(v1.Settings.CacheKeys.AllExhibitionsForAutoComplete);

            if (result == null)
            {
                //Not from cache
                var exhibitions = Umbraco.TypedContentAtRoot().DescendantsOrSelf<Exhibition>().Where(e => e.StopDate >= DateTime.Now).Select(s => new SearchDto
                {
                    Id = s.Id.ToString(CultureInfo.InvariantCulture),
                    label = string.Format("{0} {1} {2}", s.Name, s.GetGeoContainerName(), s.GetCity()),
                    city = s.GetCity(),
                    value = string.Format("{0}", s.Name),
                    link = s.Url
                }).OrderBy(s => s.value, StringComparer.Create(_svCulture, false)).ToList();

                //Add to cache.
                if (exhibitions != null)
                {
                    _memCacheManager.Add(v1.Settings.CacheKeys.AllExhibitionsForAutoComplete, exhibitions, DateTimeOffset.UtcNow.AddHours(v1.Settings.CacheTime.TwelveHours));
                }

                return exhibitions;
            }
            else
            {
                return (List<SearchDto>)result;
            }
        }
    }
}