﻿using System.Text;
using DevTrends.MvcDonutCaching;
using System.Linq;
using System.Web.Mvc;
using umbraco;
using Umbraco.Extensions.BLL;
using Umbraco.Extensions.Controllers.Base;
using Umbraco.Extensions.Models.Form;
using Umbraco.Extensions.Models.Generated;

namespace Umbraco.Extensions.Controllers
{
    public class ContactController : SurfaceRenderMvcController
    {
        [DonutOutputCache(CacheProfile = "OneDay")]
        public ActionResult Contact()
        {
            var masterModel = ModelLogic.CreateMasterModel();
            return CurrentTemplate(masterModel);
        }

        [HttpPost]
        public ActionResult SendContact(ContactFormModel model)
        {
            var contact = CurrentPage as CultureCard;
            var email = contact.Email;
            var subject = contact.Subject;
            var confirmMail = contact.ConfirmationMail;
            var subjectConfirm = contact.SubjectConfirmation;

            //Set the fields that need to be replaced.
            if (ModelState.IsValid)
            {
                var sb = new StringBuilder();
                sb.AppendFormat("<p>Förnamn: {0}</p>", model.Firstname);
                sb.AppendFormat("<p>Efternamn: {0}</p>", model.Lastname);
                sb.AppendFormat("<p>Email: {0}</p>", model.Email);
                sb.AppendFormat("<p>Adress: {0}</p>", model.Adress);
                sb.AppendFormat("<p>Postnr: {0}</p>", model.Zipcode);
                sb.AppendFormat("<p>Postort: {0}</p>", model.City);
                sb.AppendFormat("<p>Antal kort: {0}</p>", model.NrCard);
                sb.AppendFormat("<p>Kampanjkod: {0}</p>", model.PromotionalCode);

                library.SendMail(model.Email, email, subject, sb.ToString(), true);

                //send confirmation mail
                library.SendMail(model.Email, model.Email, subjectConfirm, confirmMail.ToString(), true);
            }

            //Redirect to the succes page.
            var child = CurrentPage.Children.FirstOrDefault();
            if (child != null)
            {
                return RedirectToUmbracoPage(child);
            }

            return RedirectToCurrentUmbracoPage();

        }
    }
}