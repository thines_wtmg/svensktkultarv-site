﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Umbraco.Core.Models;
using Umbraco.Extensions.Models.Custom;
using Umbraco.Extensions.Models.Generated;
using umbraco.interfaces;
using umbraco.NodeFactory;
using Umbraco.Web;
using Umbraco.Web.Models;

namespace Umbraco.Extensions.Models
{
    public interface IMasterModel
    {
        IEnumerable<MenuItem> MenuItems { get; set; }
        string SeoTitle { get; set; }
        string SeoDescription { get; set; }
        string Twitter { get; set; }
        string Facebook { get; set; }
        Adress FooterAdress { get; set; }
        string AboutUsText { get; set; }
        UrlPicker.Umbraco.Models.UrlPicker AboutUsLink { get; set; }
        IPublishedContent CurrentPage { get; set; }
        IPublishedContent SwedishHeritageCardInfoPage { get; set; }
        Umbraco.Extensions.Models.Generated.Image SwedishHeritageCardImage { get; set; }
        IPublishedContent MobilAppInfoPage { get; set; }
        IEnumerable<Annons> Ads { get; set; }
        bool ShowSideBarMenu { get; set; }
        IPublishedContent SideBarMenuStart { get; set; }
    }

    public class MasterModel<T> : RenderModel<T>, IMasterModel
        where T : class, IPublishedContent
    {
        public MasterModel(T content) : base(content) { }

        public IEnumerable<MenuItem> MenuItems { get; set; }
        public string SeoTitle { get; set; }
        public string SeoDescription { get; set; }
        public string Twitter { get; set; }
        public string Facebook { get; set; }
        public Adress FooterAdress { get; set; }
        public string AboutUsText { get; set; }
        public UrlPicker.Umbraco.Models.UrlPicker AboutUsLink { get; set; }
        public IPublishedContent CurrentPage { get; set; }
        public IPublishedContent SwedishHeritageCardInfoPage { get; set; }
        public Umbraco.Extensions.Models.Generated.Image SwedishHeritageCardImage { get; set; }
        public IPublishedContent MobilAppInfoPage { get; set; }
        public IEnumerable<Annons> Ads { get; set; }
        public IPublishedContent SideBarMenuStart { get; set; }
        public bool ShowSideBarMenu { get; set; }
    } 
}