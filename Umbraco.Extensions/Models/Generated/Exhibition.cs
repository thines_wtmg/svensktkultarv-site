﻿using Archetype.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using nuPickers;
using Our.Umbraco.PropertyConverters.Models;
using Umbraco.Core.Models;
using Umbraco.Extensions.Models.Custom;
using Umbraco.Web;
using Zbu.ModelsBuilder;

namespace Umbraco.Extensions.Models.Generated
{
    public partial class Exhibition
    {
        public Attraction RelatedAttraction { get; set; }

        public IEnumerable<Exhibition> RelevantExhibitions { get; set; }

        public OpeningHours OrdinaryOpeningHours { get; set; }

        /// <summary>Slider</summary>
        [ImplementPropertyType("flexSlider")]
        public IEnumerable<FlexSlider> FlexSlider
        {
            get
            {
                var archetypeModel = this.GetPropertyValue<ArchetypeModel>("flexSlider");
                return archetypeModel.Select(x => new FlexSlider()
                {
                    Image = x.GetValue<Image>("slideImage"),
                    ImageText = x.GetValue<string>("imageText"),
                    ImageAltText = x.GetValue<string>("alternativImageText"),
                    Photographer = x.GetValue<string>("photographer")
                }).ToList();
            }
        }

        /// <summary>Kategori</summary>
        [ImplementPropertyType("exhibitionCategories")]
        public IEnumerable<IPublishedContent> ExhibitionCategories
        {
            get
            {
                return this.GetPropertyValue<Picker>("exhibitionCategories").AsPublishedContent();
            }
        }
    }
}