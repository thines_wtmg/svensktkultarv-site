﻿using Archetype.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using nuPickers;
using Umbraco.Core.Models;
using Umbraco.Extensions.Models.Custom;
using Umbraco.Extensions.Utilities;
using Umbraco.Web;
using Zbu.ModelsBuilder;

namespace Umbraco.Extensions.Models.Generated
{
    public partial class Attraction
    {
        public IEnumerable<Exhibition> RelevantExhibitions { get; set; }

        /// <summary>Slider</summary>
        [ImplementPropertyType("flexSlider")]
        public IEnumerable<FlexSlider> FlexSlider
        {
            get
            {
                var archetypeModel = this.GetPropertyValue<ArchetypeModel>("flexSlider");
                return archetypeModel.Select(x => new FlexSlider()
                {
                    Image = x.GetValue<Image>("slideImage"),
                    ImageText = x.GetValue<string>("imageText"),
                    ImageAltText = x.GetValue<string>("alternativImageText"),
                    Photographer = x.GetValue<string>("photographer")
                }).ToList();
            }
        }

        /// <summary>Kategori</summary>
        [ImplementPropertyType("attractionCategories")]
        public IEnumerable<IPublishedContent> AttractionCategories
        {
            get
            {
                return this.GetPropertyValue<Picker>("attractionCategories").AsPublishedContent();
            }
        }

        /// <summary>Service</summary>
        [ImplementPropertyType("serviceCategories")]
        public IEnumerable<IPublishedContent> ServiceCategories
        {
            get
            {
                var serviceCategories = this.GetPropertyValue<Picker>("serviceCategories").AsPublishedContent();
                if (serviceCategories != null)
                {
                    return serviceCategories.Select(x => x as Category).ToList();
                }
                return null;
            }
        }

        public OpeningHours OrdinaryOpeningHours { get; set; }

        [ImplementPropertyType("exceptionsFromOrdinaryOpeningHours")]
        public IEnumerable<OpeningHoursException> ExceptionsFromOrdinaryOpeningHours
        {
            get
            {
                var archetypeModel = this.GetPropertyValue<ArchetypeModel>("exceptionsFromOrdinaryOpeningHours");
                return archetypeModel.Select(x => new OpeningHoursException()
                {
                    Date = x.GetValue<DateTime>("date"),
                    From = Helpers.GetTimeSpan(x.GetValue<string>("exceptionFrom")),
                    To = Helpers.GetTimeSpan(x.GetValue<string>("exceptionTo"))
                }).ToList();
            }

        }
    }
}