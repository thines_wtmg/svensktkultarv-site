﻿using Archetype.PropertyConverters;
using Archetype.Models;
using Archetype.Extensions;
using nuPickers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Umbraco.Core.Models;
using Umbraco.Extensions.Enums;
using Umbraco.Extensions.Utilities;
using Umbraco.Web;
using Zbu.ModelsBuilder;
using Umbraco.Extensions.Models.Custom;

namespace Umbraco.Extensions.Models.Generated
{
    public partial class Home
    {
        public IEnumerable<GeoContainer> GeoContainers { get; set; }
       
        public IEnumerable<Category> ServiceCategories { get; set; }

        public IEnumerable<Category> AttractionCategories { get; set; }

        public IEnumerable<Category> ExhibitionCategories { get; set; }

        public CategoriesViewModel Categories { get; set; }

        public IEnumerable<Annons> Ads { get; set; }

        [ImplementPropertyType("slider")]
        public IEnumerable<Slider> Slider
        {
            get
            {
                var archetypeModel = this.GetPropertyValue<ArchetypeModel>("slider");
                return archetypeModel.Select(x => new Slider()
                {
                    Title = x.GetValue<string>("title"),
                    SubHeading = x.GetValue<string>("subHeading"),
                    Text = x.GetValue<string>("text"),
                    Link = x.GetValue<IPublishedContent>("link"),
                    GetContentFromLink = x.GetValue<bool>("getContentFromLink"),
                    Image = x.GetValue<Image>("slideImage")
                }).ToList();
            }
        }
    }
}