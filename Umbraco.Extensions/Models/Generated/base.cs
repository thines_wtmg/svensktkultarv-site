﻿
using SEOChecker.MVC;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Umbraco.Core.Models.PublishedContent;
using Umbraco.Web;
using Zbu.ModelsBuilder;

namespace Umbraco.Extensions.Models.Generated
{
    public partial class Base 
    {
        /// <summary>SEO</summary>
        [ImplementPropertyType("seo")]
        public MetaData Seo
        {
            get { return this.GetPropertyValue<MetaData>("seo"); }
        }
    }
}