﻿using Archetype.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Umbraco.Extensions.Models.Custom;
using Umbraco.Web;
using Zbu.ModelsBuilder;

namespace Umbraco.Extensions.Models.Generated
{
    public partial class Standard 
    {
        /// <summary>Slider</summary>
        [ImplementPropertyType("flexSlider")]
        public IEnumerable<FlexSlider> FlexSlider
        {
            get
            {
                var archetypeModel = this.GetPropertyValue<ArchetypeModel>("flexSlider");
                return archetypeModel.Select(x => new FlexSlider()
                {
                    Image = x.GetValue<Image>("slideImage"),
                    ImageText = x.GetValue<string>("imageText"),
                    ImageAltText = x.GetValue<string>("alternativImageText"),
                    Photographer = x.GetValue<string>("photographer")
                }).ToList();
            }
        }
    }
}