﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Umbraco.Extensions.Models.Custom
{
    public class Adress
    {
        public string Adress1 { get; set; }
        public string Adress2 { get; set; }
        public string Adress3 { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
    }
}

