﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Umbraco.Extensions.Models.Generated;

namespace Umbraco.Extensions.Models.Custom
{
    public class FlexSlider
    {
        public Image Image { get; set; }
        public string ImageText { get; set; }
        public string ImageAltText { get; set; }
        public string Photographer { get; set; }
    }
}