﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Umbraco.Extensions.Models.Custom
{
    public class OpeningHours
    {
        public OpeningHours()
        {
            this.Monday = new DayOpeningHours();
            this.Tuesday = new DayOpeningHours();
            this.Wednesday = new DayOpeningHours();
            this.Thursday = new DayOpeningHours();
            this.Friday = new DayOpeningHours();
            this.Saturday = new DayOpeningHours();
            this.Sunday = new DayOpeningHours();
        }

        public DayOpeningHours Monday { get; set; }
        public DayOpeningHours Tuesday { get; set; }
        public DayOpeningHours Wednesday { get; set; }
        public DayOpeningHours Thursday { get; set; }
        public DayOpeningHours Friday { get; set; }
        public DayOpeningHours Saturday { get; set; }
        public DayOpeningHours Sunday { get; set; }
        public string OpeningHoursExceptionsHeading { get; set; }
        public string OpeningHoursGenericExceptions { get; set; }
    }

    public class DayOpeningHours
    {
        public DayOpeningHours()
        {
            
        }
        public TimeSpan From { get; set; }
        public TimeSpan To { get; set; }
    }

    public class OpeningHoursException
    {
        public DateTime Date { get; set; }
        public TimeSpan From { get; set; }
        public TimeSpan To { get; set; }
    }
}