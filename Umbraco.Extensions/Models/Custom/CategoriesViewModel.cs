﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Umbraco.Extensions.Models.Generated;

namespace Umbraco.Extensions.Models.Custom
{
    public class CategoriesViewModel
    {
        public IEnumerable<Category> ServiceCategories { get; set; }
        public IEnumerable<Category> AttractionCategories { get; set; }
        public IEnumerable<Category> ExhibitionCategories { get; set; }
    }
}