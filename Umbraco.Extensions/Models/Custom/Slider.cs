﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Umbraco.Core.Models;
using Umbraco.Extensions.Models.Generated;

namespace Umbraco.Extensions.Models.Custom
{
    public class Slider
    {
        public string Title { get; set; }
        public string SubHeading { get; set; }
        public string Text { get; set; }
        public IPublishedContent Link { get; set; }
        public bool GetContentFromLink { get; set; }
        public Image Image { get; set; }
    }
}