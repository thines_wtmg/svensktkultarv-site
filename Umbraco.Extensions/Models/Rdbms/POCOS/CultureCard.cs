﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Umbraco.Core.Persistence;
using Umbraco.Core.Persistence.DatabaseAnnotations;

namespace Umbraco.Extensions.Models.Rdbms.POCOS
{
    [System.Web.DynamicData.TableName("CultureCard")]
    [PrimaryKey("id")]
    [ExplicitColumns]
    public class CultureCard
    {
        [Column("id")]
        [PrimaryKeyColumn(AutoIncrement = true)]
        public int Id { get; set; }

        [Column("PhoneNumber")]
        public string PhoneNumber { get; set; }

        [Column("DeviceId")]
        public string DeviceId { get; set; }

        [Column("FirstName")]
        public string FirstName { get; set; }

        [Column("LastName")]
        public string LastName { get; set; }

        [Column("ZipCode")]
        public string ZipCode { get; set; }

        [Column("ExpireDate")]
        public DateTime ExpireDate { get; set; }

        public override string ToString()
        {
            return FirstName + " " + LastName;
        }

    }
}