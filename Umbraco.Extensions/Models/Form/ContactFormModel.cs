﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Umbraco.Extensions.Models.Form
{
    public class ContactFormModel
    {
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public string Adress { get; set; }
        public string Zipcode { get; set; }
        public string City { get; set; }
        public string NrCard { get; set; }
        public string Email { get; set; }
        public string PromotionalCode { get; set; }
    }
}