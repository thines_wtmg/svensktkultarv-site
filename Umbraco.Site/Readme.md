﻿Mobil API:
Endpoints

Attractions
/api/v1/mobil/attractions/
/api/v1/mobil/attraction/{id}

Exhibitions
/api/v1/mobil/exhibitions/
/api/v1/mobil/exhibition/{id}

Categories
/api/v1/mobil/categories/
/api/v1/mobil/servicecategories/
/api/v1/mobil/attractioncategories/
/api/v1/mobil/exhibitioncategories/

Counties
/api/v1/mobil/counties/

CultureCard

[GET]
/api/v1/mobil/culturecards/
/api/v1/mobil/culturecard/{number}/{zip}

[POST]
/api/v1/mobil/culturecard/add/
/api/v1/mobil/culturecard/update/
