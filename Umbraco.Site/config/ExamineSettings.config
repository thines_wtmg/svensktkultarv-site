﻿<?xml version="1.0"?>
<!-- 
Umbraco examine is an extensible indexer and search engine.
This configuration file can be extended to add your own search/index providers.
Index sets can be defined in the ExamineIndex.config if you're using the standard provider model.

More information and documentation can be found on CodePlex: http://umbracoexamine.codeplex.com
-->
<Examine>
  <ExamineIndexProviders>
    <providers>
      <add name="InternalIndexer" type="UmbracoExamine.UmbracoContentIndexer, UmbracoExamine"
           supportUnpublished="true"
           supportProtected="true"
           analyzer="Lucene.Net.Analysis.WhitespaceAnalyzer, Lucene.Net"/>

      <add name="InternalMemberIndexer" type="UmbracoExamine.UmbracoMemberIndexer, UmbracoExamine"
           supportUnpublished="true"
           supportProtected="true"
           analyzer="Lucene.Net.Analysis.Standard.StandardAnalyzer, Lucene.Net"/>

      <!-- default external indexer, which excludes protected and unpublished pages-->
      <add name="ExternalIndexer" type="UmbracoExamine.UmbracoContentIndexer, UmbracoExamine"/>

      <!-- Indexer for attractions, only searches public published content-->
      <add name="AttractionIndexer" type="UmbracoExamine.UmbracoContentIndexer, UmbracoExamine"
           runAsync="true"
           supportUnpublished="false"
           supportProtected="false"
           interval="10"
           analyzer="Lucene.Net.Analysis.WhitespaceAnalyzer, Lucene.Net"
           indexSet="AttractionIndexSet"/>
      
    <!-- Indexer for attractions, only searches public published content-->
      <add name="ExhibitionIndexer" type="UmbracoExamine.UmbracoContentIndexer, UmbracoExamine"
           runAsync="true"
           supportUnpublished="false"
           supportProtected="false"
           interval="10"
           analyzer="Lucene.Net.Analysis.WhitespaceAnalyzer, Lucene.Net"
           indexSet="ExhibitionIndexSet"/>

    </providers>
  </ExamineIndexProviders>

  <ExamineSearchProviders defaultProvider="ExternalSearcher">
    <providers>
      <add name="InternalSearcher"
           type="UmbracoExamine.UmbracoExamineSearcher, UmbracoExamine"
           analyzer="Lucene.Net.Analysis.WhitespaceAnalyzer, Lucene.Net"/>

      <add name="ExternalSearcher"
           type="UmbracoExamine.UmbracoExamineSearcher, UmbracoExamine" />

      <add name="AttractionSearcher"
           type="UmbracoExamine.UmbracoExamineSearcher, UmbracoExamine"
           analyzer="Lucene.Net.Analysis.WhitespaceAnalyzer, Lucene.Net"
           indexSet="AttractionIndexSet"/>
      
      <add name="ExhibitionSearcher"
           type="UmbracoExamine.UmbracoExamineSearcher, UmbracoExamine"
           analyzer="Lucene.Net.Analysis.WhitespaceAnalyzer, Lucene.Net"
           indexSet="ExhibitionIndexSet"/>

      <add name="InternalMemberSearcher"
           type="UmbracoExamine.UmbracoExamineSearcher, UmbracoExamine"
           analyzer="Lucene.Net.Analysis.Standard.StandardAnalyzer, Lucene.Net"
           enableLeadingWildcard="true"/>

    </providers>
  </ExamineSearchProviders>

</Examine>
