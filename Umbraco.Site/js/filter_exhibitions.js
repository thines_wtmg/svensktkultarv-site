﻿$(document).ready(function () {

    //Check If we have exhibition filter view.
    if ($(".exhibition-search").length > 0) {

        //Globals
        var currentExhibitionView = "list";
        var maxExhibitionPageSize = $("#settings").data('maxpagesize');
        var exhibitionMarkersArray = [];
        var mapExhibitionCenter = new window.google.maps.LatLng(61.47519350656129, 15.638281737499938);
        var mapExhibition;
        var exhibitionPageSize = $("#settings").data('initialpagesize');
        var cultureCardAdImage = $("#culture-card").data("image");
       
        //Init 
        $(function () {

            $(".exhibition-result-map-view").hide();

            //Get filter parameters from querystring.
            var initialNodes = getParameterByName('nodes');
            var initialCategories = getParameterByName('categories');
            var initialServiceCategories = getParameterByName('serviceCategories');
            var initialStart = getParameterByName('start');
            var initialStop = getParameterByName('stop');

            //Get initial exhibitions
            getInitialExhibitions(initialNodes, initialCategories, initialServiceCategories, initialStart, initialStop);
            initExhibitionDatePickers(initialStart, initialStop);

            //Initialize Map
            window.google.maps.event.addDomListener(window, 'load', initializeExhibitionMap);
            window.google.maps.event.addDomListener(window, "resize", function () {
                var center = mapExhibition.getCenter();
                window.google.maps.event.trigger(mapExhibition, "resize");
                mapExhibition.setCenter(center);
            });

            //Update the Ui to reflect selected filters
            setExhibitionGeoContainersToFilterOn(initialNodes);
            setExhibitionCategoriesToFilterOn(initialCategories);
            setExhibitionServiceCategoriesToFilterOn(initialServiceCategories);
        });

        /** Data retrieval functions */

        //Get All exhibitions or if on filterpage get selected filters, using this for the first request.
        function getInitialExhibitions(initialNodes, initialCategories, initialServiceCategories, initialStart, initialStop) {

            var apiUrl = '';
            var requestData = {};

            if (initialNodes == '' && initialCategories == '' && initialServiceCategories == '' && initialStart == '' && initialStop == '') {
                apiUrl = '/api/v1/exhibitions/getallforhome';
                requestData = { initialPageSize: exhibitionPageSize, page: 0 };
            } else {
                apiUrl = '/api/v1/exhibitions/getfiltered';
                requestData = { nodeIds: initialNodes, exhibitionCategories: initialCategories, serviceCategories: initialServiceCategories, startDate: initialStart, stopDate: initialStop, initialPageSize: exhibitionPageSize, page: 0 };
            }
            $.ajax({
                url: apiUrl,
                type: 'GET',
                dataType: 'json',
                data: requestData,
                success: function (data) {
                    setTimeout(function () {
                        writeExhibitionResponse(data.Exhibitions);
                        updateExhibitionUi(data);
                        $('#js-load-exhibitions').fadeOut(200);
                    }, 600);

                },
                error: function (x, y, z) {
                    console.log(x, y, z);
                }
            });
        }

        //Get All exhibitions
        function getAllExhibitions(initialPageSize, page, appendResult) {
            $.ajax({
                url: '/api/v1/exhibitions/getallforhome',
                type: 'GET',
                dataType: 'json',
                data: { page: page, initialPageSize: initialPageSize },
                success: function (data) {
                    updateExhibitionUi(data);

                    if (currentExhibitionView == "list") {
                        setTimeout(function () {
                            if (appendResult == null || appendResult === false) {
                                writeExhibitionResponse(data.Exhibitions);
                            } else {
                                appendExhibitionResponse(data.Exhibitions);
                            }
                            $('#js-load-exhibitions').fadeOut(200);
                        }, 600);
                    }
                },
                error: function (x, y, z) {
                    console.log(x, y, z);
                }
            });
        }

        //Get All exhibitions for google map
        function getAllExhibitionsForMap() {
            removeExhibitionMarkers();
            $.ajax({
                url: '/api/v1/exhibitions/getallforhome',
                type: 'GET',
                dataType: 'json',
                data: { page: 0, initialPageSize: maxExhibitionPageSize },
                success: function (data) {
                    updateExhibitionUi(data);
                    $.each(data.Exhibitions, function (index, exhibition) {

                        //If the marker for related attraction isn't added than, add it!
                        if (!isMarkerAdded(exhibition.RelatedAttraction)) {
                            var latlng = new window.google.maps.LatLng(exhibition.Lat, exhibition.Lng);
                            var marker = new window.google.maps.Marker({
                                map: mapExhibition,
                                position: latlng,
                                title: exhibition.RelatedAttraction
                            });

                            marker.info = new window.google.maps.InfoWindow({
                                content: ""
                            });

                            window.google.maps.event.addListener(marker, 'click', function () {
                                loadContentForMarker(marker, exhibition.FK_RelatedAttraction, exhibition.RelatedAttraction);
                                map.panTo(this.getPosition());
                            });

                            //Save markers, so we can manipulate them later.
                            exhibitionMarkersArray.push(marker);
                        }
                    });
                },
                error: function (x, y, z) {
                    console.log(x, y, z);
                }
            });
        }

        //Get filtered exhibitions
        function getFilteredExhibitions(nodes, categories, servicecategories, start, stop, initialPageSize, page, appendResult) {

            $.ajax({
                url: '/api/v1/exhibitions/getfiltered',
                type: 'GET',
                dataType: 'json',
                data: { nodeIds: nodes, exhibitionCategories: categories, serviceCategories: servicecategories, startDate: start, stopDate: stop, page: page, initialPageSize: initialPageSize },
                success: function (data) {
                    updateExhibitionUi(data);

                    if (currentExhibitionView == "list") {
                        setTimeout(function () {
                            if (appendResult == null || appendResult === false) {
                                writeExhibitionResponse(data.Exhibitions);
                            } else {
                                appendExhibitionResponse(data.Exhibitions);
                            }
                            $('#js-load-exhibitions').fadeOut(200);
                        }, 600);
                    }
                },
                error: function (x, y, z) {
                    console.log(x, y, z);
                }
            });
        }

        //Get filtered exhibitions for google map
        function getFilteredExhibitionsForMap(nodes, categories, servicecategories, start, stop, initialPageSize, page, appendResult) {
            removeExhibitionMarkers();
            $.ajax({
                url: '/api/v1/exhibitions/getfiltered',
                type: 'GET',
                dataType: 'json',
                data: { nodeIds: nodes, exhibitionCategories: categories, serviceCategories: servicecategories, startDate: start, stopDate: stop, page: 0, initialPageSize: maxExhibitionPageSize },
                success: function (data) {
                    updateExhibitionUi(data);
                    $.each(data.Exhibitions, function (index, exhibition) {

                        //If the marker for related attraction isn't added than, add it!
                        if (!isMarkerAdded(exhibition.RelatedAttraction)) {
                            var latlng = new window.google.maps.LatLng(exhibition.Lat, exhibition.Lng);
                            var marker = new window.google.maps.Marker({
                                map: mapExhibition,
                                position: latlng,
                                title: exhibition.RelatedAttraction
                            });

                            marker.info = new window.google.maps.InfoWindow({
                                content: ""
                            });

                            window.google.maps.event.addListener(marker, 'click', function () {
                                loadContentForMarker(marker, exhibition.FK_RelatedAttraction, exhibition.RelatedAttraction);
                                map.panTo(this.getPosition());
                            });

                            //Save markers, so we can manipulate them later.
                            exhibitionMarkersArray.push(marker);
                        }
                    });
                },
                error: function (x, y, z) {
                    console.log(x, y, z);
                }
            });
        }

        /** END Data retrieval functions */

        function initExhibitionDatePickers(defaultStart, defaultStop) {
            $("#startDate").datepicker({
                monthNames: ["Januari", "Februari", "Mars", "April", "Maj", "Juni", "Juli", "Augusti", "September", "Oktober", "November", "December"],
                dayNamesMin: ["Sön", "Mån", "Tis", "Ons", "Tor", "Fre", "Lör"],
                onSelect: function (dateText, inst) {
                    var selectedDate = new Date(inst.selectedYear, inst.selectedMonth, inst.selectedDay);
                    var today = new Date();
                    var selectedStopDate = $("#stopDate").datepicker('getDate');
                    //Make sure we dont can select stopdates that occure before startdate. or todays date
                    if (selectedDate > today)
                        $("#stopDate").datepicker('option', 'minDate', selectedDate);
                    else
                        $("#stopDate").datepicker('option', 'minDate', today);

                    if (selectedDate > selectedStopDate) {
                        $('#spanSelectedStopDate').html(' - ' + $.datepicker.formatDate("yymmdd", selectedDate));
                        $('#chosenStopDate').val($.datepicker.formatDate("yymmdd", selectedDate));
                    }
                    var currentStartDate = $.datepicker.formatDate("yymmdd", $("#startDate").datepicker('getDate'));
                    $('#chosenStartDate').val(currentStartDate);
                    $('#spanSelectedStartDate').html(currentStartDate);
                }
            });
            $("#stopDate").datepicker({
                monthNames: ["Januari", "Februari", "Mars", "April", "Maj", "Juni", "Juli", "Augusti", "September", "Oktober", "November", "December"],
                dayNamesMin: ["Sön", "Mån", "Tis", "Ons", "Tor", "Fre", "Lör"],
                minDate: new Date(),
                onSelect: function () {
                    var currentStopDate = $.datepicker.formatDate("yymmdd", $("#stopDate").datepicker('getDate'));

                    //If user clicks stop date before start date, make sure we also set the start date.
                    if ($('#chosenStartDate').val() == '') {
                        $("#startDate").datepicker("setDate", new Date());
                        $('#chosenStartDate').val($.datepicker.formatDate("yymmdd", new Date()));
                        $('#spanSelectedStartDate').html($.datepicker.formatDate("yymmdd", new Date()));
                    }

                    $('#chosenStopDate').val(currentStopDate);
                    $('#spanSelectedStopDate').html(' - ' + currentStopDate);

                    //TODO:Refactor..
                    var nodes = getExhibitionGeoContainersToFilterOn();
                    var categories = getExhibitionCategoriesToFilterOn();
                    var serviceCategories = getExhibitionServiceCategoriesToFilterOn();
                    var start = getExhibitionFilterStartDate();
                    var stop = getExhibitionFilterStopDate();

                    if (currentExhibitionView == "list") {
                        $('#js-load-exhibitions').show();
                    }

                    //Get exhibitions
                    getFilteredExhibitions(nodes, categories, serviceCategories, start, stop);

                }
            });

            if (defaultStart != "") {
                var startD = new Date('' + defaultStart.substring(0, 4) + '-' + defaultStart.substring(4, 6) + '-' + defaultStart.substring(6, 8));
                var stopD = new Date(defaultStop.substring(0, 4) + '-' + defaultStop.substring(4, 6) + '-' + defaultStop.substring(6, 8));

                //DatePicker selected dates
                $("#startDate").datepicker("setDate", startD);
                $("#stopDate").datepicker("setDate", stopD);
                //Selected dates to filter on
                $('#chosenStartDate').val(defaultStart);
                $('#chosenStopDate').val(defaultStop);
                //Selected dates displayed to user
                $('#spanSelectedStartDate').html(defaultStart);
                $('#spanSelectedStopDate').html(' - ' + defaultStop);
            }
        }

        /** UI interactions  */
        //Clicking search
        $("#exhibition-dropdowns .checkbox").click(function () {
            $("#exhibition-pager").data("currentpage", 0);//Reset currentPage for ajax page
            $(this).toggleClass('checked');

            var nodes = getExhibitionGeoContainersToFilterOn();
            var categories = getExhibitionCategoriesToFilterOn();
            var serviceCategories = getExhibitionServiceCategoriesToFilterOn();
            var start = getExhibitionFilterStartDate();
            var stop = getExhibitionFilterStopDate();

            if (currentExhibitionView == "list") {
                $('#js-load-exhibitions').fadeIn(200);
            }

            //Do we have counties or categories?
            if (nodes.length > 1 || categories.length > 1 || serviceCategories.length > 1 || (start.length > 6 && stop.length > 6)) {
                if (currentExhibitionView == "list") {
                    getFilteredExhibitions(nodes, categories, serviceCategories, start, stop, exhibitionPageSize, 0);
                } else {
                    getFilteredExhibitionsForMap(nodes, categories, serviceCategories, start, stop, exhibitionPageSize, 0);
                }
            }
            else {
                if (currentExhibitionView == "list") {
                    getAllExhibitions(exhibitionPageSize, 0);
                } else {
                    getAllExhibitionsForMap();
                }
            }
        });

        //Ladda fler på filtrerings sida.
        $("#exhibition-pager").click(function () {
            var nodes = getExhibitionGeoContainersToFilterOn();
            var categories = getExhibitionCategoriesToFilterOn();
            var serviceCategories = getExhibitionServiceCategoriesToFilterOn();
            var start = getExhibitionFilterStartDate();
            var stop = getExhibitionFilterStopDate();
            var currentPage = parseInt($("#exhibition-pager").data("currentpage"));
            $("#exhibition-pager").data("currentpage", currentPage + 1);

            //Do we have counties or categories?
            if (nodes.length > 1 || categories.length > 1 || serviceCategories.length > 1) {
                getFilteredExhibitions(nodes, categories, serviceCategories, start, stop, exhibitionPageSize, currentPage + 1, true);
            }
            else {
                getAllExhibitions(exhibitionPageSize, currentPage + 1, true);
            }
        });

        //Clicking 'Visa på karta'
        $("#js-exhibition-view-map").click(function () {
            currentExhibitionView = "map";
            var nodes = getExhibitionGeoContainersToFilterOn();
            var categories = getExhibitionCategoriesToFilterOn();
            var serviceCategories = getExhibitionServiceCategoriesToFilterOn();
            var start = getExhibitionFilterStartDate();
            var stop = getExhibitionFilterStopDate();

            $(".exhibition-result-grid-view").hide();
            $('#js-exhibition-view-grid').removeClass('active');
            $(this).addClass('active');

            if (nodes.length > 1 || categories.length > 1 || serviceCategories.length > 1 || (start.length > 6 && stop.length > 6)) {
                getFilteredExhibitionsForMap(nodes, categories, serviceCategories, start, stop, exhibitionPageSize, 0);
            }
            else {
                getAllExhibitionsForMap();
            }

            $(".exhibition-result-map-view").fadeIn(function () {
                redrawExhibitionMap();
            });

        });

        //Clicking 'Visa i lista'
        $("#js-exhibition-view-grid").click(function () {

            currentExhibitionView = "list";
            var nodes = getExhibitionGeoContainersToFilterOn();
            var categories = getExhibitionCategoriesToFilterOn();
            var serviceCategories = getExhibitionServiceCategoriesToFilterOn();
            var start = getExhibitionFilterStartDate();
            var stop = getExhibitionFilterStopDate();

            $(".exhibition-result-map-view").hide();
            $('#js-exhibition-view-map').removeClass('active');
            $(this).addClass('active');

            $(".exhibition-result-grid-view").fadeIn(function () {
                if (nodes.length > 1 || categories.length > 1 || serviceCategories.length > 1 || (start.length > 6 && stop.length > 6)) {
                    getFilteredExhibitions(nodes, categories, serviceCategories, start, stop, exhibitionPageSize, 0);
                } else {
                    getAllExhibitions(exhibitionPageSize, 0);
                }
            });
        });

        //Dropdowns for counties and categories.
        $('#exhibition-sorting-controls span.sort').click(function () {
            $this = $(this);
            var id = $(this).attr('data-id');
            var dropdown = $('#exhibition-dropdowns .dropdown.' + id);

            if ($this.hasClass('active')) {
                $this.removeClass('active');
            }
            else {
                $('span.sort').removeClass('active');
                $this.addClass('active');
            }

            if (dropdown.hasClass('toggled')) {
                dropdown.removeClass('toggled');
            }
            else {
                $('#exhibition-dropdowns .dropdown').removeClass('toggled');
                dropdown.addClass('toggled');
            }
        });

        //Clicking 'Rensa urval'
        $('#js-clear-exhibition-filter').click(function () {
            $('#exhibition-dropdowns .checkbox').each(function (index) {
                $(this).removeClass('checked');
            });
            if (currentExhibitionView == "list") {
                $('#js-load-exhibitions').show();
                getAllExhibitions();
            } else {
                getAllExhibitionsForMap();
            }
            $("#exhibition-pager").data("currentpage", 0);
        });

        //Clicking 'Visa fler'
        $('#exhibition-forward').click(function () {

            var nodes = getExhibitionGeoContainersToFilterOn();
            var categories = getExhibitionCategoriesToFilterOn();
            var serviceCategories = getExhibitionServiceCategoriesToFilterOn();
            var start = getExhibitionFilterStartDate();
            var stop = getExhibitionFilterStopDate();

            window.location.href = window.location.protocol + '//' + window.location.host + '/utstaellningar/?nodes=' + nodes + '&categories=' + categories + '&serviceCategories=' + serviceCategories + '&start=' + start + '&stop=' + stop + '';
        });

        /** END - UI interactions */

        /** Maps functions */
        //Initialize google map
        function initializeExhibitionMap() {
            var mapCanvas = $("#map_exhibition_canvas_home");
            var zoomFactor = 5;
            var options = {
                center: mapExhibitionCenter,
                zoom: zoomFactor,
                scaleControl: true,
                mapTypeId: window.google.maps.MapTypeId.ROADMAP
            };

            mapExhibition = new window.google.maps.Map(document.getElementById
               ("map_exhibition_canvas_home"), options);

            window.google.maps.event.addListenerOnce(mapExhibition, 'idle', function () {
                window.google.maps.event.trigger(mapExhibition, 'resize');
            });
        }

        //Get related exhibitions for attraction and adding those to marker.
        function loadContentForMarker(marker, id, parentName) {
            $.ajax({
                url: '/api/v1/exhibitions/getbyattraction/' + id,
                success: function (data) {
                    var content = "<div class=\"marker-info-window\"><h2> " + parentName + "</h2><strong>Utställningar</strong><br/></div><ul>";
                    $.each(data, function (index, exhibition) {
                        content += "<li><a href=\"" + exhibition.Link + "\">" + exhibition.Name + "</a></li>";
                    });
                    marker.info.content = content + "</ul>";
                    marker.info.open(mapExhibition, marker);
                }
            });
        }

        //Check if map marker is added, compare with title..
        function isMarkerAdded(title) {
            for (i = 0; i < exhibitionMarkersArray.length; i++) {
                if (exhibitionMarkersArray[i].title == title)
                    return true;
            }
            return false;
        }

        //Remove all markers
        function removeExhibitionMarkers() {
            for (i = 0; i < exhibitionMarkersArray.length; i++) {
                exhibitionMarkersArray[i].setMap(null);
            }
            exhibitionMarkersArray.length = 0;
        }

        //Redraw map, useful when displaying map after some animation. etc.
        function redrawExhibitionMap() {
            var center = mapExhibition.getCenter();
            window.google.maps.event.trigger(mapExhibition, "resize");
            mapExhibition.setCenter(center);
        }
        /** END - Maps functions */

        /** Helper functions */
        //Writing respons when on list view.
        function writeExhibitionResponse(exhibitions) {
            var strResult = "";
            var cultureCardAdHeading = $("#culture-card").data("heading");
            var cultureCardAdText = $("#culture-card").data("text");
            var cultureCardAdLink = $("#culture-card").data("link");
            $.each(exhibitions, function (index, exhibition) {
                //Inject culture-card as 4th element.
                if (index == 3) {
                    strResult += "<a href=\"" + cultureCardAdLink + " \" class=\"col-4\"><div class=\"grid-item ad e-ccard\"></div></a><a href=\"" + exhibition.Link + " \" class=\"col-4 box\"><div class=\"grid-item\"><img src=\"" + exhibition.MainImage + "\"><span class=\"button default yellow\">Läs mer</span><div class=\"copy\"><h2 class=\"title box-item\">" + exhibition.Name + "</h2><p class=\"box-item\">" + exhibition.City + "</p><span class=\"date\">" + exhibition.Date + "</span></div></div></a>";
                } else {
                    strResult += "<a href=\"" + exhibition.Link + " \" class=\"col-4 box\"><div class=\"grid-item\"><img src=\"" + exhibition.MainImage + "\"><span class=\"button default yellow\">Läs mer</span><div class=\"copy\"><h2 class=\"title box-item\">" + exhibition.Name + "</h2><p class=\"box-item\">" + exhibition.City + "</p><span class=\"date\">" + exhibition.Date + "</span></div></div></a>";
                }

            });
            //If attractions contains less or equal to 3 objects then append culture-card last. 
            if (exhibitions.length <= 3) {
                strResult += "<a href=\"" + cultureCardAdLink + " \" class=\"col-4\"><div class=\"grid-item ad e-ccard\"></div></a>";
            }

            $("#divExhibitionsResult").html(strResult);

            //Init culture-card ad-area.
            $(".e-ccard").css('background-image', 'url("' +cultureCardAdImage +'")');
        }

        //Response to append when on list view and clicking "visa fler".
        function appendExhibitionResponse(exhibitions) {
            var strResult = "";
            $.each(exhibitions, function (index, exhibition) {
                strResult += "<a href=\"" + exhibition.Link + " \" class=\"col-4 box\"><div class=\"grid-item\"><img src=\"" + exhibition.MainImage + "\"><span class=\"button default yellow\">Läs mer</span><div class=\"copy\"><h2 class=\"title box-item\">" + exhibition.Name + "</h2><p class=\"box-item\">" + exhibition.City + "</p><span class=\"date\">" + exhibition.Date + "</span></div></div></a>";
            });

            $("#divExhibitionsResult").append(strResult);
        }

        //Get selected counties
        function getExhibitionGeoContainersToFilterOn() {
            var results = Array();
            var nodes = "";
            $("#geocontainers-exhibition-wrap .county-checkbox.checked").each(function () {
                results.push($(this).data("id"));
            });
            return nodes + results.join(',');
        }

        //Set selected counties
        function setExhibitionGeoContainersToFilterOn(nodes) {
            $("#geocontainers-exhibition-wrap .county-checkbox").each(function () {
                if (nodes.toString() != "" && nodes.contains($(this).data('id').toString()))
                    $(this).addClass('checked');
            });

        }

        //Get selected categories
        function getExhibitionCategoriesToFilterOn() {
            var results = Array();
            var nodes = "";
            $("#categories-exhibition-wrap .category-checkbox.checked").each(function () {
                results.push($(this).data("id"));
            });
            return nodes + results.join(',');
        }

        //Set selected categories
        function setExhibitionCategoriesToFilterOn(categories) {
            $("#categories-exhibition-wrap .category-checkbox").each(function () {
                if (categories.toString() != "" && categories.contains($(this).data('id').toString()))
                    $(this).addClass('checked');
            });
        }

        //Get selected service categories
        function getExhibitionServiceCategoriesToFilterOn() {
            var results = Array();
            var nodes = "";
            $("#categories-exhibition-wrap .service-category-checkbox.checked").each(function () {
                results.push($(this).data("id"));
            });
            return nodes + results.join(',');
        }

        //Set selected service categories
        function setExhibitionServiceCategoriesToFilterOn(serviceCategories) {
            $("#categories-exhibition-wrap .service-category-checkbox").each(function () {
                if (serviceCategories.toString() != "" && serviceCategories.contains($(this).data('id').toString()))
                    $(this).addClass('checked');
            });
        }

        //
        function getExhibitionFilterStartDate() {
            return $('#chosenStartDate').val();
        }

        //
        function getExhibitionFilterStopDate() {
            return $('#chosenStopDate').val();
        }

        //Updates counter for total amount of found exhibitions
        function updateExhibitionTotalCount(data) {
            var strResult = data.Total;
            $("#spanExhibitionTotalCount").text(strResult);
        }

        //Clears the chosen dates and resets datepickers.
        function resetExhibitionDatePickers() {
            $("#startDate").datepicker('setDate', null);
            $("#stopDate").datepicker('setDate', null);

            $("#startDate").datepicker("option", {
                minDate: null,
                maxDate: null
            });

            $("#stopDate").datepicker("option", {
                minDate: new Date(),
                maxDate: null
            });

            $('#chosenStartDate').val('');
            $('#chosenStopDate').val('');
            $('#spanSelectedStartDate').html('');
            $('#spanSelectedStopDate').html('');

        }

        //Update UI.
        function updateExhibitionUi(data) {
            updateExhibitionTotalCount(data);
        }
        /** END Helper functions */

    }//END Check if on start page

});