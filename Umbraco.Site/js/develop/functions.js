$(document).ready(function () {

    //FORM STUFF
    $('.js-label input').focus( function(){
        $(this).closest('.js-label').addClass('move-label');
    });

    $('.js-label input').blur( function(){
        if( $(this).val() == '' ){
            $(this).closest('.js-label').removeClass('move-label');
        }
    });
    //SLIDERS N THAT
    //FUNCTIONS
    function setSlideTextHeight(index) {
        var item = $('ul.slides li').eq(index);
        var itemSlideText = item.find('.slide-text');
        var heightOfText = itemSlideText.outerHeight() + 1;
        itemSlideText.css('bottom', '-' + heightOfText + 'px');
    }

    function removeClasses() {
        $('.text').removeClass('toggled');
        $('.toggle-icon-slidetext').removeClass('icon-down-dir');
        $('.toggle-icon-slidetext').addClass('icon-up-dir');
        $('.js-toggle-showhide').text('Visa bildtext');

        var item = $('ul.slides li');
        var itemSlideText = item.find('.slide-text');
        var itemSlideTextContainer = item.find('.text');
        var heightOfText = itemSlideText.outerHeight() + 1;
        itemSlideTextContainer.css('bottom', '0');
    }

    function addClasses() {
        $('.text').addClass('toggled');
        $('.toggle-icon-slidetext').removeClass('icon-up-dir');
        $('.toggle-icon-slidetext').addClass('icon-down-dir');
        $('.js-toggle-showhide').text('Dölj bildtext');
    }
    function showTextBox(index) {
        var item = $('ul.slides li').eq(index);
        var itemSlideText = item.find('.slide-text');
        var itemSlideTextContainer = item.find('.text');
        var heightOfText = itemSlideText.outerHeight() + 1;
        itemSlideTextContainer.css('bottom', '' + heightOfText + 'px');
    }
    if($('.flexslider .slides img').length > 0)
    {
        $('.flexslider').flexslider({
            animation: "fade",
            slideshowSpeed: 5000,
            slideshow: true,
            controlNav: false,
            start: function (i) {
                currentSlideIndex = $('.flexslider').data('flexslider').currentSlide;
                setSlideTextHeight(currentSlideIndex);
            },
            after: function (i) {
                currentSlideIndex = $('.flexslider').data('flexslider').currentSlide;
                setSlideTextHeight(currentSlideIndex);
            }
        });
    }
    

    var currentSlideIndex = 0;
    $('.js-toggle-text').click(function () {
        if ($('.text').hasClass('toggled')) {
            removeClasses();
        }
        else {
            showTextBox(currentSlideIndex);
            addClasses();
        }
    });
    //position slidetext on load
    if ($('.slide-text').length > 0) {
        setSlideTextHeight(0);
    }
    $('.flex-next').click(function () {
        removeClasses();
    });
    $('.flex-prev').click(function () {
        removeClasses();
    });

    //MOBILE MENU
    $('#js-toggle-menu').click(function(){
        $('.menus').toggleClass('active');

        if($(this).hasClass('icon-menu'))
        {
            $(this).removeClass('icon-menu');
            $(this).addClass('icon-cancel');
        }
        else 
        {
            $(this).removeClass('icon-cancel');
            $(this).addClass('icon-menu');
        }
    });

});
$(document).click(function(e){

    if( $(e.target).closest('.dropdown').length > 0 || $(e.target).closest('span.sort').length > 0 || $(e.target).closest('.ui-icon').length > 0 ) 
    {
        return false;
    }
    else
    {
        $('.dropdown').removeClass('toggled');
        $('span.sort').removeClass('active');
    }
});