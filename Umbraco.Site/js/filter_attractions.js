﻿$(document).ready(function () {
    
    //Check if we have attraction filter view.
    if ($(".attraction-search").length > 0) {

        //Globals
        var currentAttractionView = "list";
        var maxAttractionPageSize = $("#settings").data('maxpagesize');
        var attractionMarkersArray = [];
        var mapAttractionsCenter = new window.google.maps.LatLng(61.47519350656129, 15.638281737499938);
        var mapAttractions;
        var attractionPageSize = $("#settings").data('initialpagesize');
        var cultureCardAdImage = $("#culture-card").data("image");
        
        //Init 
        $(function () {

            $(".attraction-result-map-view").hide();
            
            //Get filter parameters from querystring.
            var initialNodes = getParameterByName('nodes');
            var initialCategories = getParameterByName('categories');
            var initialServiceCategories = getParameterByName('serviceCategories');

            //Get initial attraction
            getInitialAttractions(initialNodes, initialCategories, initialServiceCategories);

            //Initialize Map
            window.google.maps.event.addDomListener(window, 'load', initializeAttractionMap);
            window.google.maps.event.addDomListener(window, "resize", function () {
                var center = mapAttractions.getCenter();
                window.google.maps.event.trigger(mapAttractions, "resize");
                mapAttractions.setCenter(center);
            });

            //Update the Ui to reflect selected filters
            setAttractionGeoContainersToFilterOn(initialNodes);
            setAttractionCategoriesToFilterOn(initialCategories);
            setAttractionServiceCategoriesToFilterOn(initialServiceCategories);
        });

        /** Data retrieval functions */

        //Get All attractions or if on filterpage get selected filters, using this for the first request.
        function getInitialAttractions(initialNodes, initialCategories, initialServiceCategories) {

            var apiUrl = '';
            var requestData = {};

            if (initialNodes == '' && initialCategories == '' && initialServiceCategories == '') {
                apiUrl = '/api/v1/attractions/getallforhome';
                requestData = { initialPageSize: attractionPageSize, page: 0 };
            } else {
                apiUrl = '/api/v1/attractions/getfiltered';
                requestData = { nodeIds: initialNodes, attractionCategories: initialCategories, serviceCategories: initialServiceCategories, initialPageSize: attractionPageSize, page: 0 };
            }
            $.ajax({
                url: apiUrl,
                type: 'GET',
                dataType: 'json',
                data: requestData,
                success: function (data) {
                    setTimeout(function () {
                        writeAttractionResponse(data.Attractions);
                        updateAttractionUi(data);
                        $('#js-load-attractions').fadeOut(200);
                    }, 600);

                },
                error: function (x, y, z) {
                    console.log(x, y, z);
                }
            });
        }

        //Get All attractions
        function getAllAttractions(initialPageSize, page, appendResult) {
            $.ajax({
                url: '/api/v1/attractions/getallforhome',
                type: 'GET',
                dataType: 'json',
                data: { page: page, initialPageSize: initialPageSize },
                success: function (data) {
                    updateAttractionUi(data);
                    
                    if (currentAttractionView == "list") {
                        setTimeout(function () {
                            if (appendResult == null || appendResult === false) {
                                writeAttractionResponse(data.Attractions);
                            } else {
                                appendAttractionResponse(data.Attractions);
                            }
                            $('#js-load-attractions').fadeOut(200);
                        }, 600);
                    }
                },
                error: function (x, y, z) {
                    console.log(x, y, z);
                }
            });
        }

        //Get All attractions for google map
        function getAllAttractionsForMap() {
            removeAttractionMarkers();
            $.ajax({
                url: '/api/v1/attractions/getallforhome',
                type: 'GET',
                dataType: 'json',
                data: { page: 0, initialPageSize: maxAttractionPageSize },
                success: function (data) {
                    updateAttractionUi(data);
                    $.each(data.Attractions, function (index, attraction) {
                        var latlng = new window.google.maps.LatLng(attraction.Lat, attraction.Lng);
                        var marker = new window.google.maps.Marker({
                            map: mapAttractions,
                            position: latlng,
                            title: attraction.Name
                        });

                        marker.info = new window.google.maps.InfoWindow({
                            content: "<div class=\"marker-info-window\"><h2> " + attraction.Name + "</h2><br/><a href=\" " + attraction.Link + " \">Till besöksmålet</a></div>"
                        });

                        window.google.maps.event.addListener(marker, 'click', function () {
                            marker.info.open(mapAttractions, marker);
                        });

                        //Save markers, so we can manipulate them later.
                        attractionMarkersArray.push(marker);
                    });
                },
                error: function (x, y, z) {
                    console.log(x, y, z);
                }
            });
        }


        //Get filtered attractions
        function getFilteredAttractions(nodes, categories, serviceCategories, initialPageSize, page, appendResult) {

            $.ajax({
                url: '/api/v1/attractions/getfiltered',
                type: 'GET',
                dataType: 'json',
                data: { nodeIds: nodes, attractionCategories: categories, serviceCategories: serviceCategories, page: page, initialPageSize: initialPageSize },
                success: function (data) {
                    updateAttractionUi(data);

                    if (currentAttractionView == "list") {
                        setTimeout(function () {
                            if (appendResult == null || appendResult === false) {
                                writeAttractionResponse(data.Attractions);
                            } else {
                                appendAttractionResponse(data.Attractions);
                            }
                            $('#js-load-attractions').fadeOut(200);
                        }, 600);
                    }
                },
                error: function (x, y, z) {
                    console.log(x, y, z);
                }
            });
        }

        //Get filtered attractions for Map
        function getFilteredAttractionsForMap(nodes, categories, serviceCategories) {
            removeAttractionMarkers();
            $.ajax({
                url: '/api/v1/attractions/getfiltered',
                type: 'GET',
                dataType: 'json',
                data: { nodeIds: nodes, attractionCategories: categories, serviceCategories: serviceCategories, page: 0, initialPageSize: maxAttractionPageSize },
                success: function (data) {
                    updateAttractionUi(data);
                    $.each(data.Attractions, function (index, attraction) {
                        var latlng = new window.google.maps.LatLng(attraction.Lat, attraction.Lng);
                        var marker = new window.google.maps.Marker({
                            map: mapAttractions,
                            position: latlng,
                            title: attraction.Name
                        });

                        marker.info = new window.google.maps.InfoWindow({
                            content: "<div class=\"marker-info-window\"><h2> " + attraction.Name + "</h2><br/><a href=\" " + attraction.Link + " \">Till besöksmålet</a></div>"
                        });

                        window.google.maps.event.addListener(marker, 'click', function () {
                            marker.info.open(mapAttractions, marker);
                        });

                        //Save markers, so we can manipulate them later.
                        attractionMarkersArray.push(marker);
                    });
                },
                error: function (x, y, z) {
                    console.log(x, y, z);
                }
            });
        }

        /** END Data retrieval functions */

        /** UI interactions  */
        //Clicking search
        $("#attraction-dropdowns .checkbox").click(function () {
            $("#attraction-pager").data("currentpage", 0);//Reset currentPage for ajax page
            $(this).toggleClass('checked');

            var nodes = getAttractionGeoContainersToFilterOn();
            var categories = getAttractionCategoriesToFilterOn();
            var serviceCategories = getAttractionServiceCategoriesToFilterOn();

            if (currentAttractionView == "list") {
                $('#js-load-attractions').fadeIn(200);
            }

            //Do we have counties or categories?
            if (nodes.length > 1 || categories.length > 1 || serviceCategories.length > 1) {
                if (currentAttractionView == "list") {
                    getFilteredAttractions(nodes, categories, serviceCategories, attractionPageSize, 0);
                } else {
                    getFilteredAttractionsForMap(nodes, categories, serviceCategories);
                }
            }
            else {
                if (currentAttractionView == "list") {
                    getAllAttractions(attractionPageSize, 0);
                } else {
                    getAllAttractionsForMap();
                }
            }
        });

        //Ladda fler på filtrerings sida.
        $("#attraction-pager").click(function () {
            var nodes = getAttractionGeoContainersToFilterOn();
            var categories = getAttractionCategoriesToFilterOn();
            var serviceCategories = getAttractionServiceCategoriesToFilterOn();
            var currentPage = parseInt($("#attraction-pager").data("currentpage"));
            $("#attraction-pager").data("currentpage", currentPage + 1);

            //Do we have counties or categories?
            if (nodes.length > 1 || categories.length > 1 || serviceCategories.length > 1) {
                getFilteredAttractions(nodes, categories, serviceCategories, attractionPageSize, currentPage + 1, true);
            }
            else {
                getAllAttractions(attractionPageSize, currentPage + 1, true);
            }
        });

        //Clicking 'Visa på karta'
        $("#js-attraction-map-view").click(function () {
            currentAttractionView = "map";
            var nodes = getAttractionGeoContainersToFilterOn();
            var categories = getAttractionCategoriesToFilterOn();
            var serviceCategories = getAttractionServiceCategoriesToFilterOn();
            $(".attraction-result-grid-view").hide();
            $('#js-attraction-grid-view').removeClass('active');
            $(this).addClass('active');

            //Do we have counties or categories?
            if (nodes.length > 1 || categories.length > 1 || serviceCategories.length > 1) {
                getFilteredAttractionsForMap(nodes, categories, serviceCategories);
            }
            else {
                getAllAttractionsForMap();
            }

            $(".attraction-result-map-view").fadeIn(function () {
                redrawAttractionMap();
            });
        });

        //Clicking 'Visa i lista'
        $("#js-attraction-grid-view").click(function () {

            currentAttractionView = "list";
            var nodes = getAttractionGeoContainersToFilterOn();
            var categories = getAttractionCategoriesToFilterOn();
            var serviceCategories = getAttractionServiceCategoriesToFilterOn();

            $(".attraction-result-map-view").hide();
            $('#js-attraction-map-view').removeClass('active');
            $(this).addClass('active');
            $(".attraction-result-grid-view").fadeIn(function () {

                //Do we have counties or categories?
                if (nodes.length > 1 || categories.length > 1 || serviceCategories.length > 1) {
                    getFilteredAttractions(nodes, categories, serviceCategories, attractionPageSize, 0, false);
                }
                else {
                    getAllAttractions(attractionPageSize, 0, false);
                }
            });
        });

        //Dropdowns for counties and categories.
        $('#attraction-sorting-controls span.sort').click(function () {
            $this = $(this);
            var id = $(this).attr('data-id');
            var dropdown = $('#attraction-dropdowns .dropdown.' + id);

            if ($this.hasClass('active')) {
                $this.removeClass('active');
            }
            else {
                $('span.sort').removeClass('active');
                $this.addClass('active');
            }

            if (dropdown.hasClass('toggled')) {
                dropdown.removeClass('toggled');
            }
            else {
                $('#attraction-dropdowns .dropdown').removeClass('toggled');
                dropdown.addClass('toggled');
            }
        });

        //Clicking 'Rensa urval'
        $('#js-clear-attraction-filter').click(function () {
            $('#attraction-dropdowns .checkbox').each(function (index) {
                $(this).removeClass('checked');
            });
            if (currentAttractionView == "list") {
                $('#js-load-attractions').show();
                getAllAttractions(attractionPageSize, 0);
            } else {
                getAllAttractionsForMap();
            }
            $("#attraction-pager").data("currentpage", 0);
        });

        //Clicking 'Visa fler'
        $('#attraction-forward').click(function () {

            var nodes = getAttractionGeoContainersToFilterOn();
            var categories = getAttractionCategoriesToFilterOn();
            var serviceCategories = getAttractionServiceCategoriesToFilterOn();

            window.location.href = window.location.protocol + '//' + window.location.host + '/besoeksmaal/?nodes=' + nodes + '&categories=' + categories + '&serviceCategories=' + serviceCategories + '';
        });

        /** END - UI interactions */

        /** Maps functions */
        //Initialize google map
        function initializeAttractionMap() {
            var mapCanvas = $("#map_attraction_canvas_home");
            var zoomFactor = 5;
            var options = {
                center: mapAttractionsCenter,
                zoom: zoomFactor,
                scaleControl: true,
                mapTypeId: window.google.maps.MapTypeId.ROADMAP
            };

            mapAttractions = new window.google.maps.Map(document.getElementById
               ("map_attraction_canvas_home"), options);
        }

        //Remove all markers
        function removeAttractionMarkers() {
            for (i = 0; i < attractionMarkersArray.length; i++) {
                attractionMarkersArray[i].setMap(null);
            }
            attractionMarkersArray.length = 0;
        }

        //Redraw map, useful when displaying map after some animation. etc.
        function redrawAttractionMap() {
            window.google.maps.event.trigger(mapAttractions, 'resize');
            mapAttractions.setCenter(mapAttractionsCenter);
        }
        /** END - Maps functions */

        /** Helper functions */
        //Response to write when on list view.
        function writeAttractionResponse(attractions) {
            var strResult = "";
            var cultureCardAdHeading = $("#culture-card").data("heading");
            var cultureCardAdText = $("#culture-card").data("text");
            var cultureCardAdLink = $("#culture-card").data("link");
            $.each(attractions, function (index, attraction) {
                //Inject culture-card as 4th element.
                if (index == 3) {
                    strResult += "<a href=\"" + cultureCardAdLink + " \" class=\"col-4\"><div class=\"grid-item ad a-ccard\"></div></a><a href=\"" + attraction.Link + " \" class=\"col-4 box\"><div class=\"grid-item\"><img src=\"" + attraction.MainImage + "\"><span class=\"button default blue\">Läs mer</span><div class=\"copy\"><h2 class=\"title box-item\">" + attraction.Name + "</h2><p class=\"box-item\">" + attraction.City + "</p></div></div></a>";
                } else {
                    strResult += "<a href=\"" + attraction.Link + " \" class=\"col-4 box\"><div class=\"grid-item\"><img src=\"" + attraction.MainImage + "\"><span class=\"button default blue\">Läs mer</span><div class=\"copy\"><h2 class=\"title box-item\">" + attraction.Name + "</h2><p class=\"box-item\">" + attraction.City + "</p></div></div></a>";
                }

            });
            //If attractions contains less or equal to 3 objects then append culture-card last.
            if (attractions.length <= 3) {
                strResult += "<a href=\"" + cultureCardAdLink + " \" class=\"col-4\"><div class=\"grid-item ad a-ccard\"></div></a>";
            }

            $("#divAttractionsResult").html(strResult);

            //Init culture-card ad-area with image from settings.
            $(".a-ccard").css('background-image', 'url("' + cultureCardAdImage + '")');
        }

        //Response to append when on list view and clicking "visa fler".
        function appendAttractionResponse(attractions) {
            var strResult = "";
            $.each(attractions, function (index, attraction) {
                strResult += "<a href=\"" + attraction.Link + " \" class=\"col-4 box\"><div class=\"grid-item\"><img src=\"" + attraction.MainImage + "\"><span class=\"button default blue\">Läs mer</span><div class=\"copy\"><h2 class=\"title box-item\">" + attraction.Name + "</h2><p class=\"box-item\">" + attraction.City + "</p></div></div></a>";
            });

            $("#divAttractionsResult").append(strResult);
        }

        //Get selected counties
        function getAttractionGeoContainersToFilterOn() {
            var results = Array();
            var nodes = "";
            $("#geocontainers-attraction-wrap .county-checkbox.checked").each(function () {
                results.push($(this).data('id'));
            });
            return nodes + results.join(',');
        }

        //Set selected counties
        function setAttractionGeoContainersToFilterOn(nodes) {
            $("#geocontainers-attraction-wrap .county-checkbox").each(function () {
                if (nodes.toString() != "" && nodes.contains($(this).data('id').toString()))
                    $(this).addClass('checked');
            });

        }

        //Get selected categories
        function getAttractionCategoriesToFilterOn() {
            var results = Array();
            var nodes = "";
            $("#categories-attraction-wrap .category-checkbox.checked").each(function () {
                results.push($(this).data('id'));
            });
            return nodes + results.join(',');
        }

        //Set selected categories
        function setAttractionCategoriesToFilterOn(categories) {
            $("#categories-attraction-wrap .category-checkbox").each(function () {
                if (categories.toString() != "" && categories.contains($(this).data('id').toString()))
                    $(this).addClass('checked');
            });
        }

        //Get selected service categories
        function getAttractionServiceCategoriesToFilterOn() {
            var results = Array();
            var nodes = "";
            $("#categories-attraction-wrap .service-category-checkbox.checked").each(function () {
                results.push($(this).data('id'));
            });
            return nodes + results.join(',');
        }

        //Set selected service categories
        function setAttractionServiceCategoriesToFilterOn(serviceCategories) {
            $("#categories-attraction-wrap .service-category-checkbox").each(function () {
                if (serviceCategories.toString() != "" && serviceCategories.contains($(this).data('id').toString()))
                    $(this).addClass('checked');
            });
        }

        //Updates counter for total amount of found attractions
        function updateAttractionTotalCount(data) {
            var strResult = data.Total;
            $("#spanAttractionTotalCount").text(strResult);
        }

        //Update UI.
        function updateAttractionUi(data) {
            updateAttractionTotalCount(data);
        }
        /** END Helper functions */

    }//END Check if on start page

});