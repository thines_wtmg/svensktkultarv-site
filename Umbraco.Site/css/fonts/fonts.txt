@font-face {
  font-family: 'VWHeadlineBkTab';
  src: url('vwheadline-bktab-webfont.eot');
  src: url('vwheadline-bktab-webfont.eot?#iefix') format('embedded-opentype'), url('vwheadline-bktab-webfont.woff') format('woff'), url('vwheadline-bktab-webfont.ttf') format('truetype'), url('vwheadline-bktab-webfont.svg#VWHeadlineBkTab') format('svg');
  font-weight: normal;
  font-style: normal;
}
@font-face {
  font-family: 'VWHeadlineBlk';
  src: url('vwheadline-blk-webfont.eot');
  src: url('vwheadline-blk-webfont.eot?#iefix') format('embedded-opentype'), url('vwheadline-blk-webfont.woff') format('woff'), url('vwheadline-blk-webfont.ttf') format('truetype'), url('vwheadline-blk-webfont.svg#VWHeadlineBlk') format('svg');
  font-weight: normal;
  font-style: normal;
}
p {
	font-family: "VWHeadlineBlk";
	font-size: 2.0em;
}