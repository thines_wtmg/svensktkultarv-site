﻿using System;
using System.Net;
using System.Net.Http;
using System.Reflection;
using System.Web.Http;
using AttributeRouting;
using AttributeRouting.Web.Http;
using log4net;
using Umbraco.Extensions.BLL;
using Umbraco.Extensions.Controllers.WebAPI.v1;
using Umbraco.Extensions.Utilities.Security;
using Umbraco.Web.WebApi;

namespace Umbraco.Site.App_Plugins.CultureCard.Controllers
{
    [ReturnsJson]
    public class CultureCardBackofficeApiController : UmbracoAuthorizedApiController
    {
        private static readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        [GET("/Plugins/Culturecards/GetAll")]
        public HttpResponseMessage GetAll()
        {
            //Check if user is allowed to access the api. This should be possible to convert to some authorization attribute.
            if (Settings.CultureCardAdminUsers.AllowedUsers.Contains(UmbracoContext.HttpContext.User.Identity.Name))
            {
                try
                {
                    return Request.CreateResponse(HttpStatusCode.OK, CultureCardLogic.GetAll());
                }
                catch (Exception ex)
                {
                    Log.ErrorFormat("Could not retriev culture cards error:{0} ", ex.Message);
                }

                throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.InternalServerError,
                    "Retrieval failed"));
            }

            throw new HttpResponseException(Request.CreateErrorResponse((HttpStatusCode)401, "Not Authorized"));
        }
        [GET("/Plugins/Culturecards/GetById")]
        public HttpResponseMessage GetById(int cardid)
        {
            //Check if user is allowed to access the api. This should be possible to convert to some authorization attribute.
            if (Settings.CultureCardAdminUsers.AllowedUsers.Contains(UmbracoContext.HttpContext.User.Identity.Name))
            {
                try
                {
                    return Request.CreateResponse(HttpStatusCode.OK, CultureCardLogic.GetById(cardid));
                }
                catch (Exception ex)
                {
                    Log.ErrorFormat("Could not retriev culture cards error:{0} ", ex.Message);
                }

                throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.InternalServerError,
                    "Retrieval failed"));
            }

            throw new HttpResponseException(Request.CreateErrorResponse((HttpStatusCode)401, "Not Authorized"));
        }

        [HttpPost]
        [POST("/Plugins/Culturecards/Update")]
        public HttpResponseMessage Update(Extensions.Models.Rdbms.POCOS.CultureCard card)
        {
            //Check if user is allowed to access the api. This should be possible to convert to some authorization attribute.
            if (Settings.CultureCardAdminUsers.AllowedUsers.Contains(UmbracoContext.HttpContext.User.Identity.Name))
            {
                Extensions.Models.Rdbms.POCOS.CultureCard existingCard = null;
                try
                {
                    existingCard = CultureCardLogic.GetById(card.Id);
                    //Get the Umbraco db
                    var db = ApplicationContext.DatabaseContext.Database;

                    //Update
                    db.Update(card);
                }
                catch (Exception ex)
                {
                    Log.ErrorFormat("Could not update existing culture card:{0}, error:{1} ", existingCard.LogFormatedString(), ex.Message);
                    throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.InternalServerError, "CRUDE operation failed"));
                }

                Log.InfoFormat("Culture card Successfully updated old card:{0} new card:{1}", existingCard.LogFormatedString(), card.LogFormatedString());
                return Request.CreateResponse(HttpStatusCode.OK, card);
            }
            throw new HttpResponseException(Request.CreateErrorResponse((HttpStatusCode)401, "Not Authorized"));
        }
        [HttpPost]
        [POST("/Plugins/Culturecards/Delete")]
        public HttpResponseMessage Delete(Extensions.Models.Rdbms.POCOS.CultureCard card)
        {
            //Check if user is allowed to access the api. This should be possible to convert to some authorization attribute.
            if (Settings.CultureCardAdminUsers.AllowedUsers.Contains(UmbracoContext.HttpContext.User.Identity.Name))
            {
                Extensions.Models.Rdbms.POCOS.CultureCard existingCard = null;
                try
                {
                    existingCard = CultureCardLogic.GetById(card.Id);
                    //Get the Umbraco db
                    var db = ApplicationContext.DatabaseContext.Database;

                    //Delete
                    db.Delete(existingCard);
                }
                catch (Exception ex)
                {
                    Log.ErrorFormat("Could not delete existing culture card:{0}, error:{1} ", existingCard.LogFormatedString(), ex.Message);
                    throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.InternalServerError, "CRUDE operation failed"));
                }

                Log.InfoFormat("Culture card Successfully deleted, card:{0}", existingCard.LogFormatedString());
                return Request.CreateResponse(HttpStatusCode.OK, card);
            }
            throw new HttpResponseException(Request.CreateErrorResponse((HttpStatusCode)401, "Not Authorized"));
        }
    }
}