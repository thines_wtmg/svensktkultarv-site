﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Formatting;
using System.Web;
using umbraco.BusinessLogic.Actions;
using Umbraco.Web.Models.Trees;
using Umbraco.Web.Mvc;
using Umbraco.Web.Trees;

namespace Umbraco.Site.App_Plugins.CultureCard.Controllers
{
    [PluginController("CultureCard")]
    [Umbraco.Web.Trees.Tree("CultureCard", "CultureCardTree", "Kulturarvskort", iconClosed: "icon-certificate")]
    public class CultureCardTreeController : TreeController
    {
        protected override TreeNodeCollection GetTreeNodes(string id, FormDataCollection queryStrings)
        {
            var nodes = new TreeNodeCollection();
            if (id == "-1")
            {
                var item = this.CreateTreeNode("dashboard", id, queryStrings, "Visa kulturkort", "icon-certificate", false);
                nodes.Add(item);
            }
            return nodes;
        }
        
        protected override MenuItemCollection GetMenuForNode(string id, FormDataCollection queryStrings)
        {
            var menu = new MenuItemCollection();
            
            return menu;
        }
    }
}