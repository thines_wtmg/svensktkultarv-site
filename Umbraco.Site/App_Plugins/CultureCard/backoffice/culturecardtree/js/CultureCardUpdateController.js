﻿var app = angular.module("umbraco");

app.controller("CultureCardUpdateController", function ($scope, $http, $location, notificationsService, dateFilter) {
    $http.get('/plugins/culturecards/getbyid', { params: { cardid: $location.search().cardid } }).
        success(function (data, status, headers, config) {
            $scope.card = data;
            $scope.card.PhoneNumber = parseInt($scope.card.PhoneNumber);
            $scope.card.ZipCode = parseInt($scope.card.ZipCode);
            $scope.card.ExpireDate = dateFilter($scope.card.ExpireDate, 'yyyy-MM-dd');
        }).
        error(function (data, status, headers, config) {
            console.log('Could not fetch culturecards');
        });

    $scope.submitForm = function () {
            if (this.ccForm.$valid) {
                // Posting data 
                $http({
                        method: 'POST',
                        url: '/plugins/culturecards/update',
                        data: $scope.card //forms cc object
                    })
                    .success(function(data, response) {
                        notificationsService.success("Success", "Kortet uppdaterat");
                    })
                    .error(function(data, response) {
                        notificationsService.error("Error", "Något gick fel");
                    });
            } else {
                notificationsService.error("Error", "Fyll i alla fält korrekt");
            }

        },
    $scope.deleteCard = function (card) {
        $http({
            method: 'POST',
            url: '/plugins/culturecards/delete',
            data: card //forms cc object
        })
        .success(function (data, response) {
            notificationsService.info("Success", "Kortet för " + card.FirstName +" " +card.LastName +" är nu borttaget:");
            window.location = "/umbraco#/CultureCard/CultureCardTree/edit/dashboard";
        })
        .error(function (data, response) {
            notificationsService.error("Error", "Något gick fel");
        });
    }
});

app.directive('confirmationNeeded', function () {
    return {
        priority: 1,
        terminal: true,
        link: function (scope, element, attr) {
            var msg = attr.confirmationNeeded || "Are you sure?";
            var clickAction = attr.ngClick;
            element.bind('click', function () {
                if (window.confirm(msg)) {
                    scope.$eval(clickAction);
                }
            });
        }
    };
});