﻿var app = angular.module("umbraco");

app.controller("CultureCardSectionController", function($scope, $http) {
        $http.get('/plugins/culturecards/getall').
            success(function(data, status, headers, config) {
                $scope.cards = data;
            }).
            error(function(data, status, headers, config) {
                console.log('Could not fetch culturecards');
            });
});

