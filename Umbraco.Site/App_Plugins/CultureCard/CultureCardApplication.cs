﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using umbraco.businesslogic;
using umbraco.interfaces;

namespace Umbraco.Site.App_Plugins.CultureCard
{
    [Application("CultureCard", "CultureCard", "traycontent", 15)]
    public class CultureCardApplication : IApplication { }
}