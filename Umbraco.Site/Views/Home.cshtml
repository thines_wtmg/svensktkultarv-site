﻿@using System.Linq
@using SEOChecker.Core.Extensions
@inherits Umbraco.Web.Mvc.UmbracoViewPage<MasterModel<Home>>
@{
    Layout = "Master.cshtml";

    var cultureCardAdHeading = Model.SwedishHeritageCardInfoPage != null ? Model.SwedishHeritageCardInfoPage.Name : "Sidan är ej utpekad";
    var cultureCardAdText = Model.SwedishHeritageCardInfoPage != null && ((Base)Model.SwedishHeritageCardInfoPage) != null ? ((Base)Model.SwedishHeritageCardInfoPage).Preamble : "Sidan är ej utpekad";
    var cultureCardLink = Model.SwedishHeritageCardInfoPage != null ? Model.SwedishHeritageCardInfoPage.Url : "#";
    var cultureCardImage = Model.SwedishHeritageCardImage != null ? Model.SwedishHeritageCardImage.Cropper.GetCropUrl(270,390,null,100) : "/css/img/kort.jpg";
}
@section headerOgTags{
    <!--Facebook og tags-->
    <meta property="og:title" content="@Model.SeoTitle" />
    <meta property="og:type" content="website" />
    <meta property="og:image" content="css/img/logo.png" />
    <meta property="og:url" content="@Model.Content.UrlWithDomain()" />
    <meta property="og:description" content="@Model.SeoDescription" />

    <!-- Twitter Card data -->
    <meta name="twitter:card" content="summary">
    <meta name="twitter:site" content="@Model.Content.UrlWithDomain()">
    <meta name="twitter:title" content="@Model.SeoTitle">
    <meta name="twitter:description" content="@Model.SeoDescription">
    <meta name="twitter:creator" content="@Model.Content.CreatorName">
    <meta name="twitter:image" content="css/img/logo.png">
}
@section footerScripts{
    <link rel="stylesheet" href="~/js/develop/jquery-ui-1.11.2.custom/jquery-ui.min.css" />
    <script src="~/js/develop/jquery-ui-1.11.2.custom/jquery-ui.min.js"></script>
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?v=3.exp"></script>
    <script type="text/javascript">
        var autocompleteData;
        initAutocomplete();

        function initAutocomplete() {
            $.ajax({
                url: '/api/v1/search/getall',
                type: 'GET',
                dataType: 'json',
                success: function (data) {
                    autocompleteData = data;
                    $("#search").autocomplete({
                        source: autocompleteData,
                        minLength: 2,
                        delay: 0,
                        create: function () {
                            $(this).data('ui-autocomplete')._renderItem = function (ul, item) {
                                return $("<li class=\"" +item.type +"\"><i class=\"" + 'icon-brush' +"\"></i><i class=\"" + 'icon-location' +"\"></i></i><span>"+ item.city +"</span>")
                                  .attr("data-value", item.value)
                                  .append(item.value)
                                  .appendTo(ul);
                            };
                        },
                        select: function (event, ui) {
                            $("#search").val(ui.item.value);
                            window.location.href = ui.item.link;
                        },
                        focus: function (event, ui) {
                            event.preventDefault();
                            $("#search").val(ui.item.value);
                        }
                    });
                },
                error: function (x, y, z) {
                    console.log(x, y, z);
                }
            });
        }


</script>

}
<h1 style="display:none;">Svenskt Kulturarv, Ett skyltfönster för det fantastiska utbudet av museer, slott och kulturhistoriska besöksmål i Sverige </h1>
<div class="content attraction-search exhibition-search" id="home">
    <section class="featured">
        <div class="flexslider home">
            <ul class="slides">
                @Html.Partial("~/Views/Shared/Slider.cshtml", @Model.Content.Slider)
            </ul>
        </div>
    </section>
    <div class="search-wrap">
        <input id="search" type="search" placeholder="Sök bland våra utställningar och besöksmål..." />
        <span class="search"><i class="icon-search"></i></span>
    </div>
    <section class="filters">

        <!-- EXHIBITION FILTER -->
        <div class="filter-wrap">
            <div class="page-heading">
                <span class="head-icon yellow"><i class="icon-brush"></i></span>
                <h2 class="title heading"> Utställningar </h2>
            </div>
            <div id="exhibition-sorting-controls" class="sorting">
                <span class="instruct">Filtrera utställningar</span>
                <span class="sort exhib" data-id="county">Alla landskap <i class="icon-down-dir"></i></span>
                <span class="sort exhib" data-id="category">Kategorier <i class="icon-down-dir"></i></span>
                <span class="sort exhib" data-id="time">Tidsperiod<i class="icon-down-dir"></i></span>
                @*<span id="filter-exhibitions-button" class="filter-exhibitions-button">Sök <i class="icon-search"></i></span>*@
                <div class="viewing-options">
                    <span id="js-exhibition-view-grid" class="active"><i class="icon-layout"></i></span>
                    <span id="js-exhibition-view-map"><i class="icon-location"></i></span>
                </div>
            </div>
            <div class="tabs">
                <span class="tab active" id="js-filter-exhibitions">Funna utställningar (<span id="spanExhibitionTotalCount"></span>)</span>
                <span class="tab button" id="js-clear-exhibition-filter"><i class="icon-cancel-circled"></i>Rensa urval</span>
            </div>
        </div>
        <!--DROPDOWN LANDSKAP -->
        <div id="exhibition-dropdowns">
            <div id="exhibition-county-dropdown" class="county-wrap dropdown county">
                <div class="head">
                    <h2 class="title box-item">Landskap</h2>
                </div>
                <div id="geocontainers-exhibition-wrap" class="lists">
                    @Html.Partial("~/Views/Shared/GeoContainerList.cshtml", Model.Content.GeoContainers)
                </div>
            </div>
            <!--DROPDOWN KATEGORI -->
            <div id="exhibition-category-dropdown" class="county-wrap dropdown category">
                <div class="head">
                    <h2 class="title box-item">Kategorier</h2>
                    <h2 class="title box-item service">Service</h2>
                </div>
                <div id="categories-exhibition-wrap" class="lists">
                    @Html.Partial("~/Views/Shared/ExhibitionCategoryList.cshtml", Model.Content.Categories)
                </div>
            </div>
            <!--DROPDOWN TIDSPERIOD -->
            <div id="exhibition-time-dropdown" class="county-wrap dropdown time">
                <div class="head">
                    <h2 class="title box-item">Tidsperiod</h2>
                    <span id="spanExhibitionSelectedTimePeriod"> <span id="spanSelectedStartDate"></span><span id="spanSelectedStopDate"></span></span>
                </div>
                <div id="time-exhibition-wrap" class="lists">
                    <div class="datepicker" id="startDate"><input type="hidden" id="chosenStartDate"></div>
                    <div class="datepicker" id="stopDate"><input type="hidden" id="chosenStopDate"></div>
                </div>
            </div>
        </div>
    </section>
    <section class="filter-exhibition-results results">
        <div class="overlay" id="js-load-exhibitions">
            <div class="loader">
                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32" width="320" height="320" fill="#efb211">
                    <circle transform="translate(8 0)" cx="0" cy="16" r="0">
                        <animate attributeName="r" values="0; 4; 0; 0" dur="1.2s" repeatCount="indefinite" begin="0"
                                 keytimes="0;0.2;0.7;1" keySplines="0.2 0.2 0.4 0.8;0.2 0.6 0.4 0.8;0.2 0.6 0.4 0.8" calcMode="spline" />
                    </circle>
                    <circle transform="translate(16 0)" cx="0" cy="16" r="0">
                        <animate attributeName="r" values="0; 4; 0; 0" dur="1.2s" repeatCount="indefinite" begin="0.3"
                                 keytimes="0;0.2;0.7;1" keySplines="0.2 0.2 0.4 0.8;0.2 0.6 0.4 0.8;0.2 0.6 0.4 0.8" calcMode="spline" />
                    </circle>
                    <circle transform="translate(24 0)" cx="0" cy="16" r="0">
                        <animate attributeName="r" values="0; 4; 0; 0" dur="1.2s" repeatCount="indefinite" begin="0.6"
                                 keytimes="0;0.2;0.7;1" keySplines="0.2 0.2 0.4 0.8;0.2 0.6 0.4 0.8;0.2 0.6 0.4 0.8" calcMode="spline" />
                    </circle>
                </svg>
            </div>
        </div>
        <div class="grid-wrap">
            <!-- Exhibition Results -->
            <section class="exhibition-result-grid-view">
                <div id="divExhibitionsResult" class="grid-wrap">

                </div>
                <span id="exhibition-forward" class="button forward yellow">Visa fler</span>
            </section>

            <section class="exhibition-result-map-view">
                <div id="map_exhibition_canvas_home"></div>
            </section>
            <!-- Exhibition Results End -->
        </div>
    </section>

    <section class="filters">
        <!-- Attraction FILTER -->
        <div class="filter-wrap">
            <div class="page-heading">
                <span class="head-icon blue"><i class="icon-location"></i></span>
                <h2 class="title heading"> Besöksmål </h2>
            </div>
            <div id="attraction-sorting-controls" class="sorting">
                <span class="instruct">Filtrera besöksmål</span>
                <span class="sort exhib" data-id="county">Alla landskap <i class="icon-down-dir"></i></span>
                <span class="sort exhib" data-id="category">Kategorier <i class="icon-down-dir"></i></span>
                @*<span id="filter-attractions-button" class="filter-attractions-button">Sök <i class="icon-search"></i></span>*@
                <div class="viewing-options">
                    <span id="js-attraction-grid-view" class="active"><i class="icon-layout"></i></span>
                    <span id="js-attraction-map-view"><i class="icon-location"></i></span>
                </div>
            </div>
            <div class="tabs">
                <span class="tab active" id="js-filter-attractions">Funna besöksmål (<span id="spanAttractionTotalCount"></span>)</span>
                <span class="tab button" id="js-clear-attraction-filter"><i class="icon-cancel-circled"></i>Rensa urval</span>
            </div>
        </div>
        <!--DROPDOWN LANDSKAP -->
        <div id="attraction-dropdowns">
            <div class="county-wrap dropdown county">
                <div class="head">
                    <h2 class="title box-item">Landskap</h2>
                </div>
                <div id="geocontainers-attraction-wrap" class="lists">
                    @Html.Partial("~/Views/Shared/GeoContainerList.cshtml", Model.Content.GeoContainers)
                </div>
            </div>
            <!--DROPDOWN KATEGORI -->
            <div class="county-wrap dropdown category">
                <div class="head">
                    <h2 class="title box-item">Kategorier</h2>
                    <h2 class="title box-item service">Service</h2>
                </div>
                <div id="categories-attraction-wrap" class="lists">
                    @Html.Partial("~/Views/Shared/AttractionCategoryList.cshtml", Model.Content.Categories)
                </div>
            </div>
        </div>
    </section>


    <!-- Attraction Results -->
    <section class="attraction-result-grid-view results">
        <div class="overlay" id="js-load-attractions">
            <div class="loader">
                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32" width="320" height="320" fill="#0f73b6">
                    <circle transform="translate(8 0)" cx="0" cy="16" r="0">
                        <animate attributeName="r" values="0; 4; 0; 0" dur="1.2s" repeatCount="indefinite" begin="0"
                                 keytimes="0;0.2;0.7;1" keySplines="0.2 0.2 0.4 0.8;0.2 0.6 0.4 0.8;0.2 0.6 0.4 0.8" calcMode="spline" />
                    </circle>
                    <circle transform="translate(16 0)" cx="0" cy="16" r="0">
                        <animate attributeName="r" values="0; 4; 0; 0" dur="1.2s" repeatCount="indefinite" begin="0.3"
                                 keytimes="0;0.2;0.7;1" keySplines="0.2 0.2 0.4 0.8;0.2 0.6 0.4 0.8;0.2 0.6 0.4 0.8" calcMode="spline" />
                    </circle>
                    <circle transform="translate(24 0)" cx="0" cy="16" r="0">
                        <animate attributeName="r" values="0; 4; 0; 0" dur="1.2s" repeatCount="indefinite" begin="0.6"
                                 keytimes="0;0.2;0.7;1" keySplines="0.2 0.2 0.4 0.8;0.2 0.6 0.4 0.8;0.2 0.6 0.4 0.8" calcMode="spline" />
                    </circle>
                </svg>
            </div>
        </div>
        <div id="divAttractionsResult" class="grid-wrap">

        </div>
        <span id="attraction-forward" class="button forward blue">Visa fler</span>
    </section>
    <section class="attraction-result-map-view">
        <div id="map_attraction_canvas_home"></div>
    </section>
    <!-- Attraction Results End -->

</div>
<!-- MOBILE ONLY -->
<div class="mobile-only">
    <a href="@cultureCardLink" class="col-4">
        <div class="grid-item ad">
            <div class="card">
                <h2 class="title ad">@cultureCardAdHeading</h2>
                <p class="article">@cultureCardAdText</p>
            </div>
        </div>
    </a>
    <a href="~/mobilapp" class="col-4">
        <div class="grid-item ad">
            <div class="card blue">
                <h2 class="title ad">Kulturarvsappen</h2>
                <p class="article">Snart lanserar vi vår nya mobil-app till android och iOS där du enkelt kan hitta utställningar och besöksmål direkt i mobilen!</p>
            </div>
        </div>
    </a>
</div>
<div id="culture-card" class="hidden" data-heading="@cultureCardAdHeading" data-text="@cultureCardAdText" data-link="@cultureCardLink" data-image="@cultureCardImage" >

</div>
<div id="settings" class="hidden" data-initialpagesize="@(global::Umbraco.Extensions.Controllers.WebAPI.v1.Settings.InitialPageSizeForHomePage)" data-maxpagesize="@(global::Umbraco.Extensions.Controllers.WebAPI.v1.Settings.MaxPageSize)" data-pagesize="" data-exhibitionsearch="utstaellningar/" data-attractionsearch="besoeksmaal/">

</div>
